INTRODUCTION
------------

Provides enhanced Drush migrate commands.


REQUIREMENTS
------------

Drush ^11.


INSTALLATION
------------

You can install Smart Migrate CLI as you would normally install a contributed
Drupal module.


CONFIGURATION
-------------

This module does not have any configuration option.


USAGE
-----

If Smart Migrate CLI is enabled, it overrides the original Drush migrate
commands.


DOCUMENTATION
-------------

There is no extra documentation, but I'm pretty sure it is not necessary at all.


MAINTAINERS
-----------

* Zoltán Horváth (huzooka) - https://www.drupal.org/u/huzooka

This project has been sponsored by [Kibit Solutions][1].

[1]: https://kibitsolutions.com/
