<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_fixes\Utility;

use Drupal\smart_migrate_cli\Traits\SmartMigrationConfigurationTrait;

/**
 * Utility for fixing issues of Drupal migration plugin definitions.
 */
class MigrationDefinitionUtility {

  use SmartMigrationConfigurationTrait;

  /**
   * Fixes entity translation related bugs of core migrations.
   *
   * If entity translation settings (language_content_settings config entities)
   * are migrated only before translations, then the default entities are
   * migrated without 'content_translation_source' and
   * 'content_translation_outdated' fields, so these won't get the appropriate
   * (default) values.
   *
   * @param array[] $migrations
   *   An associative array of migration plugin definitions keyed by their ID.
   */
  public static function fixEntityTranslationRelatedBugs(array &$migrations): void {
    static::fixCommentTranslationDependencies($migrations);
    static::fixMenuLinkTranslationDependencies($migrations);
    static::fixNodeTranslationDependencies($migrations);
    static::fixTermTranslationDependencies($migrations);
    static::fixUserTranslationDependencies($migrations);
    static::fixEntityTranslationSettingsMigrationOrder($migrations);
  }

  /**
   * Fixes entity translation related bugs of core comment migrations.
   *
   * @param array[] $migrations
   *   An associative array of migration plugin definitions keyed by their ID.
   */
  protected static function fixCommentTranslationDependencies(array &$migrations): void {
    $node_migrations = array_filter(
      $migrations,
      function (array $definition) {
        return in_array($definition['id'], ['d7_comment'], TRUE);
      }
    );

    foreach (array_keys($node_migrations) as $plugin_id) {
      $migrations[$plugin_id]['migration_dependencies']['optional'] = array_unique(
        array_merge(
          $migrations[$plugin_id]['migration_dependencies']['optional'] ?? [],
          [
            'd7_entity_translation_settings',
            'd7_language_content_comment_settings',
          ]
        )
      );
    }
  }

  /**
   * Fixes entity translation related bugs of core menu link migration.
   *
   * @param array[] $migrations
   *   An associative array of migration plugin definitions keyed by their ID.
   */
  protected static function fixMenuLinkTranslationDependencies(array &$migrations): void {
    if (!empty($migrations['d7_menu_links'])) {
      $migrations['d7_menu_links']['migration_dependencies']['optional'] = array_unique(
        array_merge(
          $migrations['d7_menu_links']['migration_dependencies']['optional'] ?? [],
          ['d7_language_content_menu_settings']
        )
      );
    }
  }

  /**
   * Fixes entity translation related bugs of core node migrations.
   *
   * @param array[] $migrations
   *   An associative array of migration plugin definitions keyed by their ID.
   */
  protected static function fixNodeTranslationDependencies(array &$migrations): void {
    $node_migrations = array_filter(
      $migrations,
      function (array $definition) {
        return in_array($definition['id'], ['d7_node', 'd7_node_complete'], TRUE);
      }
    );

    foreach (array_keys($node_migrations) as $plugin_id) {
      $migrations[$plugin_id]['migration_dependencies']['optional'] = array_unique(
        array_merge(
          $migrations[$plugin_id]['migration_dependencies']['optional'] ?? [],
          ['d7_entity_translation_settings', 'd7_language_content_settings']
        )
      );
    }
  }

  /**
   * Fixes entity translation related bugs of core taxonomy term migrations.
   *
   * @param array[] $migrations
   *   An associative array of migration plugin definitions keyed by their ID.
   */
  protected static function fixTermTranslationDependencies(array &$migrations): void {
    $node_migrations = array_filter(
      $migrations,
      function (array $definition) {
        return in_array($definition['id'], ['d7_taxonomy_term'], TRUE);
      }
    );

    foreach (array_keys($node_migrations) as $plugin_id) {
      $migrations[$plugin_id]['migration_dependencies']['optional'] = array_unique(
        array_merge(
          $migrations[$plugin_id]['migration_dependencies']['optional'] ?? [],
          [
            'd7_entity_translation_settings',
            'd7_language_content_taxonomy_vocabulary_settings',
          ]
        )
      );
    }
  }

  /**
   * Fixes entity translation related bugs of core user migration.
   *
   * @param array[] $migrations
   *   An associative array of migration plugin definitions keyed by their ID.
   */
  protected static function fixUserTranslationDependencies(array &$migrations): void {
    if (!empty($migrations['d7_user'])) {
      $migrations['d7_user']['migration_dependencies']['optional'] = array_unique(
        array_merge(
          $migrations['d7_user']['migration_dependencies']['optional'] ?? [],
          ['d7_entity_translation_settings']
        )
      );
    }
  }

  /**
   * Entity translation settings migration should be runned as last.
   *
   * @param array[] $migrations
   *   An associative array of migration plugin definitions keyed by their ID.
   */
  protected static function fixEntityTranslationSettingsMigrationOrder(array &$migrations): void {
    $et_settings_migrations = array_filter(
      $migrations,
      function (array $definition) {
        return $definition['destination']['plugin'] === 'entity:language_content_settings' && in_array('Drupal 7', $definition['migration_tags'] ?? [], TRUE);
      }
    );
    if (!empty($et_settings_migrations['d7_entity_translation_settings'])) {
      unset($et_settings_migrations['d7_entity_translation_settings']);

      $migrations['d7_entity_translation_settings']['migration_dependencies']['required'] = array_unique(
        array_merge(
          $migrations['d7_entity_translation_settings']['migration_dependencies']['required'] ?? [],
          array_keys($et_settings_migrations)
        )
      );
    }
  }

}
