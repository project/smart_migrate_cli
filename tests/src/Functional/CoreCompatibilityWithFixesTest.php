<?php

namespace Drupal\Tests\smart_migrate_cli\Functional;

/**
 * Tests Drupal 7 upgrade using Smart Migrate CLI with migration fixes.
 *
 * @see \Drupal\Tests\smart_migrate_cli\Functional\CoreCompatibilityTest
 *
 * @group smart_migrate_cli
 */
class CoreCompatibilityWithFixesTest extends CoreCompatibilityTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'smart_migrate_fixes',
  ];

}
