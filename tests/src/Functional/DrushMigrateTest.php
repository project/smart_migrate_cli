<?php

namespace Drupal\Tests\smart_migrate_cli\Functional;

use Drupal\Core\Database\Database;
use Drupal\Core\Extension\ModuleInstallerInterface;

/**
 * Tests basic import functionality.
 *
 * @group smart_migrate_cli
 */
class DrushMigrateTest extends DrushMigrateTestBase {

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'config_translation',
    'content_translation',
    'datetime_range',
    'smart_migrate_fixes',
    'migmag',
    'migrate_drupal',
    'telephone',
  ];

  /**
   * Tests migration import with Smart Migrate CLI's Drush command.
   */
  public function testDrushMigration(): void {
    // Use the default migration source connection - override the connection
    // info what the base Migrate Magician test had set up.
    $this->drush(
      'smart-migrate:configure',
      [],
      [
        'connection' => $this->randomMachineName(8),
        'sites-subdir' => substr($this->siteDirectory, 6),
        'db-url' => $this->getDatabaseUrl($this->sourceDatabase),
        'source-base-path' => implode(DIRECTORY_SEPARATOR, [
          DRUPAL_ROOT,
          'core',
          'modules',
          'migrate_drupal_ui',
          'tests',
          'src',
          'Functional',
          'd7',
          'files',
        ]),
      ]
    );

    $this->assertUnimportedD7Migrations();

    $this->drush(
      'migrate:import',
      ['--no-progress'],
      ['tag' => 'Drupal 7']
    );

    $this->assertImportedD7Migrations();
  }

  /**
   * Verifies the existence and status of imported migrations.
   */
  protected function assertImportedD7Migrations(): void {
    $this->drush(
      'migrate:status',
      [],
      [
        'tag' => 'Drupal 7',
        'fields' => 'id,status,total,imported,unprocessed',
      ]
    );
    $expected_lines = [
      'action_settings                                                                              Idle   1    0           0',
      'block_content_body_field                                                                     Idle   1    1 (100%)    0',
      'block_content_entity_display                                                                 Idle   1    1 (100%)    0',
      'block_content_entity_form_display                                                            Idle   1    1 (100%)    0',
      'block_content_type                                                                           Idle   1    1 (100%)    0',
      'contact_category                                                                             Idle   1    1 (100%)    0',
      'd7_action                                                                                    Idle   18   17 (94.4%)  0',
      'd7_block_translation                                                                         Idle   2    1 (50%)     1',
      'd7_comment                                                                                   Idle   4    4 (100%)    0',
      'd7_comment_entity_display                                                                    Idle   8    8 (100%)    0',
      'd7_comment_entity_form_display                                                               Idle   8    8 (100%)    0',
      'd7_comment_entity_form_display_subject                                                       Idle   8    8 (100%)    0',
      'd7_comment_entity_translation                                                                Idle   2    2 (100%)    0',
      'd7_comment_field                                                                             Idle   8    8 (100%)    0',
      'd7_comment_field_instance                                                                    Idle   8    8 (100%)    0',
      'd7_comment_type                                                                              Idle   8    8 (100%)    0',
      'd7_contact_settings                                                                          Idle   1    1 (100%)    0',
      'd7_custom_block                                                                              Idle   1    1 (100%)    0',
      'd7_custom_block_translation                                                                  Idle   3    2 (66.7%)   1',
      'd7_dblog_settings                                                                            Idle   1    1 (100%)    0',
      'd7_entity_translation_settings                                                               Idle   6    6 (100%)    0',
      'd7_field                                                                                     Idle   62   58 (93.5%)  0',
      'd7_field_formatter_settings                                                                  Idle   97   89 (91.8%)  0',
      'd7_field_instance                                                                            Idle   86   78 (90.7%)  0',
      'd7_field_instance_label_description_translation                                              Idle   32   14 (43.8%)  0',
      'd7_field_instance_option_translation                                                         Idle   20   8 (40%)     0',
      'd7_field_instance_widget_settings                                                            Idle   86   78 (90.7%)  0',
      'd7_field_option_translation                                                                  Idle   20   20 (100%)   0',
      'd7_file                                                                                      Idle   2    2 (100%)    0',
      'd7_file_private                                                                              Idle   1    1 (100%)    0',
      'd7_filter_format                                                                             Idle   5    5 (100%)    0',
      'd7_filter_settings                                                                           Idle   1    1 (100%)    0',
      'd7_global_theme_settings                                                                     Idle   1    1 (100%)    0',
      'd7_image_settings                                                                            Idle   1    1 (100%)    0',
      'd7_image_styles                                                                              Idle   3    3 (100%)    0',
      'd7_language_content_comment_settings                                                         Idle   8    7 (87.5%)   0',
      'd7_language_content_menu_settings                                                            Idle   1    1 (100%)    0',
      'd7_language_content_settings                                                                 Idle   8    6 (75%)     0',
      'd7_language_content_taxonomy_vocabulary_settings                                             Idle   8    8 (100%)   0',
      'd7_language_negotiation_settings                                                             Idle   1    1 (100%)    0',
      'd7_language_types                                                                            Idle   1    1 (100%)    0',
      'd7_menu                                                                                      Idle   6    6 (100%)    0',
      'd7_menu_links                                                                                Idle   13   9 (69.2%)   0',
      'd7_menu_links_localized                                                                      Idle   3    2 (66.7%)   0',
      'd7_menu_links_translation                                                                    Idle   2    1 (50%)     1',
      'd7_menu_translation                                                                          Idle   5    5 (100%)    0',
      'd7_node_complete:a_thirty_two_character_type_name                                            Idle   0    0           0',
      'd7_node_complete:article                                                                     Idle   8    8 (100%)    0',
      'd7_node_complete:blog                                                                        Idle   3    3 (100%)    0',
      'd7_node_complete:book                                                                        Idle   0    0           0',
      'd7_node_complete:et                                                                          Idle   8    8 (100%)    0',
      'd7_node_complete:forum                                                                       Idle   2    2 (100%)    0',
      'd7_node_complete:page                                                                        Idle   0    0           0',
      'd7_node_complete:test_content_type                                                           Idle   1    1 (100%)    0',
      'd7_node_settings                                                                             Idle   1    1 (100%)    0',
      'd7_node_title_label                                                                          Idle   8    1 (12.5%)   0',
      'd7_node_type                                                                                 Idle   8    8 (100%)    0',
      'd7_search_page                                                                               Idle   2    2 (100%)    0',
      'd7_search_settings                                                                           Idle   1    1 (100%)    0',
      'd7_system_authorize                                                                          Idle   1    0           0',
      'd7_system_cron                                                                               Idle   1    1 (100%)    0',
      'd7_system_date                                                                               Idle   1    1 (100%)    0',
      'd7_system_file                                                                               Idle   1    1 (100%)    0',
      'd7_system_mail                                                                               Idle   1    1 (100%)    0',
      'd7_system_maintenance_translation                                                            Idle   1    1 (100%)    0',
      'd7_system_performance                                                                        Idle   1    1 (100%)    0',
      'd7_system_site_translation                                                                   Idle   2    2 (100%)    0',
      'd7_taxonomy_term:sujet_de_discussion                                                         Idle   5    5 (100%)    0',
      'd7_taxonomy_term:tags                                                                        Idle   10   10 (100%)   0',
      'd7_taxonomy_term:test_vocabulary                                                             Idle   3    3 (100%)    0',
      'd7_taxonomy_term:vocabfixed                                                                  Idle   1    1 (100%)    0',
      'd7_taxonomy_term:vocablocalized                                                              Idle   2    2 (100%)    0',
      'd7_taxonomy_term:vocablocalized2                                                             Idle   1    1 (100%)    0',
      'd7_taxonomy_term:vocabtranslate                                                              Idle   3    3 (100%)    0',
      'd7_taxonomy_term:vocabulary_name_much_longer_than_thirty_two_characters                      Idle   0    0           0',
      'd7_taxonomy_term_entity_translation:sujet_de_discussion                                      Idle   0    0           0',
      'd7_taxonomy_term_entity_translation:tags                                                     Idle   0    0           0',
      'd7_taxonomy_term_entity_translation:test_vocabulary                                          Idle   2    2 (100%)    0',
      'd7_taxonomy_term_entity_translation:vocabfixed                                               Idle   0    0           0',
      'd7_taxonomy_term_entity_translation:vocablocalized                                           Idle   0    0           0',
      'd7_taxonomy_term_entity_translation:vocablocalized2                                          Idle   0    0           0',
      'd7_taxonomy_term_entity_translation:vocabtranslate                                           Idle   0    0           0',
      'd7_taxonomy_term_entity_translation:vocabulary_name_much_longer_than_thirty_two_characters   Idle   0    0           0',
      'd7_taxonomy_term_localized_translation                                                       Idle   6    3 (50%)     3',
      'd7_taxonomy_term_translation                                                                 Idle   4    4 (100%)    0',
      'd7_taxonomy_vocabulary                                                                       Idle   8    8 (100%)    0',
      'd7_taxonomy_vocabulary_translation                                                           Idle   7    7 (100%)    0',
      'd7_url_alias                                                                                 Idle   6    6 (100%)    0',
      'd7_user                                                                                      Idle   3    3 (100%)    0',
      'd7_user_entity_translation                                                                   Idle   2    2 (100%)    0',
      'd7_user_flood                                                                                Idle   1    1 (100%)    0',
      'd7_user_mail                                                                                 Idle   1    1 (100%)    0',
      'd7_user_mail_translation                                                                     Idle   1    1 (100%)    0',
      'd7_user_role                                                                                 Idle   3    3 (100%)    0',
      'd7_user_settings                                                                             Idle   1    1 (100%)    0',
      'd7_user_settings_translation                                                                 Idle   1    1 (100%)    0',
      'd7_view_modes                                                                                Idle   6    6 (100%)    0',
      'default_language                                                                             Idle   1    1 (100%)    0',
      'file_settings                                                                                Idle   1    1 (100%)    0',
      'language                                                                                     Idle   3    3 (100%)    0',
      'language_prefixes_and_domains                                                                Idle   3    3 (100%)    0',
      'locale_settings                                                                              Idle   1    1 (100%)    0',
      'menu_settings                                                                                Idle   1    1 (100%)    0',
      'node_translation_menu_links                                                                  Idle   13   2 (15.4%)   0',
      'system_image                                                                                 Idle   1    1 (100%)    0',
      'system_image_gd                                                                              Idle   1    1 (100%)    0',
      'system_logging                                                                               Idle   1    1 (100%)    0',
      'system_maintenance                                                                           Idle   1    1 (100%)    0',
      'system_rss                                                                                   Idle   1    1 (100%)    0',
      'system_site                                                                                  Idle   1    1 (100%)    0',
      'taxonomy_settings                                                                            Idle   1    1 (100%)    0',
      'text_settings                                                                                Idle   1    1 (100%)    0',
      'user_picture_entity_display                                                                  Idle   1    1 (100%)    0',
      'user_picture_entity_form_display                                                             Idle   1    1 (100%)    0',
      'user_picture_field                                                                           Idle   1    1 (100%)    0',
      'user_picture_field_instance                                                                  Idle   1    1 (100%)    0',
    ];

    // Prior to Drupal core 9.5.x, migration of Drupal 7 default and admin theme
    // settings just fail without any processing (because default is Olivero
    // and admin is Claro).
    $expected_lines[] = version_compare(\Drupal::VERSION, '9.5.0-dev', 'ge')
      ? 'd7_theme_settings   Idle   2   0   2'
      : 'd7_theme_settings   Idle   2   0   0';

    // Drupal core 9.5.x+ migrates more block configs than previous versions.
    $expected_lines[] = version_compare(\Drupal::VERSION, '9.5.0-dev', 'ge')
      ? 'd7_block   Idle   52   7 (13.5%)   0'
      : 'd7_block   Idle   50   7 (14%)  0';

    $this->assertDrushOutputHasAllLines($expected_lines);
  }

  /**
   * Verifies the existence and status of unimported migrations.
   */
  protected function assertUnimportedD7Migrations(): void {
    $this->drush(
      'migrate:status',
      [],
      [
        'tag' => 'Drupal 7',
        'fields' => 'id,status,total,imported,unprocessed',
      ]
    );

    $expected_lines = [
      'action_settings                                                                              Idle   1    0    1',
      'block_content_body_field                                                                     Idle   1    0    1',
      'block_content_entity_display                                                                 Idle   1    0    1',
      'block_content_entity_form_display                                                            Idle   1    0    1',
      'block_content_type                                                                           Idle   1    0    1',
      'contact_category                                                                             Idle   1    0    1',
      'd7_action                                                                                    Idle   18   0    18',
      'd7_block_translation                                                                         Idle   2    0    2',
      'd7_comment                                                                                   Idle   4    0    4',
      'd7_comment_entity_display                                                                    Idle   8    0    8',
      'd7_comment_entity_form_display                                                               Idle   8    0    8',
      'd7_comment_entity_form_display_subject                                                       Idle   8    0    8',
      'd7_comment_entity_translation                                                                Idle   2    0    2',
      'd7_comment_field                                                                             Idle   8    0    8',
      'd7_comment_field_instance                                                                    Idle   8    0    8',
      'd7_comment_type                                                                              Idle   8    0    8',
      'd7_contact_settings                                                                          Idle   1    0    1',
      'd7_custom_block                                                                              Idle   1    0    1',
      'd7_custom_block_translation                                                                  Idle   3    0    3',
      'd7_dblog_settings                                                                            Idle   1    0    1',
      'd7_entity_translation_settings                                                               Idle   6    0    6',
      'd7_field                                                                                     Idle   62   0    62',
      'd7_field_formatter_settings                                                                  Idle   97   0    97',
      'd7_field_instance                                                                            Idle   86   0    86',
      'd7_field_instance_label_description_translation                                              Idle   32   0    32',
      'd7_field_instance_option_translation                                                         Idle   20   0    20',
      'd7_field_instance_widget_settings                                                            Idle   86   0    86',
      'd7_field_option_translation                                                                  Idle   20   0    20',
      'd7_file                                                                                      Idle   2    0    2',
      'd7_file_private                                                                              Idle   1    0    1',
      'd7_filter_format                                                                             Idle   5    0    5',
      'd7_filter_settings                                                                           Idle   1    0    1',
      'd7_global_theme_settings                                                                     Idle   1    0    1',
      'd7_image_settings                                                                            Idle   1    0    1',
      'd7_image_styles                                                                              Idle   3    0    3',
      'd7_language_content_comment_settings                                                         Idle   8    0    8',
      'd7_language_content_menu_settings                                                            Idle   1    0    1',
      'd7_language_content_settings                                                                 Idle   8    0    8',
      'd7_language_content_taxonomy_vocabulary_settings                                             Idle   8    0    8',
      'd7_language_negotiation_settings                                                             Idle   1    0    1',
      'd7_language_types                                                                            Idle   1    0    1',
      'd7_menu                                                                                      Idle   6    0    6',
      'd7_menu_links                                                                                Idle   13   0    13',
      'd7_menu_links_localized                                                                      Idle   3    0    3',
      'd7_menu_links_translation                                                                    Idle   2    0    2',
      'd7_menu_translation                                                                          Idle   5    0    5',
      'd7_node_complete:a_thirty_two_character_type_name                                            Idle   0    0    0',
      'd7_node_complete:article                                                                     Idle   8    0    8',
      'd7_node_complete:blog                                                                        Idle   3    0    3',
      'd7_node_complete:book                                                                        Idle   0    0    0',
      'd7_node_complete:et                                                                          Idle   8    0    8',
      'd7_node_complete:forum                                                                       Idle   2    0    2',
      'd7_node_complete:page                                                                        Idle   0    0    0',
      'd7_node_complete:test_content_type                                                           Idle   1    0    1',
      'd7_node_settings                                                                             Idle   1    0    1',
      'd7_node_title_label                                                                          Idle   8    0    8',
      'd7_node_type                                                                                 Idle   8    0    8',
      'd7_search_page                                                                               Idle   2    0    2',
      'd7_search_settings                                                                           Idle   1    0    1',
      'd7_system_authorize                                                                          Idle   1    0    1',
      'd7_system_cron                                                                               Idle   1    0    1',
      'd7_system_date                                                                               Idle   1    0    1',
      'd7_system_file                                                                               Idle   1    0    1',
      'd7_system_mail                                                                               Idle   1    0    1',
      'd7_system_maintenance_translation                                                            Idle   1    0    1',
      'd7_system_performance                                                                        Idle   1    0    1',
      'd7_system_site_translation                                                                   Idle   2    0    2',
      'd7_taxonomy_term:sujet_de_discussion                                                         Idle   5    0    5',
      'd7_taxonomy_term:tags                                                                        Idle   10   0    10',
      'd7_taxonomy_term:test_vocabulary                                                             Idle   3    0    3',
      'd7_taxonomy_term:vocabfixed                                                                  Idle   1    0    1',
      'd7_taxonomy_term:vocablocalized                                                              Idle   2    0    2',
      'd7_taxonomy_term:vocablocalized2                                                             Idle   1    0    1',
      'd7_taxonomy_term:vocabtranslate                                                              Idle   3    0    3',
      'd7_taxonomy_term:vocabulary_name_much_longer_than_thirty_two_characters                      Idle   0    0    0',
      'd7_taxonomy_term_entity_translation:sujet_de_discussion                                      Idle   0    0    0',
      'd7_taxonomy_term_entity_translation:tags                                                     Idle   0    0    0',
      'd7_taxonomy_term_entity_translation:test_vocabulary                                          Idle   2    0    2',
      'd7_taxonomy_term_entity_translation:vocabfixed                                               Idle   0    0    0',
      'd7_taxonomy_term_entity_translation:vocablocalized                                           Idle   0    0    0',
      'd7_taxonomy_term_entity_translation:vocablocalized2                                          Idle   0    0    0',
      'd7_taxonomy_term_entity_translation:vocabtranslate                                           Idle   0    0    0',
      'd7_taxonomy_term_entity_translation:vocabulary_name_much_longer_than_thirty_two_characters   Idle   0    0    0',
      'd7_taxonomy_term_localized_translation                                                       Idle   6    0    6',
      'd7_taxonomy_term_translation                                                                 Idle   4    0    4',
      'd7_taxonomy_vocabulary                                                                       Idle   8    0    8',
      'd7_taxonomy_vocabulary_translation                                                           Idle   7    0    7',
      'd7_theme_settings                                                                            Idle   2    0    2',
      'd7_url_alias                                                                                 Idle   6    0    6',
      'd7_user                                                                                      Idle   3    0    3',
      'd7_user_entity_translation                                                                   Idle   2    0    2',
      'd7_user_flood                                                                                Idle   1    0    1',
      'd7_user_mail                                                                                 Idle   1    0    1',
      'd7_user_mail_translation                                                                     Idle   1    0    1',
      'd7_user_role                                                                                 Idle   3    0    3',
      'd7_user_settings                                                                             Idle   1    0    1',
      'd7_user_settings_translation                                                                 Idle   1    0    1',
      'd7_view_modes                                                                                Idle   6    0    6',
      'default_language                                                                             Idle   1    0    1',
      'file_settings                                                                                Idle   1    0    1',
      'language                                                                                     Idle   3    0    3',
      'language_prefixes_and_domains                                                                Idle   3    0    3',
      'locale_settings                                                                              Idle   1    0    1',
      'menu_settings                                                                                Idle   1    0    1',
      'node_translation_menu_links                                                                  Idle   13   0    13',
      'system_image                                                                                 Idle   1    0    1',
      'system_image_gd                                                                              Idle   1    0    1',
      'system_logging                                                                               Idle   1    0    1',
      'system_maintenance                                                                           Idle   1    0    1',
      'system_rss                                                                                   Idle   1    0    1',
      'system_site                                                                                  Idle   1    0    1',
      'taxonomy_settings                                                                            Idle   1    0    1',
      'text_settings                                                                                Idle   1    0    1',
      'user_picture_entity_display                                                                  Idle   1    0    1',
      'user_picture_entity_form_display                                                             Idle   1    0    1',
      'user_picture_field                                                                           Idle   1    0    1',
      'user_picture_field_instance                                                                  Idle   1    0    1',
    ];

    // Drupal core 9.5.x+ migrates more block configs than previous versions.
    $expected_lines[] = version_compare(\Drupal::VERSION, '9.5.0-dev', 'ge')
      ? 'd7_block   Idle   52   0   52'
      : 'd7_block   Idle   50   0   50';

    $this->assertDrushOutputHasAllLines($expected_lines);
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $module_installer = \Drupal::service('module_installer');
    assert($module_installer instanceof ModuleInstallerInterface);

    // Uninstall RDF if present.
    if (\Drupal::moduleHandler()->moduleExists('rdf')) {
      $module_installer->uninstall(['rdf']);
    }

    // Remove Shortcut module.
    if (\Drupal::moduleHandler()->moduleExists('shortcut')) {
      foreach (\Drupal::entityTypeManager()->getStorage('shortcut')->loadMultiple() as $shortcut) {
        $shortcut->delete();
      }
      foreach (\Drupal::entityTypeManager()->getStorage('shortcut_set')->loadMultiple() as $shortcut_set) {
        $shortcut_set->delete();
      }
      $module_installer->uninstall(['shortcut']);
    }

    // The 'default' and 'migrate' DB key strings are 7 chars long. Using a
    // different length ensures that the migration connection key used for
    // testing cannot be 'default' or 'migrate'.
    $source_db_key = $this->randomMachineName(8);
    $this->createSourceMigrationConnection($source_db_key);
    $this->sourceDatabase = Database::getConnection('default', $source_db_key);
    $this->loadFixture($this->getFixtureFilePath());

    // Delete preexisting node types.
    $node_types = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();
    foreach ($node_types as $node_type) {
      $node_type->delete();
    }
  }

}
