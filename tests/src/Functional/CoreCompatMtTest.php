<?php

namespace Drupal\Tests\smart_migrate_cli\Functional;

use Drupal\Core\Database\Database;
use Drupal\Core\Site\Settings;
use Drupal\Tests\migmag\Traits\MigMagExportTrait;

/**
 * Tests migration with multi-thread Drush migrate command.
 *
 * @group smart_migrate_cli
 */
class CoreCompatMtTest extends CoreCompatibilityWithFixesTest {

  use MigMagExportTrait {
    removeDynamicEntityValues as traitRemoveDynamicEntityValues;
    getBaseExportAssets as traitGetBaseExportAssets;
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    if ($this->getName() === 'testMigrationOrder') {
      $this->markTestSkipped('It is unnecessary to test migration order in this test - we are testing multi thread import here.');
    }
    parent::setUp();

    if (Database::getConnection()->driver() === 'mysql') {
      // Ensure database is set up with READ COMMITTED isolation level.
      // Unfortunately this is also necessary on Drupal CI.
      $connection_info = Database::getConnectionInfo();
      foreach (array_keys($connection_info) as $target) {
        $connection_info[$target]['init_commands']['isolation'] = 'SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED';
        $settings['databases']['default'][$target] = (object) [
          'value' => $connection_info[$target],
          'required' => TRUE,
        ];
      }
      $this->writeSettings($settings);
      // Database::removeConnection('default');
      // Since Drupal is bootstrapped already, install_begin_request() will not
      // bootstrap again. Hence, we have to reload the newly written custom
      // settings.php manually.
      Settings::initialize(DRUPAL_ROOT, $this->siteDirectory, $this->classLoader);

      Database::removeConnection('default');
      foreach ($connection_info as $target => $info) {
        Database::addConnectionInfo('default', $target, $info);
      }

      $isolation_level_q = version_compare(Database::getConnection()->version(), '8.0.0', 'lt')
        ? 'SELECT @@tx_isolation;'
        : 'SELECT @@transaction_isolation;';
      $this->assertTrue(in_array(
        Database::getConnection()->query($isolation_level_q)->fetchField(),
        ['READ_COMMITTED', 'READ-COMMITTED']
      ));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function executeDrushImport(): void {
    $this->drush(
      'smart-migrate:multi-thread-import',
      ['--quiet'],
      ['threads' => 3, 'tag' => 'Drupal 7']
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function executeDrupal7MigrationWithDrush(): void {
    parent::executeDrupal7MigrationWithDrush();

    $expected_report =
      "+---------------------------------------------------------------------+--------+\n" .
      "| Error                                                               | Count  |\n" .
      "+---------------------------------------------------------------------+--------+\n" .
      "| d7_filter_format                                                             |\n" .
      "+---------------------------------------------------------------------+--------+\n" .
      "|  [error]  Missing filter plugin: filter_null.                       | 1      |\n" .
      "+---------------------------------------------------------------------+--------+\n" .
      "| d7_menu_links                                                                |\n" .
      "+---------------------------------------------------------------------+--------+\n" .
      "|  [warning] d7_menu_links migration: ID map plugin reports 2 failed  | 1      |\n" .
      "| rows.                                                               |        |\n" .
      "+---------------------------------------------------------------------+--------+";

    if (version_compare(\Drupal::VERSION, '9.5.0-dev', 'lt')) {
      $expected_report .= "\n" .
        "| d7_theme_settings                                                            |\n" .
        "+---------------------------------------------------------------------+--------+\n" .
        "|  [error]  No schema for bartik.settings (<DRUPAL_ROOT>/core/lib/Dru | 1      |\n" .
        "| pal/Core/Config/Development/ConfigSchemaChecker.php:87)             |        |\n" .
        "|  [error]  No schema for seven.settings (<DRUPAL_ROOT>/core/lib/Drup | 1      |\n" .
        "| al/Core/Config/Development/ConfigSchemaChecker.php:87)              |        |\n" .
        "|  [warning] d7_theme_settings migration: ID map plugin reports 2 fai | 1      |\n" .
        "| led rows.                                                           |        |\n" .
        "+---------------------------------------------------------------------+--------+";
    }

    $this->assertEquals($expected_report, $this->getOutput());

    $this->assertEmpty($this->getErrorOutput());
  }

  /**
   * Removes certain dynamic entity instance values depending on the type.
   *
   * If we execute migrations in multiple threads simultaneously, nothing
   * guarantees that follow up migrations are executed in the same order as they
   * were generated. This isn't an issue with revisionable content entities -
   * but in case of terms, "d7_entity_reference_translation" migration does not
   * add the revision ID to migration processes, meaning that when the changed
   * entity reference fields are saved, "d7_entity_reference_translation"
   * creates new taxonomy term revision instead of updating the most recent one.
   *
   * Similar thing happens to user's "content_translation_created" property.
   *
   * @param array $values
   *   Entity values, keyed by property.
   * @param string $entity_type
   *   The entity type ID of the values array.
   *
   * @return array
   *   Entity values without the dynamic properties.
   */
  protected function removeDynamicEntityValues(array $values, $entity_type) {
    switch ($entity_type) {
      case 'taxonomy_term':
        unset($values['revision_id']);
        break;

      case 'user':
        unset($values['content_translation_created']);
        break;
    }

    return $this->traitRemoveDynamicEntityValues($values, $entity_type);
  }

  /**
   * Returns data about the base export.
   *
   * @param string|null $entity_type
   *   The entity type ID.
   * @param string|null $bundle
   *   The bundle of the entities, if any.
   *
   * @return string[]|array[]
   *   Data of the base export
   */
  protected function getBaseExportAssets($entity_type = NULL, $bundle = NULL): array {
    $assets = $this->traitGetBaseExportAssets($entity_type, $bundle);
    if ($entity_type === 'path_alias') {
      // Smart Migrate CLI executes path_alias migrations at the end of the
      // migration process. But core doesn't do this: it executes d7_url_alias
      // before taxonomy term translation are imported. If the alias of the
      // translated taxonomy term "4" is imported before its translations are
      // migrated - while the term translations are saved - Drupal also saves a
      // path_alias translation for the translations.
      // This is why the multi-thread migration ends in 2 less path_alias
      // entities.
      // BUT! Since all the path_aliases migrated with Smart Migrate CLI's
      // multi thread migration are created by a path alias migration, this is
      // the right result imho.
      // So we will identify the "first" path_alias of taxonomy term "4", and
      // unset all the other aliases.
      $term_4_aliases = array_filter(
        $assets,
        function (array $revisions) {
          return reset($revisions)['path'] === '/taxonomy/term/4';
        }
      );
      ksort($term_4_aliases);
      array_shift($term_4_aliases);
      foreach (array_keys($term_4_aliases) as $filename) {
        unset($assets[$filename]);
      }
    }

    return $assets;
  }

}
