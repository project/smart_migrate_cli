<?php

namespace Drupal\Tests\smart_migrate_cli\Functional;

use Drupal\Core\Database\Database;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\Tests\smart_migrate_cli\Traits\MigrateMapTestTrait;

/**
 * Tests 'smart-migrate:diagnose' command.
 *
 * @group smart_migrate_cli
 */
class DiagnoseTest extends DrushMigrateTestBase {

  use MigrateMapTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'migrate',
    'migrate_drupal',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // The 'default' and 'migrate' DB key strings are 7 chars long. Using a
    // different length ensures that the migration connection key used for
    // testing cannot be 'default' or 'migrate'.
    $source_db_key = $this->randomMachineName(8);
    $this->createSourceMigrationConnection($source_db_key);
    $this->sourceDatabase = Database::getConnection('default', $source_db_key);
    $fixture_file_path = $this->getFixtureFilePath();
    if (!empty($fixture_file_path)) {
      $this->loadFixture($fixture_file_path);
    }
  }

  /**
   * Tests 'smart-migrate:diagnose' command.
   *
   * @dataProvider providerDiagnoseTest
   */
  public function testDiagnose(array $map_records, array $message_records, array $command_params, $expected_output_lines): void {
    $this->populateMapTables($map_records);
    $this->populateMessageTables($message_records);

    $this->drush('smart-migrate:diagnose', ...$command_params);
    $this->assertEmpty($this->getErrorOutput());

    $this->assertEquals(
      $expected_output_lines,
      $this->getOutput()
    );
  }

  /**
   * Data provider for ::testDiagnose.
   *
   * @return array[]
   *   The test cases.
   */
  public function providerDiagnoseTest(): array {
    return [
      'Nothing to diagnose' => [
        'map records' => [],
        'message records' => [],
        'command params' => [[], ['tag' => 'Drupal 7']],
        'expected output' => <<<EOF
 ----------- -------------- ------------------- ------- ---------
  Plugin ID   Source ID(s)   Destination ID(s)   Level   Message
 ----------- -------------- ------------------- ------- ---------
EOF
        ,
      ],
      'Diagnose failed rows of article migration with full ID' => [
        'map records' => ResetFailedTest::TEST_MIGRATE_MAP_RECORDS,
        'message records' => ResetFailedTest::TEST_MIGRATE_MESSAGE_RECORDS,
        'command params' => [['d7_node_complete:article']],
        'expected output' => <<<EOF
 -------------- ------------------- ------- -----------------------------------
  Source ID(s)   Destination ID(s)   Level   Message
 -------------- ------------------- ------- -----------------------------------
  1 : 1 : en                         1       This row did fail for some reason
  1 : 1 : en                         4       Informational message
  3 : 3 : it
 -------------- ------------------- ------- -----------------------------------
EOF
        ,
      ],
      'Diagnose all failed node migration with base ID' => [
        'map records' => ResetFailedTest::TEST_MIGRATE_MAP_RECORDS,
        'message records' => ResetFailedTest::TEST_MIGRATE_MESSAGE_RECORDS,
        'command params' => [['d7_node_complete']],
        'expected output' => <<<EOF
 -------------------------- -------------- ------------------- ------- -----------------------------------
  Plugin ID                  Source ID(s)   Destination ID(s)   Level   Message
 -------------------------- -------------- ------------------- ------- -----------------------------------
  d7_node_complete:article   1 : 1 : en                         1       This row did fail for some reason
  d7_node_complete:article   1 : 1 : en                         4       Informational message
  d7_node_complete:article   3 : 3 : it
  d7_node_complete:page      4 : 4 : en                         4       Failed page migration message
 -------------------------- -------------- ------------------- ------- -----------------------------------
EOF
      ],
      'Diagnose all migrations with tag' => [
        'map records' => ResetFailedTest::TEST_MIGRATE_MAP_RECORDS,
        'message records' => ResetFailedTest::TEST_MIGRATE_MESSAGE_RECORDS,
        'command params' => [[], ['tag' => 'Drupal 7', 'status' => 'all']],
        'expected output' => <<<EOF
 -------------------------- -------------- ------------------- ------------ ------- -----------------------------------
  Plugin ID                  Source ID(s)   Destination ID(s)   Row status   Level   Message
 -------------------------- -------------- ------------------- ------------ ------- -----------------------------------
  d7_field                   bar : file                         3            1       Failed to migrate 'bar' field
  d7_field                   bar : file                         3            4       Message set by skip_on_empty
  d7_field                   baz : file                         3
  d7_field                   foo : node     foo : node          0
  d7_node_complete:article   1 : 1 : en                         3            1       This row did fail for some reason
  d7_node_complete:article   1 : 1 : en                         3            4       Informational message
  d7_node_complete:article   2 : 2 : hu     2 : 2 : hu          0
  d7_node_complete:article   3 : 3 : it                         3
  d7_node_complete:page      4 : 4 : en                         3            4       Failed page migration message
 -------------------------- -------------- ------------------- ------------ ------- -----------------------------------
EOF
      ],
      'Get data about node migration rows with error message' => [
        'map records' => ResetFailedTest::TEST_MIGRATE_MAP_RECORDS,
        'message records' => ResetFailedTest::TEST_MIGRATE_MESSAGE_RECORDS,
        'command params' => [
          ['d7_node_complete,d7_field'],
          ['level' => MigrationInterface::MESSAGE_ERROR],
        ],
        'expected output' => <<<EOF
 -------------------------- -------------- ------------------- -----------------------------------
  Plugin ID                  Source ID(s)   Destination ID(s)   Message
 -------------------------- -------------- ------------------- -----------------------------------
  d7_field                   bar : file                         Failed to migrate 'bar' field
  d7_node_complete:article   1 : 1 : en                         This row did fail for some reason
 -------------------------- -------------- ------------------- -----------------------------------
EOF
      ],
      'Get data about config migration rows with info message' => [
        'map records' => ResetFailedTest::TEST_MIGRATE_MAP_RECORDS,
        'message records' => ResetFailedTest::TEST_MIGRATE_MESSAGE_RECORDS,
        'command params' => [
          [],
          [
            'tag' => 'Configuration',
            'level' => MigrationInterface::MESSAGE_INFORMATIONAL,
          ],
        ],
        'expected output' => <<<EOF
 ----------- -------------- ------------------- ------------------------------
  Plugin ID   Source ID(s)   Destination ID(s)   Message
 ----------- -------------- ------------------- ------------------------------
  d7_field    bar : file                         Message set by skip_on_empty
 ----------- -------------- ------------------- ------------------------------
EOF
      ],
      'Diagnose failed migrations and shorten messages' => [
        'map records' => ResetFailedTest::TEST_MIGRATE_MAP_RECORDS,
        'message records' => ResetFailedTest::TEST_MIGRATE_MESSAGE_RECORDS,
        'command params' => [
          [],
          [
            'tag' => 'Drupal 7',
            'status' => '3',
            'shorten-messages' => '23',
          ],
        ],
        'expected output' => <<<EOF
 -------------------------- -------------- ------------------- ------- -------------------------
  Plugin ID                  Source ID(s)   Destination ID(s)   Level   Message
 -------------------------- -------------- ------------------- ------- -------------------------
  d7_field                   bar : file                         1       Failed to migrate 'bar…
  d7_field                   bar : file                         4       Message set by skip_on…
  d7_field                   baz : file
  d7_node_complete:article   1 : 1 : en                         1       This row did fail for …
  d7_node_complete:article   1 : 1 : en                         4       Informational message
  d7_node_complete:article   3 : 3 : it
  d7_node_complete:page      4 : 4 : en                         4       Failed page migration …
 -------------------------- -------------- ------------------- ------- -------------------------
EOF
      ],
    ];
  }

}
