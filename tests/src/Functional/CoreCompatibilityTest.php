<?php

namespace Drupal\Tests\smart_migrate_cli\Functional;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormState;
use Drupal\migrate_drupal_ui\Form\CredentialForm;
use Drupal\smart_migrate_cli\Commands\MigrateRunnerCommands;
use Drupal\Tests\migmag\Functional\MigMagCoreMigrationTestBase;
use Drupal\Tests\migmag\Traits\MigMagMigrationTestDatabaseTrait;
use Drupal\Tests\smart_migrate_cli\Traits\EmeExportTrait;
use Drush\Log\DrushLoggerManager;
use Symfony\Component\Console\Output\NullOutput;

/**
 * Tests Drupal 7 upgrade using Smart Migrate CLI.
 *
 * This test executes the migration of the Drupal 7 DB fixture twice.
 *
 * After the first migration finishes, the content entities' default revision
 * is exported to an EME-generated module (into JSON files) to a location
 * which is retained between test cases are executed.
 *
 * The second case repeats the same migration process with Drush and Smart
 * Migrate CLI, creates another export, and compares the end result of
 * the two JSON data set.
 *
 * @group smart_migrate_cli
 */
class CoreCompatibilityTest extends MigMagCoreMigrationTestBase {

  use MigMagMigrationTestDatabaseTrait;
  use EmeExportTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'smart_migrate_cli',
  ];

  /**
   * {@inheritdoc}
   */
  public function useTestMailCollector() {
  }

  /**
   * {@inheritdoc}
   */
  protected function assertEmailsSent() {
  }

  /**
   * Executes the test of Drupal 7 migration and saves content output.
   */
  public function testCreateBaseDrupal7MigrationDump(): void {
    $this->executeDrupal7Migration();
    $this->ensureBaseExportIsPresent();
    $this->assertTrue(TRUE);
  }

  /**
   * Checks whether SMC does the same as core content migrations.
   *
   * @depends testCreateBaseDrupal7MigrationDump
   */
  public function testCoreCompatibility(): void {
    $this->executeDrupal7MigrationWithDrush();
    $this->createActualExport();
    $this->compareAllEntityContentExportSets();
    $this->removeTempBaseExportModule();
  }

  /**
   * Executes migration import with Smart Migrate CLI's Drush command.
   */
  protected function executeDrupal7MigrationWithDrush(): void {
    // Set source base file path.
    $this->writeSettings([
      'settings' => [
        'source_base_path' => (object) [
          'value' => implode(DIRECTORY_SEPARATOR, [
            DRUPAL_ROOT,
            'core',
            'modules',
            'migrate_drupal_ui',
            'tests',
            'src',
            'Functional',
            'd7',
            'files',
          ]),
          'required' => TRUE,
        ],
      ],
    ]);

    // The 'default' and 'migrate' DB key strings are 7 chars long. Using a
    // different length ensures that the migration connection key used for
    // testing cannot be 'default' or 'migrate'.
    $source_db_key = $this->randomMachineName(8);
    $this->createSourceMigrationConnection($source_db_key);
    $this->sourceDatabase = Database::getConnection('default', $source_db_key);

    $this->executeDrushImport();
  }

  /**
   * Executed migration with Drush.
   */
  protected function executeDrushImport(): void {
    $this->drush(
      'migrate:import',
      ['--no-progress'],
      ['tag' => 'Drupal 7']
    );
  }

  /**
   * Test that our Drush migrate import command calculates the same order as UI.
   */
  public function testMigrationOrder(): void {
    // Create connection to the source Drupal 7 instance.
    $source_db_key = $this->randomMachineName(8);
    $this->createSourceMigrationConnection($source_db_key);
    $this->sourceDatabase = Database::getConnection('default', $source_db_key);

    // Get the Drupal 7 migration IDs in the same order how Migrate Drupal UI
    // executes them.
    // Instantiate Migrate Drupal UI's credential form.
    $credential_form = CredentialForm::create(\Drupal::getContainer());
    $form_ref = new \ReflectionClass($credential_form);
    // The 'setupMigrations' method of the form returns the filtered and ordered
    // list of migrations.
    $setup_migrations = $form_ref->getMethod('setupMigrations');
    $setup_migrations->setAccessible(TRUE);
    $database = Database::getConnectionInfo($source_db_key)['default'];
    $form_state = new FormState();
    // Let's invoke the method then get the calculated migrations from the
    // form's private temporary store.
    $setup_migrations->invoke($credential_form, $this->sourceDatabase, '7', $database, $form_state);
    $store_ref = $form_ref->getProperty('store');
    $store_ref->setAccessible(TRUE);
    $store = $store_ref->getValue($credential_form);
    // Migrations are stored at the 'migrations' key.
    $migrate_drupal_ui_migrations_ordered = $store->get('migrations');
    // This list is an array of Migration plugin instance labels keyed by the
    // migration plugin ID. We only need the IDs.
    $ui_ordered_ids = array_keys($migrate_drupal_ui_migrations_ordered);

    // Get the order of the migrations used by our Drush command.
    $command = new MigrateRunnerCommands(
      \Drupal::service('date.formatter'),
      \Drupal::service('keyvalue'),
      \Drupal::service('account_switcher'),
      \Drupal::service('entity_type.manager'),
      \Drupal::service('smart_migrate_cli.plugin.manager.migration.simple')
    );
    // Set logger and output.
    $command->setLogger(new DrushLoggerManager());
    $command->setOutput(new NullOutput());
    $command_ref = new \ReflectionClass($command);
    // The import command uses 'getMigrationList' to get the migrations.
    $get_list_method = $command_ref->getMethod('getMigrationList');
    $get_list_method->setAccessible(TRUE);
    // This command returns an array of migration plugin instances, keyed by the
    // appropriate migration plugin tags.
    $drush_ordered_grouped_list = $get_list_method->invoke($command, NULL, 'Drupal 7', TRUE);
    $this->assertSame('Drupal 7', key($drush_ordered_grouped_list));
    $drush_ordered_ids = array_keys(reset($drush_ordered_grouped_list));

    // The most important assertion in this test: order of IDs must be the same.
    $this->assertSame(
      $ui_ordered_ids,
      $drush_ordered_ids
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    $this->cleanupSourceMigrateConnection($this->sourceDatabase->getKey());
    parent::tearDown();
  }

  /**
   * Returns the path to the DB fixture file.
   *
   * @return string
   *   The path to the DB fixture file.
   */
  protected function getFixtureFilePath(): string {
    return implode(
      DIRECTORY_SEPARATOR,
      [
        DRUPAL_ROOT,
        'core',
        'modules',
        'migrate_drupal',
        'tests',
        'fixtures',
        'drupal7.php',
      ]
    );
  }

  /**
   * Loads a database fixture into the source database connection.
   *
   * This method is the exact copy of
   * Drupal\Tests\migrate\Kernel\MigrateTestBase::loadFixture().
   *
   * @param string $path
   *   Path to the dump file.
   */
  protected function loadFixture($path): void {
    $default_db = Database::getConnection()->getKey();
    Database::setActiveConnection($this->sourceDatabase->getKey());

    if (substr($path, -3) == '.gz') {
      $path = 'compress.zlib://' . $path;
    }
    require $path;

    Database::setActiveConnection($default_db);
  }

}
