<?php

namespace Drupal\Tests\smart_migrate_cli\Functional;

use Drupal\Core\Database\Database;
use Drupal\migrate\Plugin\MigrateIdMapInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\smart_migrate_cli\Commands\SmartMigrateCliCommands;
use Drupal\Tests\smart_migrate_cli\Traits\MigrateMapTestTrait;

/**
 * Tests 'smart-migrate:reset-failed' command.
 *
 * @group smart_migrate_cli
 */
class ResetFailedTest extends DrushMigrateTestBase {

  use MigrateMapTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'migrate',
    'migrate_drupal',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // The 'default' and 'migrate' DB key strings are 7 chars long. Using a
    // different length ensures that the migration connection key used for
    // testing cannot be 'default' or 'migrate'.
    $source_db_key = $this->randomMachineName(8);
    $this->createSourceMigrationConnection($source_db_key);
    $this->sourceDatabase = Database::getConnection('default', $source_db_key);
    $fixture_file_path = $this->getFixtureFilePath();
    if (!empty($fixture_file_path)) {
      $this->loadFixture($fixture_file_path);
    }
  }

  /**
   * Test reset failed.
   *
   * @dataProvider providerValidTest
   */
  public function testReset(array $map_records, array $message_records, array $reset_command_params, array $expected_map_count, array $expected_message_count): void {
    $manager = \Drupal::service('plugin.manager.migration');
    assert($manager instanceof MigrationPluginManagerInterface);

    $this->populateMapTables($map_records);
    $this->populateMessageTables($message_records);

    $this->drush('smart-migrate:reset-failed', ...$reset_command_params);
    $this->assertEmpty($this->getErrorOutput());

    foreach ($expected_map_count as $migration_plugin_id => $expected_count) {
      $migration = $manager->createInstance($migration_plugin_id);
      $this->assertInstanceOf(MigrationInterface::class, $migration);
      $this->assertSame(
        $expected_count,
        $migration->getIdMap()->processedCount(),
        sprintf(
          "Expected map count (%s) does not match for '%s' migration.",
          $expected_count,
          $migration_plugin_id
        )
      );
    }

    foreach ($expected_message_count as $migration_plugin_id => $expected_count) {
      $migration = $manager->createInstance($migration_plugin_id);
      $this->assertInstanceOf(MigrationInterface::class, $migration);
      $this->assertSame(
        $expected_count,
        $migration->getIdMap()->messageCount(),
        sprintf(
          "Expected message count ('%s') does not match for '%s' migration.",
          $expected_count,
          $migration_plugin_id
        ));
    }
  }

  /**
   * Data provider for ::testValidOperations.
   *
   * @return array[]
   *   The test cases.
   */
  public function providerValidTest(): array {
    return [
      'Nothing to reset' => [
        'map records' => [],
        'message records' => [],
        'reset command params' => [[], ['tag' => 'Drupal 7']],
        'expected map count after' => [],
        'expected message count after' => [],
      ],
      'Reset article migration with full ID' => [
        'map records' => static::TEST_MIGRATE_MAP_RECORDS,
        'message records' => static::TEST_MIGRATE_MESSAGE_RECORDS,
        'reset command params' => [['d7_node_complete:article']],
        'expected map count after' => [
          'd7_node_complete:article' => 1,
          'd7_node_complete:page' => 1,
          'd7_field' => 3,
        ],
        'expected message count after' => [
          'd7_node_complete:article' => 0,
          'd7_node_complete:page' => 1,
          'd7_field' => 2,
        ],
      ],
      'Reset all node migration with base ID' => [
        'map records' => static::TEST_MIGRATE_MAP_RECORDS,
        'message records' => static::TEST_MIGRATE_MESSAGE_RECORDS,
        'reset command params' => [['d7_node_complete']],
        'expected map count after' => [
          'd7_node_complete:article' => 1,
          'd7_node_complete:page' => 0,
          'd7_field' => 3,
        ],
        'expected message count after' => [
          'd7_node_complete:article' => 0,
          'd7_node_complete:page' => 0,
          'd7_field' => 2,
        ],
      ],
      'Reset migrations with tag' => [
        'map records' => static::TEST_MIGRATE_MAP_RECORDS,
        'message records' => static::TEST_MIGRATE_MESSAGE_RECORDS,
        'reset command params' => [[], ['tag' => 'Drupal 7']],
        'expected map count after' => [
          'd7_node_complete:article' => 1,
          'd7_node_complete:page' => 0,
          'd7_field' => 1,
        ],
        'expected message count after' => [
          'd7_node_complete:article' => 0,
          'd7_node_complete:page' => 0,
          'd7_field' => 0,
        ],
      ],
    ];
  }

  /**
   * Tests error "messages".
   *
   * @dataProvider providerErrorTest
   */
  public function testErrors(array $command_params, string $expected_error):void {
    $this->drush('smart-migrate:reset-failed', ...$command_params);
    $this->assertStringContainsString($expected_error, $this->getErrorOutput());
  }

  /**
   * Data provider for ::testErrors.
   *
   * @return array[]
   *   The test cases.
   */
  public function providerErrorTest(): array {
    return [
      'No migrations match the given tag' => [
        'command params' => [[], ['tag' => 'FooBar'], NULL, NULL, 1],
        'error' => sprintf(
          SmartMigrateCliCommands::MESSAGE_TEMPLATE_ERROR_NO_MIGRATIONS_PER_TAG,
          'FooBar',
        ),
      ],
      'Missing ID' => [
        'command params' => [
          ['d3_node:page,d4_node,d7_node_complete'],
          [],
          NULL,
          NULL,
          1,
        ],
        'error' => sprintf(
          SmartMigrateCliCommands::MESSAGE_TEMPLATE_ERROR_NO_MIGRATIONS_PER_IDS,
          "d3_node:page', 'd4_node"
        ),
      ],
      'No arguments' => [
        'command params' => [[], [], NULL, NULL, 1],
        'error' => SmartMigrateCliCommands::MESSAGE_TEMPLATE_ERROR_MISSING_TAG_OR_IDS,
      ],
      'Too many command params' => [
        'command params' => [['foo'], ['tag' => 'BarBaz'], NULL, NULL, 1],
        'error' => SmartMigrateCliCommands::MESSAGE_TEMPLATE_ERROR_TAG_AND_IDS_PRESENT,
      ],
    ];
  }

  /**
   * Migrate map records to test with.
   *
   * @const array
   */
  const TEST_MIGRATE_MAP_RECORDS = [
    'd7_node_complete:article' => [
      [
        ['nid' => 1, 'vid' => 1, 'language' => 'en'],
        [NULL, NULL, NULL],
        MigrateIdMapInterface::STATUS_FAILED,
        MigrateIdMapInterface::ROLLBACK_PRESERVE,
      ],
      [
        ['nid' => 2, 'vid' => 2, 'language' => 'hu'],
        [2, 2, 'hu'],
        MigrateIdMapInterface::STATUS_IMPORTED,
        MigrateIdMapInterface::ROLLBACK_DELETE,
      ],
      [
        ['nid' => 3, 'vid' => 3, 'language' => 'it'],
        [NULL, NULL, NULL],
        MigrateIdMapInterface::STATUS_FAILED,
        MigrateIdMapInterface::ROLLBACK_PRESERVE,
      ],
    ],
    'd7_node_complete:page' => [
      [
        ['nid' => 4, 'vid' => 4, 'language' => 'en'],
        [NULL, NULL, NULL],
        MigrateIdMapInterface::STATUS_FAILED,
        MigrateIdMapInterface::ROLLBACK_PRESERVE,
      ],
    ],
    'd7_field' => [
      [
        ['field_name' => 'foo', 'entity_type' => 'node'],
        ['foo', 'node'],
        MigrateIdMapInterface::STATUS_IMPORTED,
        MigrateIdMapInterface::ROLLBACK_DELETE,
      ],
      [
        ['field_name' => 'bar', 'entity_type' => 'file'],
        [NULL, NULL],
        MigrateIdMapInterface::STATUS_FAILED,
        MigrateIdMapInterface::ROLLBACK_PRESERVE,
      ],
      [
        ['field_name' => 'baz', 'entity_type' => 'file'],
        [NULL, NULL],
        MigrateIdMapInterface::STATUS_FAILED,
        MigrateIdMapInterface::ROLLBACK_PRESERVE,
      ],
    ],
  ];

  /**
   * Migrate message records to test with.
   *
   * @const array
   */
  const TEST_MIGRATE_MESSAGE_RECORDS = [
    'd7_node_complete:article' => [
      [
        ['nid' => 1, 'vid' => 1, 'language' => 'en'],
        'Informational message',
        MigrationInterface::MESSAGE_INFORMATIONAL,
      ],
      [
        ['nid' => 1, 'vid' => 1, 'language' => 'en'],
        'This row did fail for some reason',
        MigrationInterface::MESSAGE_ERROR,
      ],
    ],
    'd7_node_complete:page' => [
      [
        ['nid' => 4, 'vid' => 4, 'language' => 'en'],
        'Failed page migration message',
        MigrationInterface::MESSAGE_INFORMATIONAL,
      ],
    ],
    'd7_field' => [
      [
        ['field_name' => 'bar', 'entity_type' => 'file'],
        "Message set by skip_on_empty",
        MigrationInterface::MESSAGE_INFORMATIONAL,
      ],
      [
        ['field_name' => 'bar', 'entity_type' => 'file'],
        "Failed to migrate 'bar' field",
        MigrationInterface::MESSAGE_ERROR,
      ],
    ],
  ];

}
