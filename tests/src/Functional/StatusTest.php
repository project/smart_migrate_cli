<?php

namespace Drupal\Tests\smart_migrate_cli\Functional;

use Drupal\Core\Extension\ModuleInstallerInterface;

/**
 * Tests the status command.
 *
 * @group smart_migrate_cli
 */
class StatusTest extends DrushMigrateTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'smart_migrate_cli',
  ];

  /**
   * {@inheritdoc}
   */
  protected string $consoleWidth = '200';

  /**
   * Tests 'smart-migrate:status' command.
   *
   * @dataProvider statusTestProvider
   */
  public function testStatus(array $modules_to_install, bool $with_source_db, array $expected_lines_regex): void {
    if ($modules_to_install) {
      $installer = \Drupal::service('module_installer');
      assert($installer instanceof ModuleInstallerInterface);
      $this->assertTrue($installer->install($modules_to_install));
    }

    if ($with_source_db) {
      // Verify that the command works accordingly if we have a DB connection.
      $source_db_key = $this->randomMachineName(8);
      $this->createSourceMigrationConnection($source_db_key);
    }

    $this->drush('smart-migrate:status');
    $this->assertEmpty($this->getErrorOutput());
    $output = $this->getOutput();

    foreach ($expected_lines_regex as $line_regex) {
      if ($with_source_db) {
        $line_regex = str_replace('<<expected-source-key>>', $source_db_key, $line_regex);
      }
      $this->assertMatchesRegularExpression($line_regex, $output);
    }
    $this->assertCount(count($expected_lines_regex), explode(PHP_EOL, $output));
  }

  /**
   * Data provider for ::testStatus.
   *
   * @return array[]
   *   The test cases.
   */
  public function statusTestProvider(): array {
    return [
      'No extra modules, no source DB' => [
        'extra modules' => [],
        'source DB' => FALSE,
        'regex of expected output lines' => [
          '/\bPHP memory\s*:\s*-?\d+/',
          '/\bHost DB URI\s*:\s*\w+:\/\//',
          '/\bHost connection key \(target\)\s*:\s*default \(default\)/',
          '/\bHost isolation level\s*:\s*\w+/',
          '/\bSource DB\s*:\s*NaN/',
        ],
      ],
      'No extra modules, with source DB' => [
        'extra modules' => [],
        'source DB' => TRUE,
        'regex of expected output lines' => [
          '/\bPHP memory\s*:\s*-?\d+/',
          '/\bHost DB URI\s*:\s*\w+:\/\//',
          '/\bHost connection key \(target\)\s*:\s*default \(default\)/',
          '/\bHost isolation level\s*:\s*\w+/',
          '/\bSource DB\s*:\s*NaN/',
        ],
      ],
      'With Migrate module, no source DB' => [
        'extra modules' => ['migrate'],
        'source DB' => FALSE,
        'regex of expected output lines' => [
          '/\bPHP memory\s*:\s*-?\d+/',
          '/\bHost DB URI\s*:\s*\w+:\/\//',
          '/\bHost connection key \(target\)\s*:\s*default \(default\)/',
          '/\bHost isolation level\s*:\s*\w+/',
          '/\bSource DB\s*:\s*Not connected/',
          '/\bSource connection key \(target\)\s*:\s*migrate \(default\)/',
        ],
      ],
      'With Migrate module and source DB' => [
        'extra modules' => ['migrate'],
        'source DB' => TRUE,
        'regex of expected output lines' => [
          '/\bPHP memory\s*:\s*-?\d+/',
          '/\bHost DB URI\s*:\s*\w+:\/\//',
          '/\bHost connection key \(target\)\s*:\s*default \(default\)/',
          '/\bHost isolation level\s*:\s*\w+/',
          '/\bSource DB\s*:\s*Connected/',
          '/\bSource DB URI\s*:\s*\w+:\/\//',
          '/\bSource connection key \(target\)\s*:\s*<<expected-source-key>> \(default\)/',
          '/\bSource isolation level\s*:\s*\w+/',
          '/\bSource and ID Map joinable\s*:\s*NaN/',
        ],
      ],
      'With Migrate Drupal module, no source DB' => [
        'extra modules' => ['migrate_drupal'],
        'source DB' => FALSE,
        'regex of expected output lines' => [
          '/\bPHP memory\s*:\s*-?\d+/',
          '/\bHost DB URI\s*:\s*\w+:\/\//',
          '/\bHost connection key \(target\)\s*:\s*default \(default\)/',
          '/\bHost isolation level\s*:\s*\w+/',
          '/\bSource DB\s*:\s*Not connected/',
          '/\bSource connection key \(target\)\s*:\s*migrate \(default\)/',
        ],
      ],
      'With Migrate Drupal module and source DB' => [
        'extra modules' => ['migrate_drupal'],
        'source DB' => TRUE,
        'regex of expected output lines' => [
          '/\bPHP memory\s*:\s*-?\d+/',
          '/\bHost DB URI\s*:\s*\w+:\/\//',
          '/\bHost connection key \(target\)\s*:\s*default \(default\)/',
          '/\bHost isolation level\s*:\s*\w+/',
          '/\bSource DB\s*:\s*Connected/',
          '/\bSource DB URI\s*:\s*\w+:\/\//',
          '/\bSource connection key \(target\)\s*:\s*<<expected-source-key>> \(default\)/',
          '/\bSource isolation level\s*:\s*\w+/',
          '/\bSource and ID Map joinable\s*:\s*Yes/',
        ],
      ],
    ];
  }

}
