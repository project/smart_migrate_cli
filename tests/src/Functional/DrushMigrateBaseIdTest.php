<?php

namespace Drupal\Tests\smart_migrate_cli\Functional;

use Drupal\Core\Database\Database;

/**
 * Verifies that inherited Drush migrate commands accept base plugin IDs.
 *
 * @group smart_migrate_cli
 */
class DrushMigrateBaseIdTest extends DrushMigrateTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'language',
    'menu_ui',
    'migrate',
    'migrate_drupal',
    'node',
    'smart_migrate_cli',
    'taxonomy',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    // The 'default' and 'migrate' DB key strings are 7 chars long. Using a
    // different length ensures that the migration connection key used for
    // testing cannot be 'default' or 'migrate'.
    $source_db_key = $this->randomMachineName(8);
    $this->createSourceMigrationConnection($source_db_key);
    $this->sourceDatabase = Database::getConnection('default', $source_db_key);
    $fixture_file_path = $this->getFixtureFilePath();
    if (!empty($fixture_file_path)) {
      $this->loadFixture($fixture_file_path);
    }
  }

  /**
   * Tests migrate:status command.
   */
  public function testDrushMigrateStatus(): void {
    // Get status of existing migration.
    $this->drush(
      'migrate:status',
      ['d7_node_complete,d7_taxonomy_term:tags'],
    );
    $this->assertEmpty($this->getErrorOutput());

    $this->assertEquals(
      <<<EOL
 --------------------------------------------------- -------- ------- ---------- ------------- ---------------
  Migration ID                                        Status   Total   Imported   Unprocessed   Last Imported
 --------------------------------------------------- -------- ------- ---------- ------------- ---------------
  d7_node_complete:a_thirty_two_character_type_name   Idle     0       0          0
  d7_node_complete:article                            Idle     8       0          8
  d7_node_complete:blog                               Idle     3       0          3
  d7_node_complete:book                               Idle     0       0          0
  d7_node_complete:et                                 Idle     8       0          8
  d7_node_complete:forum                              Idle     2       0          2
  d7_node_complete:page                               Idle     0       0          0
  d7_node_complete:test_content_type                  Idle     1       0          1
  d7_taxonomy_term:tags                               Idle     10      0          10
 --------------------------------------------------- -------- ------- ---------- ------------- ---------------
EOL
      ,
      $this->getOutput()
    );

    // Try to get status of missing migrations.
    $this->drush(
      'migrate:status',
      ['_missing,please:dont:be:sad'],
      [],
      NULL,
      NULL,
      1
    );
    $this->assertEquals(
      <<<EOL
In MigrateRunnerTrait.php line 57:
  Invalid migration IDs: _missing, please:dont:be:sad
EOL
      ,
      $this->getErrorOutput()
    );
  }

  /**
   * Tests migrate:import command.
   */
  public function testDrushMigrateImport(): void {
    // Execute migrations (d7_node_complete is a base plugin ID!).
    $this->drush(
      'migrate:import',
      [
        'language,d7_user_role,d7_user,d7_node_type,d7_node_complete,d7_taxonomy_vocabulary,d7_taxonomy_term:tags',
        '--no-progress',
      ],
    );
    $error_output_without_time = preg_replace(
      '/\bin\s+\d+\.?\d?\s+seconds\s+\(.*\/min\)\s+-\s+done\s+with\s+/',
      '',
      $this->getErrorOutput()
    );
    // This is the order we get on Drupal core 10.1.x, which is much more
    // logical than the one on 9.5.x.
    // @todo Find the cause.
    $this->assertDrushOutputHasAllLines(
      [
        "[notice] Processed 3 items (3 created, 0 updated, 0 failed, 0 ignored) 'language'",
        "[notice] Processed 8 items (8 created, 0 updated, 0 failed, 0 ignored) 'd7_node_type'",
        "[notice] Processed 8 items (8 created, 0 updated, 0 failed, 0 ignored) 'd7_taxonomy_vocabulary'",
        "[notice] Processed 3 items (3 created, 0 updated, 0 failed, 0 ignored) 'd7_user_role'",
        "[notice] Processed 3 items (3 created, 0 updated, 0 failed, 0 ignored) 'd7_user'",
        "[notice] Processed 8 items (8 created, 0 updated, 0 failed, 0 ignored) 'd7_node_complete:article'",
        "[notice] Processed 0 items (0 created, 0 updated, 0 failed, 0 ignored) 'd7_node_complete:a_thirty_two_character_type_name'",
        "[notice] Processed 3 items (3 created, 0 updated, 0 failed, 0 ignored) 'd7_node_complete:blog'",
        "[notice] Processed 0 items (0 created, 0 updated, 0 failed, 0 ignored) 'd7_node_complete:book'",
        "[notice] Processed 8 items (8 created, 0 updated, 0 failed, 0 ignored) 'd7_node_complete:et'",
        "[notice] Processed 2 items (2 created, 0 updated, 0 failed, 0 ignored) 'd7_node_complete:forum'",
        "[notice] Processed 0 items (0 created, 0 updated, 0 failed, 0 ignored) 'd7_node_complete:page'",
        "[notice] Processed 1 item (1 created, 0 updated, 0 failed, 0 ignored) 'd7_node_complete:test_content_type'",
        "[notice] Processed 10 items (10 created, 0 updated, 0 failed, 0 ignored) 'd7_taxonomy_term:tags'",
      ],
      $error_output_without_time
    );
  }

  /**
   * Tests migrate:rollback command.
   *
   * @depends testDrushMigrateImport
   */
  public function testDrushMigrateRollback(): void {
    // We will perform the import of ::testDrushMigrateImport, then roll back
    // all taxonomy and article migrations.
    $this->testDrushMigrateImport();

    $this->drush(
      'migrate:rollback',
      [
        'd7_node_complete:article,d7_taxonomy_term',
        '--no-progress',
      ],
    );

    $this->assertSame(<<<EOS
 [notice] Rolled back 0 items - done with 'd7_taxonomy_term:vocablocalized2'
 [notice] Rolled back 0 items - done with 'd7_taxonomy_term:vocabfixed'
 [notice] Rolled back 0 items - done with 'd7_taxonomy_term:vocabtranslate'
 [notice] Rolled back 0 items - done with 'd7_taxonomy_term:vocablocalized'
 [notice] Rolled back 0 items - done with 'd7_taxonomy_term:vocabulary_name_much_longer_than_thirty_two_characters'
 [notice] Rolled back 0 items - done with 'd7_taxonomy_term:test_vocabulary'
 [notice] Rolled back 0 items - done with 'd7_taxonomy_term:sujet_de_discussion'
 [notice] Rolled back 10 items - done with 'd7_taxonomy_term:tags'
 [notice] Rolled back 8 items - done with 'd7_node_complete:article'
EOS
      ,
      $this->getErrorOutput()
    );
  }

}
