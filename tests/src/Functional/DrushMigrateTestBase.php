<?php

namespace Drupal\Tests\smart_migrate_cli\Functional;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\migmag\Traits\MigMagMigrationTestDatabaseTrait;
use Drush\TestTraits\DrushTestTrait;
use PHPUnit\Framework\ExpectationFailedException;

/**
 * Base class for testing migration related Drush commands.
 */
abstract class DrushMigrateTestBase extends BrowserTestBase {

  use DrushTestTrait {
    drush as traitDrush;
  }
  use MigMagMigrationTestDatabaseTrait;

  /**
   * Drush console width in tests.
   *
   * @var string|null
   */
  protected string $consoleWidth;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'smart_migrate_cli',
  ];

  /**
   * Executes Drush commands with the specified console width.
   */
  protected function drush($command, array $args = [], array $options = [], $site_specification = NULL, $cd = NULL, $expected_return = 0, $suffix = NULL, array $env = []): void {
    $env['COLUMNS'] = $this->consoleWidth ?? '140';
    $this->traitDrush($command, $args, $options, $site_specification, $cd, $expected_return, $suffix, $env);
  }

  /**
   * Returns the path to the DB fixture file.
   *
   * @return string
   *   The path to the DB fixture file.
   */
  protected function getFixtureFilePath(): string {
    return implode(
      DIRECTORY_SEPARATOR,
      [
        DRUPAL_ROOT,
        'core',
        'modules',
        'migrate_drupal',
        'tests',
        'fixtures',
        'drupal7.php',
      ]
    );
  }

  /**
   * Loads a database fixture into the source database connection.
   *
   * This method is the exact copy of
   * Drupal\Tests\migrate\Kernel\MigrateTestBase::loadFixture().
   *
   * @param string $path
   *   Path to the dump file.
   */
  protected function loadFixture($path): void {
    $default_db = Database::getConnection()->getKey();
    Database::setActiveConnection($this->sourceDatabase->getKey());

    if (substr($path, -3) == '.gz') {
      $path = 'compress.zlib://' . $path;
    }
    require $path;

    Database::setActiveConnection($default_db);
  }

  /**
   * Checks that migrate status output has all the lines.
   *
   * @param string|string[] $expected_lines
   *   The expected lines in drush output.
   * @param string $actual_output
   *   The actual output. Optional.
   */
  protected function assertDrushOutputHasAllLines($expected_lines, string $actual_output = NULL): void {
    $actual_output = $actual_output ?? $this->getOutput();
    $actual_output_simplified = $this->simplifyOutput($actual_output);

    try {
      foreach ((array) $expected_lines as $expected_line) {
        $expected_line_pieces = array_filter(
          explode(' ', $expected_line),
          function ($word) {
            return (string) $word !== '';
          }
        );
        foreach ($expected_line_pieces as $key => $text) {
          $expected_line_pieces[$key] = preg_quote((string) $text, '/');
        }
        $pattern_base = implode('\s+', $expected_line_pieces);
        if (preg_match('/[\w]/', mb_substr($pattern_base, 0, 1))) {
          $pattern_base = '\b' . $pattern_base;
        }
        if (preg_match('/[\w]/', mb_substr($pattern_base, -1))) {
          $pattern_base .= '\b';
        }
        $pattern = '/' . $pattern_base . '/';
        $this->assertEquals(1, preg_match($pattern, $actual_output_simplified));
      }
    }
    catch (ExpectationFailedException $e) {
      $this->assertEquals(implode("\n", $expected_lines), $actual_output);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    if (isset($this->sourceDatabase) && $this->sourceDatabase instanceof Connection) {
      $this->cleanupSourceMigrateConnection($this->sourceDatabase->getKey());
    }
    parent::tearDown();
  }

  /**
   * Returns the database URL of the given connection.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The connection.
   *
   * @return string
   *   The database URL representation of the given connection.
   */
  protected function getDatabaseUrl(Connection $connection): string {
    $connection_info = $connection->getConnectionOptions();
    $driver = $connection_info['driver'];
    $user = $connection_info['username'] ?? NULL;
    $pass = $connection_info['password'] ?? NULL;
    $prefix = $connection_info['prefix'];
    $host = strpos($connection_info['database'], DRUPAL_ROOT) === 0
      ? 'localhost'
      : $connection_info['host'] ?? NULL;
    $port = $connection_info['port'] ?? NULL;
    $db_name = strpos($connection_info['database'], DRUPAL_ROOT) === 0
      ? substr($connection_info['database'], strlen(DRUPAL_ROOT) + 1)
      : $connection_info['database'];
    $server = implode(
      '@',
      array_filter([
        implode(':', array_filter([$user, $pass])),
        implode(':', array_filter([$host, $port])),
      ])
    );
    $table_and_prefix = implode('#', array_filter([
      $db_name,
      $prefix,
    ]));
    return "$driver://$server/$table_and_prefix";
  }

  /**
   * {@inheritdoc}
   */
  public function getOutput() {
    return implode(
      PHP_EOL,
      array_filter(
        array_map(
          function (string $line) {
            return rtrim($line);
          },
          explode(PHP_EOL, $this->getOutputRaw())
        )
      )
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getErrorOutput() {
    return implode(
      PHP_EOL,
      array_filter(
        array_map(
          function (string $line) {
            return rtrim($line);
          },
          explode(PHP_EOL, $this->getErrorOutputRaw())
        )
      )
    );
  }

}
