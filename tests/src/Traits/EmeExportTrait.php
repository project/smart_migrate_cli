<?php

declare(strict_types = 1);

namespace Drupal\Tests\smart_migrate_cli\Traits;

use Drupal\Tests\migmag\Traits\MigMagExportTrait;

/**
 * Better trait for comparing EME exports.
 *
 * Comparison is performed in a single step rather than comparing entities after
 * each other. This gives a better overview what changed (or what does not work)
 * with the migrations.
 */
trait EmeExportTrait {

  use MigMagExportTrait;

  /**
   * Compares the base and the actual EME export data set.
   */
  protected function compareAllEntityContentExportSets() {
    $expected_list = $this->getBaseExportAssets();
    $actual_list = $this->getActualExportAssets();

    $this->assertNotEmpty(
      $expected_list,
      'Base set is empty' . $this->getTempBaseExportModuleLocation()
    );
    $this->assertNotEmpty(
      $actual_list,
      'Current set is empty' . $this->getActualExportModuleLocation()
    );
    $this->assertEquals($expected_list, $actual_list);

    $actual = [];
    $expected = [];

    foreach ($expected_list as $entity_type) {
      $expected_bundles = $this->getBaseExportAssets($entity_type);
      // If "$base_bundle_or_entity_revisions" is a multidimensional array,
      // then it is an array of an entity revisions.
      if (!is_array($expected_bundles[key($expected_bundles)])) {
        // Nodes, comment entities, taxonomy terms - all of these have bundles.
        foreach ($expected_bundles as $bundle) {
          $expected_entities_per_bundle = $this->getBaseExportAssets($entity_type, $bundle);
          $actual_entities_per_bundle = $this->getActualExportAssets($entity_type, $bundle);
          foreach ($expected_entities_per_bundle as $filename => $revision_values) {
            $expected[$entity_type][$bundle][$filename] = $this->cleanupDynamicEntityValues($revision_values, $entity_type);
          }
          foreach ($actual_entities_per_bundle as $filename => $revision_values) {
            $actual[$entity_type][$bundle][$filename] = $this->cleanupDynamicEntityValues($revision_values, $entity_type);
          }
        }
      }
      else {
        // Files, users, path aliases.
        $actual_bundles = $this->getActualExportAssets($entity_type);
        foreach ($expected_bundles as $bundleless_filename => $bundleles_values) {
          $expected[$entity_type][$bundleless_filename] = $this->cleanupDynamicEntityValues($bundleles_values, $entity_type);
        }
        foreach ($actual_bundles as $bundleless_filename => $bundleles_values) {
          $actual[$entity_type][$bundleless_filename] = $this->cleanupDynamicEntityValues($bundleles_values, $entity_type);
        }
      }
    }

    $this->assertEquals($expected, $actual);
  }

  /**
   * Compares the given entity data to each other.
   *
   * @param array[][] $values
   *   List of the entity revisions values.
   * @param string $filename
   *   The name of the file which contains the entity properties. The entity
   *   type ID and the entity ID are extracted from this argument.
   */
  protected function cleanupDynamicEntityValues(array $values, string $filename): array {
    $entity_type_and_id = explode('.json', $filename)[0];
    [$entity_type] = explode('-', $entity_type_and_id, 2);
    foreach ($values as $key => $entity_revision_values) {
      $clean_values[$key] = $this->removeDynamicEntityValues($entity_revision_values, $entity_type);
    }
    return $clean_values;
  }

  /**
   * Removes certain dynamic entity instance values depending on the type.
   *
   * @param array $values
   *   Entity values, keyed by property.
   * @param string $entity_type
   *   The entity type ID of the values array.
   *
   * @return array
   *   Entity values without the dynamic properties.
   */
  protected function removeDynamicEntityValues(array $values, $entity_type) {
    // UUIDs aren't migrated from Drupal 7 core.
    $ignore = [
      'uuid',
    ];

    switch ($entity_type) {
      case 'aggregator_item':
        $ignore[] = 'timestamp';
        break;

      case 'block_content':
        $ignore[] = 'revision_id';
        $ignore[] = 'revision_created';
        $ignore[] = 'changed';
        break;

      case 'menu_link_content':
        $ignore[] = 'revision_id';
        $ignore[] = 'revision_created';
        $ignore[] = 'changed';
        $ignore[] = 'content_translation_created';
        // We cannot check menu link parents.
        $ignore[] = 'parent';
        // 'node_translation_menu_links' does not migrate langcode.
        $ignore[] = 'langcode';
        break;

      case 'node':
        // Node's changed property is destroyed by the followup migrations +
        // https://drupal.org/i/2329253.
        $ignore[] = 'changed';
        break;

      case 'taxonomy_term':
        $ignore[] = 'changed';
        $ignore[] = 'revision_created';
        $ignore[] = 'content_translation_created';
        break;

      case 'user':
        // Ignore the comparison of specific properties of user with UID < 3.
        if ($values['uid'] < 3) {
          $ignore[] = 'pass';
          $ignore[] = 'access';
          $ignore[] = 'login';
        }
        $ignore[] = 'changed';
        $ignore[] = 'content_translation_created';
        break;
    }

    return array_diff_key($values, array_combine($ignore, $ignore));
  }

}
