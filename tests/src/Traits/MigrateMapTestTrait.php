<?php

namespace Drupal\Tests\smart_migrate_cli\Traits;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate\Row;

/**
 * Trait for populating ID map plugins with mappings or messages.
 */
trait MigrateMapTestTrait {

  /**
   * Populates migration map tables with data.
   *
   * @param array[][] $map_table_records_per_table
   *   Map table records grouped by the migration's full plugin ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function populateMapTables(array $map_table_records_per_table): void {
    $manager = \Drupal::service('plugin.manager.migration');
    $this->assertInstanceOf(MigrationPluginManagerInterface::class, $manager);

    foreach ($map_table_records_per_table as $migration_plugin_id => $map_table_records) {
      $migration = $manager->createInstance($migration_plugin_id);
      $this->assertInstanceOf(
        MigrationInterface::class,
        $migration,
        sprintf("Migration '%s' isn't discoverable", $migration_plugin_id)
      );
      $idmap = $migration->getIdMap();
      foreach ($map_table_records as $map_table_record) {
        $row_data = array_shift($map_table_record);
        $row = new Row($row_data, $row_data);
        $idmap->saveIdMapping($row, ...$map_table_record);
      }

      $this->assertSame(count($map_table_records), $idmap->processedCount());
    }
  }

  /**
   * Populates migration message tables with data.
   *
   * @param array[][] $message_table_records_per_table
   *   Message table records grouped by the migration's full plugin ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function populateMessageTables(array $message_table_records_per_table): void {
    $manager = \Drupal::service('plugin.manager.migration');
    $this->assertInstanceOf(MigrationPluginManagerInterface::class, $manager);

    foreach ($message_table_records_per_table as $migration_plugin_id => $message_table_records) {
      $migration = $manager->createInstance($migration_plugin_id);
      $this->assertInstanceOf(
        MigrationInterface::class,
        $migration,
        sprintf("Migration '%s' isn't discoverable", $migration_plugin_id)
      );
      $idmap = $migration->getIdMap();
      foreach ($message_table_records as $message_table_record) {
        $row_data = array_shift($message_table_record);
        $idmap->saveMessage((new Row($row_data, $row_data))->getSourceIdValues(), ...$message_table_record);
      }

      $this->assertSame(count($message_table_records), $idmap->messageCount());
    }
  }

}
