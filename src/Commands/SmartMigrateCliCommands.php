<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\OutputFormatters\StructuredData\PropertyList;
use Consolidation\OutputFormatters\StructuredData\RenderCellCollectionTrait;
use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Consolidation\SiteAlias\SiteAliasManagerAwareInterface;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\Random;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\Core\Session\UserSession;
use Drupal\Core\Site\Settings;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\migrate\id_map\Sql;
use Drupal\migrate\Plugin\MigrateIdMapInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\smart_migrate_cli\SmartMigrateCli;
use Drupal\smart_migrate_cli\Thread\MigrationThreadInterface;
use Drupal\smart_migrate_cli\SimpleMigrationManager;
use Drupal\smart_migrate_cli\Traits\SmartMigrateHelperTrait;
use Drupal\smart_migrate_cli\Traits\SmartMigrationConfigurationTrait;
use Drupal\smart_migrate_cli\Utility\ResettableTimer;
use Drush\Commands\DrushCommands;
use Drush\Exceptions\CommandFailedException;
use Drush\Exceptions\UserAbortException;
use Drush\Utils\StringUtils;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Path;

/**
 * Smart Migrate CLI's own Drush commands.
 */
class SmartMigrateCliCommands extends DrushCommands implements SiteAliasManagerAwareInterface {

  use SiteAliasManagerAwareTrait;
  use SmartMigrateHelperTrait;
  use SmartMigrationConfigurationTrait;
  use LockableTrait;
  use RenderCellCollectionTrait;

  /**
   * Exit code used of processes which migration's requirements aren't met.
   *
   * @const int
   */
  const EXIT_REQUIREMENTS_NOT_MET = 10;

  /**
   * Exit code of single processes which migration threw an exception.
   *
   * @const int
   */
  const EXIT_PROCESS_CRITICAL = 100;

  /**
   * Requirements cache key for only required migrations.
   *
   * @const string
   */
  const REQUIREMENTS_KEY_REQUIRED_ONLY = 'required_only';

  /**
   * Requirements cache key for required and available optional migrations.
   *
   * @const string
   */
  const REQUIREMENTS_KEY_WITH_OPTIONALS = 'with_optionals';

  /**
   * Message template if tag and IDs are missing.
   *
   * @const string
   */
  const MESSAGE_TEMPLATE_ERROR_MISSING_TAG_OR_IDS = 'You must specify either a migration tag with the --tag option or provide a (comma separated) list of migration (base) IDs';

  /**
   * Message template if both tag and IDs is present.
   *
   * @const string
   */
  const MESSAGE_TEMPLATE_ERROR_TAG_AND_IDS_PRESENT = 'You cannot use both migration IDs and migration tags.';

  /**
   * Message template if migrations cannot be found based on IDs.
   *
   * @const string
   */
  const MESSAGE_TEMPLATE_ERROR_NO_MIGRATIONS_PER_IDS = "No migrations found which are matching the ID(s) '%s'.";

  /**
   * Message template if migrations cannot be found based on tag.
   *
   * @const string
   */
  const MESSAGE_TEMPLATE_ERROR_NO_MIGRATIONS_PER_TAG = "No migrations found with the given tag %s.";

  /**
   * The logger section used by SmartMigrateHelper's getNextUnblockedMigration.
   *
   * Yepp, bad design.
   *
   * @var \Symfony\Component\Console\Output\OutputInterface|null
   */
  private ?OutputInterface $loggerSection;

  /**
   * Storage for empty "placeholder" sections around progress bars.
   *
   * @var \Symfony\Component\Console\Output\OutputInterface[]
   */
  private array $barPlaceholderSections = [];

  /**
   * Error messages recorded during multi-thread migration.
   *
   * First level keys are migration plugin IDs, second level keys are md5 hashes
   * of the messages. Each value is an array where the "message" key contains
   * the message and the "count" key's value is the number of how many times the
   * error was triggered during migration of the specified plugin instance.
   *
   * @var array[][]
   */
  private array $errorOutputs = [];

  /**
   * The progress bars and their metadata.
   *
   * @var array[]
   *
   * Each "subthread" has a progress bar.
   *
   * @todo Provide our own object for these things.
   */
  private array $progressBars = [];

  /**
   * The threads.
   *
   * @var \Drupal\smart_migrate_cli\Thread\MigrationThreadInterface[]
   */
  private array $threads = [];

  /**
   * List of the migration plugin instances to execute.
   *
   * @var \Drupal\migrate\Plugin\MigrationInterface[]
   */
  private array $list = [];

  /**
   * List of the migration plugin IDs which should be executed.
   *
   * @var string[]
   */
  private array $migrationsToProcess = [];

  /**
   * List of the already processed migrations' plugin IDs.
   *
   * @var string[]
   */
  private array $migrationsProcessed = [];

  /**
   * List of the fully processed migrations' plugin IDs.
   *
   * @var string[]
   */
  private array $migrationsFullyProcessed = [];

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * Account switcher.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected AccountSwitcherInterface $accountSwitcher;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Light-weight migration plugin manager service optimized for multi-thread.
   *
   * @var \Drupal\smart_migrate_cli\SimpleMigrationManager|null
   */
  protected ?SimpleMigrationManager $migrationPluginManager;

  /**
   * Light-weight migration plugin manager service optimized for multi-thread.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Section of the total progress feedback.
   *
   * @var \Symfony\Component\Console\Output\OutputInterface|null
   */
  protected $totalProgressSection;

  /**
   * The total progress feedback.
   *
   * @var \Drupal\smart_migrate_cli\TotalProgress
   */
  protected $totalProgress;

  /**
   * Log frequency in minutes.
   *
   * @var float|null
   */
  protected ?float $logging = NULL;

  /**
   * Constructs a new SmartMigrateCliCommands instance.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Session\AccountSwitcherInterface $account_switcher
   *   The account switcher service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity_type manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Database\Connection $connection
   *   The active database connection.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\smart_migrate_cli\SimpleMigrationManager|null $simple_migration_plugin_manager
   *   The light-weight migration plugin manager.
   */
  public function __construct(StateInterface $state, AccountSwitcherInterface $account_switcher, EntityTypeManagerInterface $entity_type_manager, DateFormatterInterface $date_formatter, Connection $connection, ModuleHandlerInterface $module_handler, ?SimpleMigrationManager $simple_migration_plugin_manager = NULL) {
    parent::__construct();
    $this->state = $state;
    $this->accountSwitcher = $account_switcher;
    $this->entityTypeManager = $entity_type_manager;
    $this->migrationPluginManager = $simple_migration_plugin_manager;
    $this->dateFormatter = $date_formatter;
    $this->database = $connection;
    $this->moduleHandler = $module_handler;
    $this->loggerSection = $this->output();
  }

  /**
   * Executes migrations in the optimal order, in multiple threads.
   *
   * @param array|null $options
   *   Command line options.
   *
   * @command smart-migrate:multi-thread-import
   *
   * @option tag
   *   Tag of the migrations to execute.
   * @option threads
   *   Maximal number of threads to use. Optional, defaults to 4.
   * @option memory
   *   Display memory usage even if verbosity is normal.
   * @option estimate
   *   Display estimated time even if verbosity is normal.
   * @option run-at-the-end
   *   Execute the specified migrations at the end of the process.
   * @option logging
   *   If only informational status messages should be sent to output (with the
   *   actual progress and the ID of the actually executed migrations), then
   *   this option should be the time in minutes how often the report should
   *   happen. It is possible to specify float value, e.g. '0.25'.
   *
   * @validate-module-enabled migrate
   *
   * @aliases smimt,smmti
   *
   * @throws \Exception
   *   When not enough options were provided or no migration was found.
   */
  public function mtImport(?array $options = [
    'tag' => self::REQ,
    'threads' => self::OPT,
    'memory' => self::OPT,
    'estimate' => self::OPT,
    'run-at-the-end' => self::OPT,
    'logging' => self::OPT,
  ]): int {
    // Report each minute by default.
    $this->logging = $options['logging']
      ? (float) ($this->input->getOption('logging') ?? 1)
      : NULL;
    return $this->multiThreadProcess($options, 'import');
  }

  /**
   * Executes rollback-removed operation in multiple threads.
   *
   * @param array|null $options
   *   Command line options.
   *
   * @command smart-migrate:multi-thread-rollback-removed
   *
   * @option tag
   *   Tag of the migrations to execute.
   * @option threads
   *   Maximal number of threads to use. Optional, defaults to 4.
   * @option memory
   *   Display memory usage even if verbosity is normal.
   * @option estimate
   *   Display estimated time even if verbosity is normal.
   * @option run-at-the-end
   *   Execute the specified migrations at the end of the process.
   * @option logging
   *   Whether only informational messages should be sent to output.
   *
   * @validate-module-enabled migrate
   *
   * @aliases smrdmt,smmtrd
   *
   * @throws \Exception
   *   When not enough options were provided or no migration was found.
   */
  public function mtRollbackRemoved(?array $options = [
    'tag' => self::REQ,
    'threads' => self::OPT,
    'memory' => self::OPT,
    'estimate' => self::OPT,
    'run-at-the-end' => self::OPT,
    'logging' => self::OPT,
  ]): int {
    // Report each minute by default.
    $this->logging = $options['logging']
      ? (float) ($this->input->getOption('logging') ?? 1)
      : NULL;
    return $this->multiThreadProcess($options, 'rollback-removed');
  }

  /**
   * Performs the given migration import operation in multiple threads.
   *
   * @param array $options
   *   Options of the initiator command.
   * @param string $operation
   *   The migration operation to perform.
   *
   * @return int
   *   The exit code. Might be self::EXIT_SUCCESS or self::EXIT_FAILURE.
   */
  protected function multiThreadProcess(array $options, string $operation): int {
    if (!$this->lock(SmartMigrateCli::MULTI_THREAD_ID)) {
      $this->logger()->error('A multi-thread command is already running in another process.');
      return self::EXIT_FAILURE;
    }
    $start = time();

    if ($return = $this->initializeMultiThreadCommand($options, $operation)) {
      $this->release();
      return $return;
    };

    // Register our SIGINT and SIGTERM signals. If the main process gets one of
    // these, we have to stop all the subprocesses too.
    if (extension_loaded('pcntl')) {
      pcntl_signal(SIGTERM, static::stopSignalClosure());
      pcntl_signal(SIGINT, static::stopSignalClosure());
      pcntl_async_signals(TRUE);
    }

    ResettableTimer::start(SmartMigrateCli::MULTI_THREAD_ID);

    while (!(empty($this->getBusyThreads()) && empty($this->migrationsToProcess))) {
      try {
        while (count($busy_threads = $this->getBusyThreads()) < count($this->threads) && !empty($this->migrationsToProcess)) {
          if (!($migration_id = $this->getNextUnblockedMigration($operation))) {
            if (empty($busy_threads)) {
              // There is no unblocked migration, and we don't have any active
              // processes. This means we cannot continue.
              break 2;
            }
            // Let's wait until a process finishes.
            // @todo Move SmartMigrateHelperTrait::getNextUnblockedMigration's
            //   logging here.
            break 1;
          }
          $this->scheduleMigrationProcess($migration_id);
        }

        $this->handleThreadProgress();
        usleep((int) (SmartMigrateCli::POLL_WAIT_SECONDS * 1000 * 1000));
      }
      catch (\Exception $e) {
        if (static::isStopSignal($e)) {
          // Do not report temporary error messages.
          $this->errorOutputs = array_intersect_key($this->errorOutputs, $this->migrationsProcessed);

          // Kill threads' subprocesses.
          foreach ($this->activeThreads() as $thread) {
            try {
              $thread->stop();
            }
            catch (\Throwable $t) {
            }
          }

          // Do not report "unmet" migration dependencies.
          $this->migrationsToProcess = [];
          break 1;
        }
        throw $e;
      }
    }

    return $this->finishMultiThreadCommand($start, !empty($e));
  }

  /**
   * Executes one migration.
   *
   * @command smart-migrate:single-import
   *
   * @usage smart-migrate:single-import foo
   *   Executes "foo" migration.
   *
   * @validate-module-enabled migrate
   *
   * @hidden
   */
  public function singleImport(): int {
    $migration_id = getenv(SmartMigrateCli::MIGRATION_PLUGIN_ID_ENV);
    $migrations = $migration_id ? $this->migrationPluginManager->createInstances([$migration_id]) : [];
    if (count($migrations) !== 1) {
      $this->logger()->error('Too many, or no migrations found.');
      $return = self::EXIT_FAILURE;
    }
    $migration = reset($migrations);
    $message_to_parent = ['id' => $migration_id];
    $verbosity = $this->output()->getVerbosity();
    if (
      !isset($return) &&
      ($requirement_message = $this->getMigrationRequirementsMessages($migration))
    ) {
      $this->logger()->error($requirement_message);
      $return = static::EXIT_REQUIREMENTS_NOT_MET;
    }

    if (empty($return)) {
      // Switch to user 1. This is what we usually do on the UI as well.
      // The user ID mostly affects the user reference (base) fields of migrated
      // entities which don't have source data - e.g. the owner of user or
      // taxonomy term translations.
      $this->accountSwitcher->switchTo(new UserSession(['uid' => 1]));
      try {
        $follow_up_ids = $this->executeMigration($migration);
        if (!empty($follow_up_ids)) {
          $message_to_parent['follow_ups'] = $follow_up_ids;
        }
        $return = self::EXIT_SUCCESS;
      }
      catch (\Exception $e) {
        $this->logger()->critical($e->getMessage());
        $return = self::EXIT_PROCESS_CRITICAL;
      }
      $this->accountSwitcher->switchBack();
    }

    $this->output()->setVerbosity(OutputInterface::VERBOSITY_NORMAL);
    $this->output()->writeln(json_encode($message_to_parent));
    $this->output()->setVerbosity($verbosity);

    return $return;
  }

  /**
   * Rolls back items removed from source since the last import.
   *
   * @command smart-migrate:single-rollback-removed
   *
   * @usage smart-migrate:single-rollback-removed foo
   *   Rolls back items in "foo" migration which were deleted from source.
   *
   * @validate-module-enabled migrate
   *
   * @hidden
   */
  public function singleRollbackRemoved(): int {
    $migration_id = getenv(SmartMigrateCli::MIGRATION_PLUGIN_ID_ENV);
    $migrations = $migration_id ? $this->migrationPluginManager->createInstances([$migration_id]) : [];
    if (count($migrations) !== 1) {
      $this->logger()->error('Too many, or no migrations found.');
      $return = self::EXIT_FAILURE;
    }
    $migration = reset($migrations);
    $message_to_parent = ['id' => $migration_id];
    $verbosity = $this->output()->getVerbosity();
    if (
      !isset($return) &&
      ($requirement_message = $this->getMigrationRequirementsMessages($migration))
    ) {
      $this->logger()->error($requirement_message);
      $return = static::EXIT_REQUIREMENTS_NOT_MET;
    }

    if (empty($return)) {
      // Switch to user 1. This is what we usually do on the UI as well.
      // The user ID mostly affects the user reference (base) fields of migrated
      // entities which don't have source data - e.g. the owner of user or
      // taxonomy term translations.
      $this->accountSwitcher->switchTo(new UserSession(['uid' => 1]));
      try {
        $this->executeMigration($migration, 'rollback-removed');
        $return = self::EXIT_SUCCESS;
      }
      catch (\Exception $e) {
        $this->logger()->critical($e->getMessage());
        $return = self::EXIT_PROCESS_CRITICAL;
      }
      $this->accountSwitcher->switchBack();
    }

    $this->output()->setVerbosity(OutputInterface::VERBOSITY_NORMAL);
    $this->output()->writeln(json_encode($message_to_parent));
    $this->output()->setVerbosity($verbosity);

    return $return;
  }

  /**
   * Returns the active threads.
   *
   * @return \Drupal\smart_migrate_cli\Thread\MigrationThreadInterface[]
   *   The active threads.
   */
  protected function activeThreads(): array {
    return array_filter(
      $this->threads,
      function (MigrationThreadInterface $thread) {
        return $thread->isActive();
      }
    );
  }

  /**
   * Initializes multi-thread process task.
   *
   * @param array $options
   *   Options of the initiator command.
   * @param string $operation
   *   The migration operation to initialize. Defaults to "import".
   *
   * @return int
   *   The exit code. Might be self::EXIT_SUCCESS or self::EXIT_FAILURE.
   */
  protected function initializeMultiThreadCommand(array $options, string $operation = 'import'): int {
    // Initialize our custom progress bar format.
    $format_template = $format_template_nomax = SmartMigrateCli::PROGRESS_BAR_FORMAT;
    $format_template_total = $format_template_total_nomax = SmartMigrateCli::PROGRESS_BAR_FORMAT_TOTAL;
    if (
      $this->output()->getVerbosity() > OutputInterface::VERBOSITY_NORMAL ||
      $options['estimate']
    ) {

      $format_template_total .= ' | %remaining:7s%';
      $format_template .= ' | %remaining:7s%';
    }
    if (
      $this->output()->getVerbosity() > OutputInterface::VERBOSITY_NORMAL ||
      $options['memory']
    ) {
      $format_template_total .= ' | %memory:6s%';
      $format_template_total_nomax .= ' | %memory:6s%';
    }
    ProgressBar::setFormatDefinition(SmartMigrateCli::PROGRESS_BAR_FORMAT_ID, $format_template);
    ProgressBar::setFormatDefinition(SmartMigrateCli::PROGRESS_BAR_FORMAT_ID_NOMAX, $format_template_nomax);
    ProgressBar::setFormatDefinition(SmartMigrateCli::PROGRESS_BAR_FORMAT_ID_TOTAL, $format_template_total);
    ProgressBar::setFormatDefinition(SmartMigrateCli::PROGRESS_BAR_FORMAT_ID_TOTAL_NOMAX, $format_template_total_nomax);

    $this->loggerSection = $this->output()->section();
    $this->logAnyway('Getting migration list...', $this->loggerSection);
    $list = $this->getMigrationList($this->input()->getOption('tag'));
    $this->clearLoggerSection();
    if (empty($list) || empty($list = reset($list))) {
      $this->logger()->error(sprintf(
        "No migrations found with the given tag %s.",
        $this->input()->getOption('tag')
      ));
      return self::EXIT_FAILURE;
    }
    $run_at_the_end = $options['run-at-the-end']
      ? StringUtils::csvToArray($options['run-at-the-end'])
      : array_keys(
        array_filter(
          $list,
          function (MigrationInterface $migration) {
            return in_array(
              $migration->getDestinationConfiguration()['plugin'],
              ['entity:path_alias', 'entity:menu_link_content'],
              TRUE
            );
          }
        )
      );
    $this->initializeMultiThreadProcessing($list, $run_at_the_end, $format_template_total, $operation);

    // Determine how many threads we can use.
    $threads_input = $this->input()->getOption('threads');
    $threads = is_numeric($threads_input) ? (int) $threads_input : 4;
    $this->logAnyway(sprintf(
      "Starting migration in %s threads",
      $threads
    ));

    // Initialize UI.
    $this->initializeThreads($threads, $operation);
    $this->loggerSection = $this->output()->section();
    $this->setLoggerOutput($this->loggerSection);

    return self::EXIT_SUCCESS;
  }

  /**
   * Finishes multi-thread process task.
   */
  protected function finishMultiThreadCommand(int $process_start_timestamp, bool $stopped): int {
    foreach ($this->threads as $thread) {
      $thread->stop();
    }

    foreach ($this->barPlaceholderSections as $section) {
      $section->clear();
    }
    $elapsed_time = $this->dateFormatter->formatDiff(
      $process_start_timestamp,
      time(),
      ['granularity' => 3, 'langcode' => 'en']
    );

    $this->totalProgress->finish();
    $this->finalReport();

    if ($stopped) {
      $this->logger()->warning('Migration process has been stopped.');
      return self::EXIT_FAILURE;
    }

    if (!empty($this->migrationsToProcess)) {
      $this->logger()->error('Cannot properly finish import process because the following migrations have unmet (required) dependencies: ' . implode(', ', $this->migrationsToProcess));
      return self::EXIT_FAILURE;
    }

    $this->loggerSection->clear();
    $this->logger()->success(sprintf(
      'Migration process has been finished. It took %s.',
      $elapsed_time
    ));
    $this->release();
    return self::EXIT_SUCCESS;
  }

  /**
   * Returns the sorted and filtered list of migrations with the given tag.
   *
   * @param string|null $tag
   *   The migration tag of the migrations.
   * @param string|null $ids
   *   ID(s) and / or base plugin ID(s) of migrations.
   *
   * @return \Drupal\migrate\Plugin\MigrationInterface[][]
   *   List of the discovered and executable migrations.
   */
  protected function getMigrationList(?string $tag, ?string $ids = NULL): array {
    $grouped_list = $tag
      ? $this->doGetGroupedMigrationList(NULL, $tag)
      : $this->doGetGroupedMigrationList($ids, NULL);
    $grouped_list = $this->filterMigrationList($grouped_list);
    return $this->orderMigrationList($grouped_list, TRUE);
  }

  /**
   * Changes the logger output to the specified output.
   *
   * @param \Symfony\Component\Console\Output\OutputInterface|null $output
   *   The output to use by the logger.
   */
  private function setLoggerOutput(OutputInterface $output = NULL): void {
    $this->logger()->setErrorStream($output ?? $this->output());
    $this->logger()->setOutputStream($output ?? $this->output());
  }

  /**
   * Adds migrate source data to settings.php.
   *
   * @command smart-migrate:configure
   *
   * @option db-url
   *   Url of database connection. Format:
   *   "driver://username:password@host:port/$database#table_prefix".
   * @option source-base-path
   *   Absolute path to the source Drupal instance's web root. Necessary for
   *   migrating files. The path of the files will be calculated with also
   *   evaluating the files_public_path variable of the source Drupal instance,
   *   so please do not add "sites/default/files" to the end of this path.
   * @option source-files-private
   *   Absolute path to the source Drupal instance's private file directory.
   * @option sites-subdir
   *   Name of directory under <info>sites</info> which should be configured.
   *   Optional, defaults to the 'default' directory.
   * @option connection
   *   Name of the database connection to use. Defaults to <info>migrate</info>.
   *
   * @usage smart-migrate:configure --db-url=mysql://user:password@127.0.0.1/foo
   *   Adds the 'foo' mysql database connection as migration source to the
   *   settings.php.
   * @usage smart-migrate:configure --source-base-path=/web/drupal-7
   *   Sets the '/web/drupal-7' directory as migration source webroot.
   * @usage smart-migrate:configure --source-files-private=/web/private
   *   Sets the '/web/private' directory as migration source path of private
   *   files.
   * @usage smart-migrate:configure --db-url=sqlite/localhost/test.sqlite --source-base-path=/web/drupal-7 --source-files-private=/web/private
   *   Multiple options could set in one command.
   *
   * @aliases smconf,sm-conf
   */
  public function migrationConfigure(array $options = [
    'db-url' => self::REQ,
    'source-base-path' => self::REQ,
    'source-files-private' => self::REQ,
    'sites-subdir' => self::OPT,
    'connection' => 'migrate',
  ]): void {
    $db_url = $this->input()->getOption('db-url');
    $files_source = $this->input()->getOption('source-base-path');
    $files_private_source = $this->input()->getOption('source-files-private');

    // If all options are empty, throw an exception.
    if (!$db_url && !$files_source && !$files_private_source) {
      throw new \LogicException('To configure any value for migration, one of --db-url, --source-base-path or --source-files-private options must be defined.');
    }

    // Handle aliases, if any.
    // @see SiteInstallCommands::pre().
    $alias_record = $this->siteAliasManager()->getSelf();
    $root = $alias_record->root();
    if (!($dir = $this->input()->getOption('sites-subdir'))) {
      $dir = $this->getSitesSubdirFromUri($root, $alias_record->get('uri'));
    }

    if (!$dir) {
      throw new \Exception('Could not determine target sites directory for site to install. Use --sites-subdir to specify.');
    }

    $sites_subdir = Path::join('sites', $dir);
    $settingsfile = Path::join($sites_subdir, 'settings.php');
    $settingsfile_write = Path::join($root, $settingsfile);

    if ($db_url) {
      $key = $this->input()->getOption('connection');
      $connection_info = Database::convertDbUrlToConnectionInfo($db_url, $root);

      // Let's test the connection before writing it to settings.php.
      try {
        // Find a connection key that doesn't exist.
        $random = new Random();
        do {
          $test_key = 'tmp' . strtolower($random->name(6));
        } while (Database::getConnectionInfo($test_key));
        Database::addConnectionInfo($test_key, 'default', $connection_info);
        Database::getConnection('default', $test_key)->schema()->findTables('%');
        // Restore original connection structure.
        Database::removeConnection($test_key);
      }
      catch (\Exception $e) {
        throw new \Exception(sprintf(
          "Cannot connect to the database given in DB url '%s'. Maybe a typo?",
          $db_url
        ));
      }

      if (
        Database::getConnectionInfo($key) &&
        $db_url !== $this->getDatabaseUrl(Database::getConnection('default', $key)) &&
        !$this->io()->confirm(dt('The given database connection already exists. Do you really want to override?'))
      ) {
        throw new UserAbortException();
      }

      if (
        !Database::getConnectionInfo($key) ||
        $db_url !== $this->getDatabaseUrl(Database::getConnection('default', $key))
      ) {
        $settings['databases'][$key]['default'] = (object) [
          'value' => $connection_info,
          'required' => TRUE,
        ];
      }
    }

    if ($files_source) {
      $current_base_path = Settings::get('source_base_path');
      if (
        $current_base_path &&
        $current_base_path !== $files_source &&
        !$this->io()->confirm(dt(
          "The source base path is already configured, and it is set to '!value'. Do you really want to override?",
          ['!value' => $current_base_path]
        ))
      ) {
        throw new UserAbortException();
      }
      $settings['settings']['source_base_path'] = (object) [
        'value' => $files_source,
        'required' => TRUE,
      ];
    }

    if ($files_private_source) {
      $current_private_source = Settings::get('source_private_file_path');
      if (
        $current_private_source &&
        $current_private_source !== $files_private_source &&
        !$this->io()->confirm(dt(
          "The source private files path is already configured, and it is set to '!value'. Do you really want to override?",
          ['!value' => $current_private_source]
        ))
      ) {
        throw new UserAbortException();
      }
      $settings['settings']['source_private_file_path'] = (object) [
        'value' => $files_private_source,
        'required' => TRUE,
      ];
    }

    if (!empty($settings)) {
      if (!is_writable($settingsfile_write) && !chmod($settingsfile_write, 0644)) {
        throw new \Exception(sprintf(
          "Cannot make '%s' file writable. Please modify file permissions manually and re-execute the command!",
          $settingsfile_write
        ));
      }

      include_once $root . '/core/includes/install.inc';
      drupal_rewrite_settings($settings, $settingsfile_write);
    }

    if ($db_url) {
      $this->state->set('smart_migrate_cli.source_connection', [
        'key' => $key,
        'target' => 'default',
      ]);
      $this->state->set('migrate.fallback_state_key', 'smart_migrate_cli.source_connection');
    }

    $this->logger()->success('Migration configuration was written to settings.php.');
  }

  /**
   * Determine an appropriate site subdir name to use for the provided URI.
   *
   * @param string $root
   *   The Drupal root.
   * @param string $uri
   *   The site URI.
   *
   * @return string|null
   *   The name of the directory of the specified Drupal site, or NULL if it
   *   cannot be determined.
   */
  protected function getSitesSubdirFromUri(string $root, string $uri): ?string {
    $dir = strtolower($uri);
    // Always accept simple uris (e.g. 'dev', 'stage', etc).
    if (preg_match('#^[a-z0-9_-]*$#', $dir)) {
      return $dir;
    }
    // Strip off the protocol from the provided uri -- however,
    // now we will require that the sites subdir already exist.
    $dir = preg_replace('#[^/]*/*#', '', $dir);
    if ($dir && file_exists(Path::join($root, $dir))) {
      return $dir;
    }
    // Find the dir from sites.php file.
    $sites_file = $root . '/sites/sites.php';
    if (file_exists($sites_file)) {
      $sites = [];
      include $sites_file;
      if (!empty($sites) && array_key_exists($uri, $sites)) {
        return $sites[$uri];
      }
    }
    // Fall back to default directory if it exists.
    if (file_exists(Path::join($root, 'sites', 'default'))) {
      return 'default';
    }
    return NULL;
  }

  /**
   * Returns the database URL of the given connection.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The connection.
   *
   * @return string
   *   The database URL representation of the given connection.
   */
  protected function getDatabaseUrl(Connection $connection): string {
    $connection_info = $connection->getConnectionOptions();
    $driver = $connection_info['driver'];
    $user = $connection_info['username'] ?? NULL;
    $pass = $connection_info['password'] ?? NULL;
    $prefix = $connection_info['prefix'];
    $host = strpos($connection_info['database'], DRUPAL_ROOT) === 0
      ? 'localhost'
      : $connection_info['host'] ?? NULL;
    $port = $connection_info['port'] ?? NULL;
    $db_name = strpos($connection_info['database'], DRUPAL_ROOT) === 0
      ? substr($connection_info['database'], strlen(DRUPAL_ROOT) + 1)
      : $connection_info['database'];
    $server = implode(
      '@',
      array_filter([
        implode(':', array_filter([$user, $pass])),
        implode(':', array_filter([$host, $port])),
      ])
    );
    $table_and_prefix = implode('#', array_filter([
      $db_name,
      $prefix,
    ]));
    return "$driver://$server/$table_and_prefix";
  }

  /**
   * Closure called when operation is aborted.
   *
   * @return \Closure
   *   Closure called when operation is aborted.
   */
  protected static function stopSignalClosure(): \Closure {
    return function () {
      throw new \Exception(SmartMigrateCli::MULTI_THREAD_ID);
    };
  }

  /**
   * Determines whether the throwable is a stop signal.
   *
   * @param \Throwable $t
   *   A throwable to check.
   *
   * @return bool
   *   Whether the throwable is a stop signal.
   */
  protected static function isStopSignal(\Throwable $t): bool {
    return $t->getMessage() === SmartMigrateCli::MULTI_THREAD_ID;
  }

  /**
   * Resets all failed rows of the given migrations.
   *
   * @param string|null $migration_ids
   *   IDs or base plugin IDs of the migrations to diagnose.
   * @param array|null $options
   *   Command line options.
   *
   * @command smart-migrate:reset-failed
   *
   * @option tag
   *   Tag of the migrations to reset.
   *
   * @validate-module-enabled migrate
   *
   * @aliases smrf
   *
   * @throws \Exception
   *   When not enough options were provided or no migration was found.
   */
  public function migrateReset(?string $migration_ids = NULL, ?array $options = [
    'tag' => self::REQ,
  ]): int {
    $list = $this->getMigrationInstancesBasedOnListOrTag($migration_ids, $this->input()->getOption('tag'));

    $bar_section = $this->output() instanceof ConsoleOutput
      ? $this->output()->section()
      : $this->output();
    $bar = new ProgressBar($bar_section, count($list));

    if ($this->input()->isInteractive()) {
      $bar->start();
    }

    foreach ($list as $instance) {
      assert($instance instanceof MigrationInterface);
      $id_map = $instance->getIdMap();
      if ($id_map->errorCount()) {
        if ($id_map instanceof Sql) {
          $hashes_of_failed_rows = $id_map->getDatabase()->select($id_map->mapTableName(), 'map')
            ->fields('map', [Sql::SOURCE_IDS_HASH])
            ->condition('source_row_status', MigrateIdMapInterface::STATUS_FAILED)
            ->execute()
            ->fetchAllKeyed(0, 0);
          if (!empty($hashes_of_failed_rows)) {
            // Delete messages.
            $id_map->getDatabase()->delete($id_map->messageTableName())
              ->condition(Sql::SOURCE_IDS_HASH, $hashes_of_failed_rows, 'IN')
              ->execute();
            // Delete map records.
            $id_map->getDatabase()->delete($id_map->mapTableName())
              ->condition('source_row_status', MigrateIdMapInterface::STATUS_FAILED)
              ->execute();
          }
        }
        else {
          // Not every failed migration rows have message.
          $id_map->rewind();
          while ($id_map->valid()) {
            $source_values = $id_map->currentSource();
            $row = $id_map->getRowBySource($source_values);
            if (!empty($row['source_row_status']) && $row['source_row_status'] == MigrateIdMapInterface::STATUS_FAILED) {
              $id_map->delete($source_values);
            }
            $id_map->next();
          }
        }
      }

      $bar->advance();
    }

    $bar->finish();

    if ($this->input()->isInteractive()) {
      $bar->clear();
    }
    $this->output()->writeln('Failed rows were reset.');
    return static::EXIT_SUCCESS;
  }

  /**
   * An overview of the environment - focusing on DB to DB migrations.
   *
   * @command smart-migrate:status
   * @aliases sms
   * @table-style compact
   * @list-delimiter :
   * @field-labels
   *   php-memory: PHP memory
   *   host-db-uri: Host DB URI
   *   host-target: Host connection key (target)
   *   host-isolation: Host isolation level
   *   source-db: Source DB
   *   source-db-uri: Source DB URI
   *   source-target: Source connection key (target)
   *   source-isolation: Source isolation level
   *   joinability: Source and ID Map joinable
   * @default-fields php-memory,host-db-uri,host-target,host-isolation,source-db,source-db-uri,source-target,source-isolation,joinability
   * @pipe-format json
   */
  public function status(array $options = ['format' => 'table']): PropertyList {
    $result_data = [
      'php-memory' => ini_get('memory_limit'),
      'host-db-uri' => static::getConnectionInfoAsUrl($this->database->getKey(), $this->database->getTarget()),
      'host-target' => "{$this->database->getKey()} ({$this->database->getTarget()})",
      'host-isolation' => $this->getTransactionIsolationLevel($this->database->getKey(), $this->database->getTarget()),
      'source-db' => 'NaN',
    ];

    if ($this->moduleHandler->moduleExists('migrate')) {
      $migrate_source_state_key = $this->state->get('migrate.fallback_state_key');
      $migrate_source = $migrate_source_state_key
        ? $this->state->get($migrate_source_state_key)
        : [];
      $migrate_source += ['target' => 'default', 'key' => 'migrate'];
      $result_data['source-target'] = "{$migrate_source['key']} ({$migrate_source['target']})";
      $result_data['source-db'] = '<comment>Not connected</comment>';

      if (
        !empty(Database::getConnectionInfo($migrate_source['key'])) &&
        !empty(Database::getConnectionInfo($migrate_source['key'])[$migrate_source['target']])
      ) {
        $source_connected = TRUE;
        $result_data['source-db'] = '<info>Connected</info>';
        $result_data['source-db-uri'] = static::getConnectionInfoAsUrl($migrate_source['key'], $migrate_source['target']);
        $result_data['source-isolation'] = $this->getTransactionIsolationLevel($migrate_source['key'], $migrate_source['target']);
        $result_data['joinability'] = 'NaN';
      }

      if (!empty($source_connected) && $this->moduleHandler->moduleExists('migrate_drupal')) {
        $test_migration = $this->migrationPluginManager->createStubMigration([
          'id' => 'smc-test',
          'source' => ['plugin' => 'variable'],
          'destination' => ['plugin' => 'null'],
        ]);
        $var_source = $test_migration->getSourcePlugin();
        $reflection = new \ReflectionClass($var_source);
        $joinable = $reflection->getMethod('mapJoinable');
        $joinable->setAccessible(TRUE);
        $result_data['joinability'] = $joinable->invoke($var_source) ? '<info>Yes</info>' : '<error>No</error>';

        // Id map tables of the test migration should be destroyed.
        $test_migration->getIdMap()->destroy();
      }
    }

    $result = new PropertyList($result_data);

    return $result;
  }

  /**
   * Returns the isolation level of the given DB connection.
   *
   * @param string $key
   *   (Optional) The database connection key.
   * @param string $target
   *   (Optional) The database connection target.
   *
   * @return string
   *   Isolation level of the given DB connection.
   */
  protected function getTransactionIsolationLevel(string $key = 'default', string $target = 'default'): string {
    $connection = Database::getConnection($target, $key);

    // The database variable "tx_isolation" has been removed in MySQL v8.0.3 and
    // has been replaced by "transaction_isolation".
    // @see https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html#sysvar_tx_isolation
    // @see https://dev.mysql.com/doc/refman/8.0/en/added-deprecated-removed.html
    $query = !$connection->isMariaDb() && version_compare($connection->version(), '8.0.2-AnyName', '>')
      ? 'SELECT @@SESSION.transaction_isolation'
      : 'SELECT @@SESSION.tx_isolation';

    try {
      return $connection->query($query)->fetchField() ?: '<comment>NaN</comment>';
    }
    catch (\Exception $e) {
    }

    return '<error>Fetching isolation triggered an exception</error>';
  }

  /**
   * Gets database connection info as a URL.
   *
   * @param string $key
   *   (Optional) The database connection key.
   * @param string $target
   *   (Optional) The database connection target.
   *
   * @return string
   *   The connection info as a URL.
   *
   * @throws \RuntimeException
   *   When the database connection is not defined.
   *
   * @see \Drupal\Core\Database\Database::getConnectionInfoAsUrl()
   */
  public static function getConnectionInfoAsUrl(string $key = 'default', string $target = 'default'): string {
    $db_info = Database::getConnectionInfo($key);
    if (empty($db_info) || empty($db_info[$target])) {
      throw new \RuntimeException(sprintf(
        "Database connection '%s' not defined or missing the '%s' settings",
        $key,
        $target
      ));
    }
    $namespace = $db_info[$target]['namespace'];

    // If the driver namespace is within a Drupal module, add the module name
    // to the connection options to make it easy for the connection class's
    // createUrlFromConnectionOptions() method to add it to the URL.
    if (static::isWithinModuleNamespace($namespace)) {
      $db_info[$target]['module'] = explode('\\', $namespace)[1];
    }

    $connection_class = $namespace . '\\Connection';
    return $connection_class::createUrlFromConnectionOptions($db_info[$target]);
  }

  /**
   * Checks whether a namespace is within the namespace of a Drupal module.
   *
   * This can be used to determine if a database driver's namespace is provided
   * by a Drupal module.
   *
   * @param string $namespace
   *   The namespace (for example, of a database driver) to check.
   *
   * @return bool
   *   TRUE if the passed in namespace is a sub-namespace of a Drupal module's
   *   namespace.
   *
   * @see \Drupal\Core\Database\Database::isWithinModuleNamespace()
   */
  private static function isWithinModuleNamespace(string $namespace): bool {
    [$first, $second] = explode('\\', $namespace, 3);

    // The namespace for Drupal modules is Drupal\MODULE_NAME, and the module
    // name must be all lowercase. Second-level namespaces containing uppercase
    // letters (e.g., "Core", "Component", "Driver") are not modules.
    // @see \Drupal\Core\DrupalKernel::getModuleNamespacesPsr4()
    // @see https://www.drupal.org/docs/8/creating-custom-modules/naming-and-placing-your-drupal-8-module#s-name-your-module
    return ($first === 'Drupal' && strtolower($second) === $second);
  }

  /**
   * Returns the list of failed rows including migration messages, if any.
   *
   * @param string|null $migration_ids
   *   IDs or base plugin IDs of the migrations to diagnose.
   * @param array|null $options
   *   Command line options.
   *
   * @command smart-migrate:diagnose
   *
   * @option tag
   *   Tag of the migrations to diagnose.
   * @option status
   *   Status of the migration source rows to list. Defaults to
   *   MigrateIdMapInterface::STATUS_FAILED. Other possible values:
   *   MigrateIdMapInterface::STATUS_IMPORTED,
   *   MigrateIdMapInterface::STATUS_NEEDS_UPDATE,
   *   MigrateIdMapInterface::STATUS_IGNORED. Use --status=all to get all
   *   records regardless of source row status.
   * @option level
   *   Level of the migration messages to diagnose. If this option is used, only
   *   those rows will be listed which have messages. Warning! Not every failed
   *   migration rows have migration message! The most common values are
   *   MigrationInterface::MESSAGE_INFORMATIONAL and
   *   MigrationInterface::MESSAGE_ERROR.
   * @option shorten-messages
   *   Sorten messages longer than this value. Optional, defaults to 0 (which
   *   disables the feature). Use non-negative integers only!
   *
   * @validate-module-enabled migrate
   *
   * @aliases smd
   *
   * @field-labels
   *   id: Plugin ID
   *   source_ids: Source ID(s)
   *   destination_ids: Destination ID(s)
   *   status: Row status
   *   level: Level
   *   message: Message
   * @default-fields source_ids,destination_ids,status,level,message
   *
   * @throws \Exception
   *   When not enough options were provided or no migration was found.
   *
   * @see \Drupal\migrate\Plugin\MigrationInterface
   */
  public function migrateDiagnose(?string $migration_ids = NULL, ?array $options = [
    'tag' => self::REQ,
    'status' => self::OPT,
    'level' => self::OPT,
    'shorten-messages' => self::REQ,
  ]): RowsOfFields {
    $tag = $this->input()->getOption('tag');
    $list = $this->getMigrationInstancesBasedOnListOrTag($migration_ids, $tag);

    $status = $this->input()->getOption('status')
      ? (int) $this->input()->getOption('status')
      : MigrateIdMapInterface::STATUS_FAILED;
    if ($this->input()->getOption('status') === 'all') {
      $status = NULL;
    }
    $message_max_length = (int) $this->input()->getOption('shorten-messages');

    $level = $this->input()->getOption('level')
      ? (int) $this->input()->getOption('level')
      : NULL;

    if ($this->input()->isInteractive()) {
      $bar_section = $this->output() instanceof ConsoleOutput
        ? $this->output()->section()
        : $this->output();
      $bar = new ProgressBar($bar_section, count($list));
      $bar->start();
    }

    assert($this->commandData instanceof CommandData);
    $formatter_option_fields = StringUtils::csvToArray($this->commandData->formatterOptions()->getOptions()['fields']);
    // Add ID column if:
    // - Tag is specified (drush smd --tag=Foo), OR
    // - There are more than one migration discovered, OR
    // - There is only one migration discovered, but the ID given in command
    //   argument does not equal to the ID of the discovered migration: e.g.
    //   the command was "drush smd d7_node_complete", and we found only
    //   "d7_node_complete:blog".
    if (
      !in_array('id', $formatter_option_fields, TRUE) &&
      (
        !is_null($tag) ||
        (count($list) > 1 || key($list) !== $migration_ids)
      )
    ) {
      array_unshift($formatter_option_fields, 'id');
    }

    $fields_to_remove = [];
    if ($level) {
      $fields_to_remove[] = 'level';
    }
    if (!is_null($status)) {
      $fields_to_remove[] = 'status';
    }

    $formatter_option_fields = array_filter(
      $formatter_option_fields,
      function (string $field) use ($fields_to_remove) {
        return !in_array($field, $fields_to_remove, TRUE);
      }
    );
    $this->commandData->formatterOptions()->setOption('fields', implode(',', $formatter_option_fields));

    $table = [];
    foreach ($list as $migration) {
      if (($id_map = $migration->getIdMap())->processedCount()) {
        $id = $migration->id();
        if ($id_map instanceof Sql) {
          $query = $id_map->getDatabase()->select($id_map->mapTableName(), 'map')
            ->fields('map');
          $query->leftJoin($id_map->messageTableName(), 'msg', sprintf('msg.%s = map.%s', $id_map::SOURCE_IDS_HASH, $id_map::SOURCE_IDS_HASH));
          $query->fields('msg', ['level', 'message']);
          if (is_int($status)) {
            $query->condition('map.source_row_status', $status);
          }
          if ($level) {
            $query->condition('msg.level', $level);
          }
          // Standardize sort.
          $ids_count = count($migration->getSourcePlugin()->getIds());
          for ($count = 1; $count <= $ids_count; $count++) {
            $query->orderBy('map.sourceid' . $count);
          }
          $query->orderBy('msg.level');
          foreach ($query->execute()->fetchAll(\PDO::FETCH_ASSOC) as $row) {
            $table[] = ['id' => $id] + $this->preprocessRow($row, [], $message_max_length);
          }
        }
        else {
          $id_map->rewind();
          while ($id_map->valid()) {
            $source_values = $id_map->currentSource();
            $row = $id_map->getRowBySource($source_values);

            if (is_null($status) || (int) $row['source_row_status'] === $status) {
              $messages = iterator_to_array($id_map->getMessages($source_values, $level), FALSE);
              foreach ($messages as $message) {
                $table[] = ['id' => $id] + $this->preprocessRow($row, (array) $message, $message_max_length);
              }
              if (!$level && !count($messages)) {
                $table[] = ['id' => $id] + $this->preprocessRow($row, [], $message_max_length);
              }
            }
            $id_map->next();
          }
        }
      }

      if (!empty($bar)) {
        $bar->advance();
      }
    }

    if (!empty($bar)) {
      $bar->finish();
      $bar->clear();
    }
    return new RowsOfFields($table);
  }

  /**
   * Preprocesses migrate map rows.
   *
   * Given an item inside the list generated by
   * MigrateIdMapInterface::getMessages(), prepare it for display.
   *
   * @param array $map_row
   *   A map row to process.
   * @param array|null $message
   *   A message row to process, if any.
   * @param int $message_max_length
   *   Maximal length of the message. If the message is longer, it will be
   *   shortened.
   *
   * @see \Drupal\migrate\Plugin\MigrateIdMapInterface::getMessages()
   */
  protected function preprocessRow(array $map_row, ?array $message = [], int $message_max_length = 0): array {
    foreach ($map_row as $key => $value) {
      if (str_starts_with($key, 'sourceid') && isset($value)) {
        $source_ids[$key] = $value;
      }
      if (str_starts_with($key, 'destid') && isset($value)) {
        $destination_ids[$key] = $value;
      }
    }
    $map_row['source_ids'] = implode(' : ', $source_ids ?? []);
    $map_row['destination_ids'] = implode(' : ', $destination_ids ?? []);
    $map_row['status'] = $map_row['source_row_status'] ?? '';
    $map_row += ($message + ['level' => '', 'message' => '']);
    if ($message_max_length) {
      $map_row['message'] = mb_strimwidth($map_row['message'] ?? '', 0, $message_max_length, '…');
    }
    return $map_row;
  }

  /**
   * Returns migration instances based on CSV-style ID list or based on tag.
   *
   * @param string|null $migration_ids
   *   ID (or base plugin ID) list (CSV-style, separated by commas).
   * @param string|null $tag
   *   Migration tag of the migrations.
   *
   * @return \Drupal\migrate\Plugin\MigrationInterface[]
   *   List of migration plugin instances.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If one of the plugin IDs is invalid.
   * @throws \Drush\Exceptions\CommandFailedException
   *   If the list is empty.
   */
  protected function getMigrationInstancesBasedOnListOrTag(?string $migration_ids, ?string $tag = NULL): array {
    if (empty($migration_ids) && empty($tag)) {
      throw new CommandFailedException(static::MESSAGE_TEMPLATE_ERROR_MISSING_TAG_OR_IDS);
    }
    if (!empty($migration_ids) && !empty($tag)) {
      throw new CommandFailedException(static::MESSAGE_TEMPLATE_ERROR_TAG_AND_IDS_PRESENT);
    }

    if ($this->input()->isInteractive()) {
      $this->output()->writeln('Getting migration list...');
    }

    if (!empty($migration_ids)) {
      // We want to handle base plugin IDs too.
      $expanded_ids = array_keys($this->migrationPluginManager->createInstances(StringUtils::csvToArray($migration_ids)));
      $all_ids_with_base = array_reduce(
        $expanded_ids,
        function (array $carry, string $full_id) {
          $pieces = explode(PluginBase::DERIVATIVE_SEPARATOR, $full_id);
          foreach ($pieces as $piece) {
            $partial = isset($partial)
              ? $partial . PluginBase::DERIVATIVE_SEPARATOR . $piece
              : $piece;
            $carry[$partial] = $partial;
          }
          return $carry;
        },
        []
      );
      if ($missing_ids = array_diff(StringUtils::csvToArray($migration_ids), $all_ids_with_base)) {
        throw new CommandFailedException(sprintf(
          static::MESSAGE_TEMPLATE_ERROR_NO_MIGRATIONS_PER_IDS,
          implode("', '", $missing_ids)
        ));
      }
    }
    $list = !empty($expanded_ids)
      ? $this->getMigrationList(NULL, implode(',', $expanded_ids))
      : $this->getMigrationList($tag);
    if (empty($list) || empty($list = reset($list))) {
      throw new CommandFailedException(sprintf(
        static::MESSAGE_TEMPLATE_ERROR_NO_MIGRATIONS_PER_TAG,
        $tag
      ));
    }

    return $list;
  }

}
