<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli\Commands;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\Core\Session\UserSession;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate_drupal\Plugin\MigrationWithFollowUpInterface;
use Drupal\smart_migrate_cli\Traits\MigrateRunnerTrait;
use Drupal\smart_migrate_cli\Traits\SmartMigrationConfigurationTrait;
use Drush\Drupal\Commands\core\MigrateRunnerCommands as OriginalMigrateRunnerCommands;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Path;

/**
 * Smarter Migrate runner commands.
 *
 * Now:
 * - Build optimal execution order on import operation, making
 *   "--execute-dependencies" unnecessary in most of the situations.
 * - Removes unnecessary D6 migrations if we are migrating from Drupal 7 and
 *   vice versa.
 * - Skips the unnecessary node migrations.
 */
class MigrateRunnerCommands extends OriginalMigrateRunnerCommands {

  use SmartMigrationConfigurationTrait;
  use MigrateRunnerTrait;

  /**
   * Storage for warnings happened during import.
   *
   * @var string[]
   */
  protected array $importWarnings;

  /**
   * Storage for follow-up migrations to execute.
   *
   * @var array
   */
  protected array $followUpMigrationsToExecute;

  /**
   * Account switcher.
   *
   * @var \Drupal\Core\Session\AccountSwitcherInterface
   */
  protected AccountSwitcherInterface $accountSwitcher;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new class instance.
   *
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   Date formatter service.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   *   The key-value factory service.
   * @param \Drupal\Core\Session\AccountSwitcherInterface $accountSwitcher
   *   The account switcher.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\smart_migrate_cli\SimpleMigrationManager|null $migrationPluginManager
   *   The migration plugin manager service.
   */
  public function __construct(DateFormatter $dateFormatter, KeyValueFactoryInterface $keyValueFactory, AccountSwitcherInterface $accountSwitcher, EntityTypeManagerInterface $entityTypeManager, ?MigrationPluginManagerInterface $migrationPluginManager = NULL) {
    parent::__construct($dateFormatter, $keyValueFactory, $migrationPluginManager);
    $this->accountSwitcher = $accountSwitcher;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  protected function getMigrationList(?string $migrationIds, ?string $tags): array {
    $invoked = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2)[1]['function'] ?? NULL;
    $order_forced = func_get_args()[2] ?? FALSE;
    $is_import_operation = $invoked === 'import' || $order_forced;
    $grouped_list = $this->doGetGroupedMigrationList($migrationIds, $tags);
    if ($is_import_operation) {
      ksort($grouped_list);
    }

    $grouped_list = $this->filterMigrationList($grouped_list);

    return $this->orderMigrationList($grouped_list, $is_import_operation);
  }

  /**
   * Executes one or more migrations.
   *
   * This is the overridden, enhanced version of Drush core's migrate command.
   * Differences compared to Drush:
   * - Imports are performed with user 1.
   * - Migrations are executed in the optimal order. If you are executing a
   *   complete migration set (e.g. "Drupal 6", "Drupal 7"), there is no need
   *   to use the "--execute-dependencies" argument anymore.
   * - Only the necessary Drupal to Drupal node migrations are executed.
   *
   * @param string|null $migrationIds
   *   Comma-separated list of migration IDs.
   * @param array|null $options
   *   Command line options.
   *
   * @command migrate:import
   *
   * @option all Process all migrations.
   * @option tag A comma-separated list of migration tags to import
   * @option limit Limit on the number of items to process in each migration
   * @option feedback Frequency of progress messages, in items processed
   * @option idlist Comma-separated list of IDs to import. As an ID may have more than one column, concatenate the columns with the colon ':' separator
   * @option update In addition to processing unprocessed items from the source, update previously-imported items with the current data
   * @option force Force an operation to run, even if all dependencies are not satisfied
   * @option execute-dependencies Execute all dependent migrations first.
   * @option timestamp Show progress ending timestamp in progress messages
   * @option total Show total processed item number in progress messages
   * @option progress Show progress bar
   * @option delete
   *   Delete destination records missed from the source. Not
   *   compatible with --limit and --idlist options, and high_water_property
   *   source configuration key.
   *
   * @usage migrate:import --all
   *   Perform all migrations
   * @usage migrate:import --all --no-progress
   *   Perform all migrations but avoid the progress bar
   * @usage migrate:import --tag=user,main_content
   *   Import all migrations tagged with <info>user</info> and
   *   <info>main_content</info> tags
   * @usage migrate:import classification,article
   *   Import new terms and nodes using migration <info>classification</info>
   *   and <info>article</info>
   * @usage migrate:import user --limit=2
   *   Import no more than 2 users using the <info>user</info> migration
   * @usage migrate:import user --idlist=5
   *   Import the user record with source ID 5
   * @usage migrate:import node_revision --idlist=1:2,2:3,3:5
   *   Import the node revision record with source IDs [1,2], [2,3], and [3,5]
   * @usage migrate:import user --limit=50 --feedback=20
   *   Import 50 users and show process message every 20th record
   * @usage migrate:import --all --delete
   *   Perform all migrations and delete the destination items that are missing
   *   from source
   *
   * @aliases mim,migrate-import
   *
   * @topics docs:migrate
   *
   * @validate-module-enabled migrate
   *
   * @version 10.4
   *
   * @throws \Exception
   *   When not enough options were provided or no migration was found.
   */
  public function import(
    ?string $migrationIds = NULL,
    array $options = [
      'all' => FALSE,
      'tag' => self::REQ,
      'limit' => self::REQ,
      'feedback' => self::REQ,
      'idlist' => self::REQ,
      'update' => FALSE,
      'force' => FALSE,
      'execute-dependencies' => FALSE,
      'timestamp' => FALSE,
      'total' => FALSE,
      'progress' => TRUE,
      'delete' => FALSE,
    ]): void {
    $tags = $options['tag'];
    $all = $options['all'];

    if (!$all && !$migrationIds && !$tags) {
      throw new \Exception(dt('You must specify --all, --tag or one or more migration names separated by commas'));
    }

    if (!$list = $this->getMigrationList($migrationIds, $options['tag'])) {
      throw new \Exception(dt('No migrations found.'));
    }

    $userData = [
      'options' => array_intersect_key($options, array_flip([
        'limit',
        'feedback',
        'idlist',
        'update',
        'force',
        'timestamp',
        'total',
        'progress',
        'delete',
      ])),
      'execute_dependencies' => $options['execute-dependencies'],
    ];

    // Include the file providing a migrate_prepare_row hook implementation.
    require_once Path::join(DRUSH_BASE_PATH, 'src/Drupal/Migrate/migrate_runner.inc');
    // If the 'migrate_prepare_row' hook implementations are already cached,
    // make sure that system_migrate_prepare_row() is picked-up.
    // @codingStandardsIgnoreLine
    \Drupal::moduleHandler()->resetImplementations();

    // Switch to user 1. This is what we usually do on the UI as well.
    // The user ID mostly affects the user reference (base) fields of migrated
    // entities which don't have source data - e.g. the owner of user or
    // taxonomy term translations.
    $this->accountSwitcher->switchTo(new UserSession(['uid' => 1]));
    $this->importWarnings = [];
    $this->followUpMigrationsToExecute = [];
    foreach ($list as $migrations) {
      array_walk($migrations, [static::class, 'executeMigration'], $userData);
    }
    if (!empty($this->followUpMigrationsToExecute)) {
      $this->writeLineIfNotQuiet('');
      $this->logger()->notice('Executing follow-up migrations');
      $this->writeLineIfNotQuiet('');
      array_walk(
        $this->followUpMigrationsToExecute,
        [static::class, 'executeMigration'],
        $userData
      );
    }
    $this->accountSwitcher->switchBack();
    if ($this->importWarnings) {
      $this->writeLineIfNotQuiet('');
      $lines_to_log = array_merge(
        [dt('Some rows failed to import, but this not necessarily means that something went terribly wrong. Consider checking the migration map and migration message tables of the following migrations.')],
        $this->importWarnings
      );
      $this->logger()->warning(implode(
        "\n           ",
        $lines_to_log
      ));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function executeMigration(MigrationInterface $migration, string $migrationId, array $userData): void {
    $migration = $this->getFreshMigrationInstance($migration->id());

    $this->prepareMigration($migration);

    try {
      parent::executeMigration($migration, $migrationId, $userData);
    }
    catch (\Exception $e) {
      $this->importWarnings[] = $e->getMessage();
    }

    // After the migration on which they depend has been successfully executed,
    // the follow-up migrations should be immediately added to the list of the
    // migrations to be executed.
    if ($migration instanceof MigrationWithFollowUpInterface) {
      $diff = array_diff_key($migration->generateFollowUpMigrations(), $this->followUpMigrationsToExecute ?? []);
      $this->followUpMigrationsToExecute = array_merge(
        $this->followUpMigrationsToExecute ?? [],
        $diff
      );
    }
  }

  /**
   * Writes a message to non-quiet output and adds a newline at the end.
   *
   * @param string|iterable $messages
   *   The message.
   * @param int $options
   *   A bitmask of options (one of the OUTPUT or VERBOSITY constants), 0 is
   *   considered the same as self::OUTPUT_NORMAL | self::VERBOSITY_NORMAL.
   */
  protected function writeLineIfNotQuiet($messages, int $options = 0) {
    if ($this->output()->getVerbosity() !== OutputInterface::VERBOSITY_QUIET) {
      $this->output()->writeln($messages, $options);
    }
  }

}
