<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli\Plugin\Mail;

use Drupal\Core\Mail\MailInterface;

/**
 * The mail plugin that loses all the emails.
 *
 * @Mail(
 *   id = "terrible_mail_man",
 *   label = @Translation("Terrible Mail Man"),
 *   description = @Translation("Loses all the emails.")
 * )
 */
class TerribleMailMan implements MailInterface {

  /**
   * {@inheritdoc}
   */
  public function mail(array $message) {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function format(array $message) {
    $message['body'] = 'already lost!';
    return $message;
  }

}
