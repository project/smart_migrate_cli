<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli\Plugin\migrate\id_map;

use Drupal\smart_sql_idmap\Plugin\migrate\id_map\SmartSql;
use Drupal\smart_migrate_cli\Traits\RemoveDeletedIdMapTrait;

/**
 * ID map plugin for rolling back items removed from source.
 *
 * @PluginID("smart_sql_rollback_removed")
 */
class SmartSqlRollBackRemoved extends SmartSql {

  use RemoveDeletedIdMapTrait;

}
