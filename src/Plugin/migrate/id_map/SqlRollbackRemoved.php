<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli\Plugin\migrate\id_map;

use Drupal\migrate\Plugin\migrate\id_map\Sql;
use Drupal\smart_migrate_cli\Traits\RemoveDeletedIdMapTrait;

/**
 * ID map plugin for rolling back items removed from source.
 *
 * @PluginID("sql_rollback_removed")
 */
class SqlRollbackRemoved extends Sql {

  use RemoveDeletedIdMapTrait;

}
