<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli;

/**
 * Constants and basic routines of Smart Migrate CLI.
 */
class SmartMigrateCli {

  /**
   * Identifier of the main multi thread migration process.
   *
   * Used for locking and killing subprocesses.
   *
   * @const string
   */
  const MULTI_THREAD_ID = 'smart_migrate_cli_multithread';

  /**
   * Environment variable name storing migration plugin ID.
   *
   * @const string
   */
  const MIGRATION_PLUGIN_ID_ENV = 'SMC_MIGRATION_PLUGIN_ID';

  /**
   * Text displayed in empty progress bar sections.
   *
   * @const string
   */
  const PROGRESS_BAR_EMPTY_TEXT = ' [- Idle ---------------------]';

  /**
   * Width of progress bar.
   *
   * @const int
   */
  const PROGRESS_BAR_WIDTH = 28;

  /**
   * Minimum time to wait between updating scheduled processes and progress.
   *
   * @const float
   */
  const POLL_WAIT_SECONDS = 0.2;

  /**
   * ID of the normal progress bar format.
   *
   * @const string
   */
  const PROGRESS_BAR_FORMAT_ID = 'mt_progress';

  /**
   * ID of the "no max" progress bar format.
   *
   * @const string
   */
  const PROGRESS_BAR_FORMAT_ID_NOMAX = 'mt_progress_nomax';

  /**
   * ID of the total progress bar format.
   *
   * @const string
   */
  const PROGRESS_BAR_FORMAT_ID_TOTAL = 'mt_total';

  /**
   * ID of the total progress bar format without item count.
   *
   * @const string
   */
  const PROGRESS_BAR_FORMAT_ID_TOTAL_NOMAX = 'mt_total_nomax';

  /**
   * The default progress bar format used by multi thread migrate import.
   *
   * @const string
   */
  const PROGRESS_BAR_FORMAT = ' [%bar%] %percent:3s%% |%current:5s%/%max:-5s%| %message%';

  /**
   * The default progress bar format used by multi thread migrate import.
   *
   * @const string
   */
  const PROGRESS_BAR_FORMAT_TOTAL = '<comment>Total progress:</comment> [%bar%] %percent:3s%%';

}
