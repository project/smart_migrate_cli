<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Drupal\smart_migrate_cli\Cache\CacheKillerPass;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;

/**
 * Registers cache killer compiler pass and adds a simple migration manager.
 */
class SmartMigrateCliServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->addCompilerPass(new CacheKillerPass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, 1);
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->hasDefinition('plugin.manager.migration')) {
      $definition = new Definition(SimpleMigrationManager::class);
      $definition->setArguments([
        new Reference('module_handler'),
        new Reference('cache.discovery_migration'),
        new Reference('language_manager'),
        new Reference('plugin.manager.migration'),
      ]);
      $definition->setPublic(TRUE);
      $container->setDefinition('smart_migrate_cli.plugin.manager.migration.simple', $definition);
    }
  }

}
