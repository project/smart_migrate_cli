<?php

namespace Drupal\smart_migrate_cli\Cache;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Disarms cache bins - speed up migration, lower DB i/o and prevent deadlocks.
 */
class CacheKillerPass implements CompilerPassInterface {

  /**
   * IDs of the caches to suppress.
   *
   * @const string[].
   */
  const KILLED_CACHES = [
    'cache.entity',
    'cache.config',
  ];

  /**
   * IDs of caches to move to consistent cache.
   *
   * @const string[].
   */
  const CACHES_MADE_CONSISTENT = [
    'cache.bootstrap',
    'cache.discovery',
  ];

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container) {
    foreach ($container->findTaggedServiceIds('cache.bin') as $id => $attributes) {
      if (
        in_array($id, self::KILLED_CACHES, TRUE) &&
        (
          !isset($attributes[0]['default_backend']) ||
          $attributes[0]['default_backend'] === 'cache.backend.database'
        )
      ) {
        $cache_definition = $container->getDefinition($id);
        $tags = $cache_definition->getTags();
        $tags['cache.bin'] = [['default_backend' => 'cache.backend.smc_null']];
        $cache_definition->setTags($tags);
      }

      if (in_array($id, self::CACHES_MADE_CONSISTENT, TRUE)) {
        $cache_definition = $container->getDefinition($id);
        $tags = $cache_definition->getTags();
        $tags['cache.bin'] = [['default_backend' => 'cache.backend.database']];
        $cache_definition->setTags($tags);
      }
    }
  }

}
