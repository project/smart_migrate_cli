<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\migrate\MigrateExecutable;

/**
 * Overrides mail plugin configuration.
 */
class MigrationMailConfigOverrider implements ConfigFactoryOverrideInterface {

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    if (!in_array('system.mail', $names, TRUE)) {
      return [];
    }

    $backtrace = debug_backtrace(0);
    $is_migration_call_stack = array_reduce(
      $backtrace,
      function (bool $carry, array $stack) {
        if ($carry) {
          return $carry;
        }
        $class = $stack['class'] ?? NULL;
        return $class === MigrateExecutable::class || is_subclass_of($class, MigrateExecutable::class);
      },
      FALSE
    );
    return $is_migration_call_stack
      ? ['system.mail' => ['interface' => ['default' => 'terrible_mail_man']]]
      : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'MigrateMailConfigOverrider';
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    if ($name === 'system.mail') {
      return (new CacheableMetadata())->setCacheMaxAge(0);
    }
    return new CacheableMetadata();
  }

}
