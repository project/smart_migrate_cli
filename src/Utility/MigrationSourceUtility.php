<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli\Utility;

use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\migrate\Plugin\migrate\id_map\Sql;
use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Plugin\MigrateIdMapInterface;
use Drupal\migrate\Plugin\MigrateSourceInterface;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Utility for migration source plugins.
 *
 * Copied from Migrate Magician.
 */
class MigrationSourceUtility {

  /**
   * Whether the source SQL database and the DB of SQL-based id map is joinable.
   *
   * @var bool
   */
  protected static bool $joinable;

  /**
   * Instantiates a migration source plugin from a plugin ID or configuration.
   *
   * This is a smarter alternative to MigrationDeriverTrait::getSourcePlugin,
   * which isn't able to accept plugin configuration, which means that you're
   * unable to instantiate the 'variable' source plugin in Drupal 8.9.x.
   *
   * @param string|array $source_plugin
   *   The source plugin ID, or a full source plugin configuration.
   *
   * @return \Drupal\migrate\Plugin\MigrateSourceInterface|\Drupal\migrate\Plugin\RequirementsInterface
   *   The fully initialized source plugin.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If the plugin configuration is invalid, or the plugin cannot be found.
   */
  public static function getSourcePlugin($source_plugin) {
    $source_plugin_configuration = is_string($source_plugin)
      ? ['plugin' => $source_plugin, 'ignore_map' => TRUE]
      : ['ignore_map' => TRUE] + $source_plugin;
    $stub_migration_definition = [
      'source' => $source_plugin_configuration,
      'destination' => [
        'plugin' => 'null',
      ],
      'idMap' => [
        'plugin' => 'null',
      ],
    ];
    return static::createStubMigration($stub_migration_definition)
      ->getSourcePlugin();
  }

  /**
   * Creates a stub migration from the given definition.
   *
   * @param array $definition
   *   A migration plugin definition.
   *
   * @return \Drupal\migrate\Plugin\MigrationInterface
   *   A stub migration.
   */
  protected static function createStubMigration(array $definition): MigrationInterface {
    return \Drupal::service('plugin.manager.migration')
      ->createStubMigration($definition);
  }

  /**
   * Determines how many items have to be imported (processed).
   *
   * @param \Drupal\migrate\Plugin\MigrateSourceInterface $source
   *   Source plugin instance of the migration.
   * @param \Drupal\migrate\Plugin\MigrateIdMapInterface $id_map
   *   ID map plugin of the migration.
   * @param string $migration_id
   *   ID of the migration.
   * @param bool $track_changes
   *   Whether change tracking is enabled.
   * @param bool $high_water_enabled
   *   Whether high water property is used.
   *
   * @return int
   *   Number of items to be imported (processed).
   */
  public static function getNumberOfItemsToImportProcessFromPieces(MigrateSourceInterface $source, MigrateIdMapInterface $id_map, string $migration_id, bool $track_changes, bool $high_water_enabled): int {
    $source_count = $source->count(TRUE);
    // Change tracking always means that we have to process all items.
    if (
      $id_map->processedCount() === 0 || $track_changes) {
      $items_to_process = $source_count;
    }
    // High water property.
    elseif (
      $high_water_enabled &&
      !is_null(\Drupal::keyValue('migrate:high_water')->get($migration_id))) {
      if (!$source instanceof SqlBase) {
        // We don't know how many items we have to process.
        $items_to_process = MigrateSourceInterface::NOT_COUNTABLE;
      }
      else {
        $source_ref = new \ReflectionClass($source);
        $source_iterator_method = $source_ref->getMethod('initializeIterator');
        $source_iterator_method->setAccessible(TRUE);
        $source_iterator_method->invoke($source);
        $source_query_ref = $source_ref->getProperty('query');
        $source_query_ref->setAccessible(TRUE);
        $items_to_process = (int) $source_query_ref->getValue($source)->countQuery()->execute()->fetchField();
      }
    }

    // No change tracking or no HW, or there is a HW, but the source is not SQL.
    if (!isset($items_to_process)) {
      // Errored items aren't processed again.
      $items_to_process = $source_count > 0
        ? $source_count - $id_map->processedCount() - $id_map->updateCount()
        : MigrateSourceInterface::NOT_COUNTABLE;
    }

    unset($source_ref);
    return $items_to_process;
  }

  /**
   * Determines how many items have to be imported (processed) in the migration.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration_instance
   *   The migration plugin instance to check.
   *
   * @return int
   *   Number of items to be imported (processed) in the migration.
   */
  public static function getNumberOfItemsToImportProcess(MigrationInterface $migration_instance): int {
    $migration = clone $migration_instance;
    $source_config = $migration->getSourceConfiguration();
    $source = $migration->getSourcePlugin();
    $id_map = $migration->getIdMap();

    $items_to_process = static::getNumberOfItemsToImportProcessFromPieces($source, $id_map, $migration->id(), !empty($source_config['track_changes']), array_key_exists('high_water_property', $source_config));
    unset($migration);
    unset($source);
    unset($id_map);
    return $items_to_process;
  }

  /**
   * Whether source and id map DB are joinable.
   *
   * @return bool
   *   TRUE if we can join against the source table otherwise FALSE.
   *
   * @see \Drupal\migrate\Plugin\migrate\source\SqlBase::mapJoinable()
   */
  protected static function sourceIsJoinableToSqlIdMap(): bool {
    if (!isset(static::$joinable)) {
      $stub_migration = \Drupal::service('plugin.manager.migration')
        ->createStubMigration([
          'id' => 'smc_joinability_test',
          'source' => ['plugin' => 'variable', 'variables' => []],
          'destination' => ['plugin' => 'null'],
          'idMap' => ['plugin' => 'sql'],
        ]);
      assert($stub_migration instanceof MigrationInterface);
      $source = $stub_migration->getSourcePlugin();
      assert($source instanceof SqlBase);
      $source_db = $source->getDatabase();
      $id_map = $stub_migration->getIdMap();
      assert($id_map instanceof Sql);
      $id_map_db = $id_map->getDatabase();

      $source_ref = new \ReflectionClass($source);
      $is_joinable = $source_ref->getMethod('mapJoinable');
      $is_joinable->setAccessible(TRUE);
      $source_and_id_map_are_joinable = $is_joinable->invoke($source);

      if ($source_and_id_map_are_joinable) {
        try {
          $source_db->select($id_map->getQualifiedMapTableName(), 'map')
            ->fields('map')
            ->execute()
            ->fetchField();
        }
        catch (DatabaseExceptionWrapper $e) {
          $source_and_id_map_are_joinable = FALSE;
        }
      }

      $id_map_db->schema()->dropTable($id_map->mapTableName());
      $id_map_db->schema()->dropTable($id_map->messageTableName());
      static::$joinable = $source_and_id_map_are_joinable;
    }
    return static::$joinable;
  }

}
