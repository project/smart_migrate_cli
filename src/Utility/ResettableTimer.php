<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli\Utility;

use Drupal\Component\Utility\Timer;

/**
 * A timer which is resettable.
 */
class ResettableTimer extends Timer {

  /**
   * Resets the timer with the specified name.
   *
   * @param string|int $name
   *   The name of the timer.
   */
  public static function reset($name): void {
    unset(static::$timers[$name]);
  }

}
