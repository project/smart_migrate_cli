<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\migrate\Exception\RequirementsException;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\MigrationPluginManager;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate\Plugin\RequirementsInterface;
use Drupal\smart_migrate_cli\Component\MigrationDependencyGraph;

/**
 * Lightweight migration plugin manager.
 */
class SimpleMigrationManager extends MigrationPluginManager {

  /**
   * The actual migration plugin manager.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManagerInterface
   */
  protected $currentPluginManager;

  /**
   * Construct a migration plugin manager.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend for the definitions.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\migrate\Plugin\MigrationPluginManagerInterface $plugin_manager
   *   The migration plugin manager.
   */
  public function __construct(ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend, LanguageManagerInterface $language_manager, MigrationPluginManagerInterface $plugin_manager) {
    parent::__construct($module_handler, $cache_backend, $language_manager);
    $this->currentPluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    if (!isset($this->definitions)) {
      $this->definitions = $this->currentPluginManager->getDefinitions();
    }
    return $this->definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function clearCachedDefinitions() {
    $this->definitions = NULL;
    $this->currentPluginManager->clearCachedDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function buildDependencyMigration(array $migrations, array $dynamic_ids) {
    // If caller is the manager's createInstances method, we don't build the
    // dependency graph. Smart Migrate CLI will do this separately.
    $is_createinstances_call = array_reduce(
      debug_backtrace(0, 2),
      function (bool $carry, array $stack) {
        if ($carry) {
          return $carry;
        }
        $class = $stack['class'] ?? NULL;
        return $class &&
          in_array(MigrationPluginManagerInterface::class, class_implements($class)) &&
          ($stack['function'] ?? NULL) === 'createInstances';
      },
      FALSE
    );
    if ($is_createinstances_call) {
      return $migrations;
    }

    return $this->doBuildDependencyMigration($migrations, $dynamic_ids);
  }

  /**
   * Builds a dependency tree for the migrations and set their order.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface[] $migrations
   *   Array of loaded migrations with their declared dependencies.
   * @param string[] $dynamic_ids
   *   Keys are dynamic ids (for example node:*) values are a list of loaded
   *   migration ids (for example node:page, node:article).
   *
   * @return \Drupal\migrate\Plugin\MigrationInterface[]
   *   An array of migrations.
   */
  public function doBuildDependencyMigration(array $migrations, array $dynamic_ids) {
    // Migration dependencies can be optional or required. If an optional
    // dependency does not run, the current migration is still OK to go. Both
    // optional and required dependencies (if run at all) must run before the
    // current migration.
    $dependency_graph = new MigrationDependencyGraph([]);
    $have_optional = FALSE;

    foreach ($migrations as $migration_id => $migration) {
      assert($migration instanceof MigrationInterface);
      $requirements[$migration_id] = [];
      $graph_item = ['edges' => []];
      $migration_dependencies = $migration->getMigrationDependencies();

      if (!empty($migration_dependencies['required'])) {
        foreach ($migration_dependencies['required'] as $dependency) {
          $this->addItemDependency($graph_item, $dependency, $dynamic_ids);
        }
      }

      if (!empty($migration_dependencies['optional'])) {
        foreach ($migration_dependencies['optional'] as $dependency) {
          $this->addItemDependency($graph_item, $dependency, $dynamic_ids);
        }
        $have_optional = TRUE;
      }

      $dependency_graph->addItem($graph_item, $migration_id);
    }

    // Collect weights.
    $dependency_graph = $dependency_graph->searchAndSort();
    $weights = array_reduce(
      array_keys($migrations),
      function (array $carry, string $item) use ($dependency_graph) {
        $carry[$item] = $dependency_graph[$item]['weight'];
        return $carry;
      },
      []
    );

    // "Required" graph.
    if ($have_optional) {
      $dependency_graph = new MigrationDependencyGraph([]);

      foreach ($migrations as $migration_id => $migration) {
        assert($migration instanceof MigrationInterface);
        $requirements[$migration_id] = [];
        $required_graph_item = ['edges' => []];
        $migration_dependencies = $migration->getMigrationDependencies();

        if (!empty($migration_dependencies['required'])) {
          foreach ($migration_dependencies['required'] as $dependency) {
            if (!isset($dynamic_ids[$dependency])) {
              $this->addItemDependency($required_graph_item, $dependency, $dynamic_ids);
            }
          }
        }
        $dependency_graph->addItem($required_graph_item, $migration_id);
      }
      $dependency_graph = $dependency_graph->searchAndSort();
    }

    foreach ($migrations as $migration_id => $migration) {
      if (!empty($dependency_graph[$migration_id]['paths'])) {
        $migration->set('requirements', $dependency_graph[$migration_id]['paths']);
      }
    }
    unset($dependency_graph);

    // Sort weights, labels, and keys in the same order as each other.
    array_multisort(
      // Use the numerical weight as the primary sort.
      $weights,
      SORT_DESC,
      SORT_NUMERIC,
      // When migrations have the same weight, sort them alphabetically by ID.
      array_keys($migrations),
      SORT_ASC,
      SORT_NATURAL,
      $migrations
    );

    return $migrations;
  }

  /**
   * Add one or more dependencies to a graph item.
   *
   * @param array $graph_item
   *   The graph item.
   * @param string $dependency
   *   The dependency string.
   * @param string[] $dynamic_ids
   *   The dynamic ID mapping.
   */
  protected function addItemDependency(array &$graph_item, string $dependency, array $dynamic_ids) {
    $dependencies = $dynamic_ids[$dependency] ?? [$dependency];
    if (!isset($graph_item['edges'])) {
      $graph_item['edges'] = [];
    }
    $graph_item['edges'] += array_combine($dependencies, $dependencies);
  }

  /**
   * Checks if requirements for the given migration plugin instance are met.
   *
   * This is an optimized version of the requirements check built into
   * migrations: this performs the same check, but operates on a clone, which is
   * removed from PHP memory after the check is done.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration plugin instance to check.
   * @param bool $check_dependencies
   *   Whether dependencies also should be checked or not. Optional, defaults to
   *   TRUE.
   *
   * @return bool
   *   Whether requirements are met or not.
   */
  public function migrationRequirementsMet(MigrationInterface $migration, bool $check_dependencies = TRUE): bool {
    $clone = clone $migration;
    // Check whether the current migration source and destination plugin
    // requirements are met or not.
    try {
      if ($clone->getSourcePlugin() instanceof RequirementsInterface) {
        $clone->getSourcePlugin()->checkRequirements();
      }
      if ($clone->getDestinationPlugin() instanceof RequirementsInterface) {
        $clone->getDestinationPlugin()->checkRequirements();
      }

      if ($check_dependencies && !empty($requirements = $clone->getRequirements())) {
        // Requirements need to be checked.
        $required_migrations = $this->createInstances($requirements);

        if (empty(array_diff($requirements, array_keys($required_migrations)))) {
          // Check if the dependencies are in good shape.
          foreach ($required_migrations as $required_migration) {
            if (!$required_migration->allRowsProcessed()) {
              $return = FALSE;
              break 1;
            }
          }
        }
        else {
          $return = FALSE;
        }
      }

      $return = $return ?? TRUE;
    }
    catch (RequirementsException $e) {
      $return = FALSE;
    }

    unset($clone);
    unset($required_migrations);
    unset($requirements);

    return $return;
  }

  /**
   * Returns the IDs of the given migration's unmet migration dependencies.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration plugin instance to check.
   */
  public function getMissingRequirements(MigrationInterface $migration) {
    $clone = clone $migration;
    $requirements = $clone->getRequirements();
    if (!empty($requirements)) {
      // Requirements need to be checked.
      $required_migrations = $this->createInstances($requirements);
      $missing_migrations = array_diff($requirements, array_keys($required_migrations));

      // Check if the dependencies are in good shape.
      foreach ($required_migrations as $required_migration) {
        if (!$required_migration->allRowsProcessed()) {
          $missing_migrations[] = $required_migration->id();
        }
      }
    }

    unset($clone);
    unset($requirements);
    unset($required_migrations);
    unset($required_migration);

    return $missing_migrations ?? [];
  }

  /**
   * Returns requirement message of the given plugin, if any.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration plugin instance to check.
   *
   * @return string|null
   *   The requirements-not-met message if not met, or NULL if the migration is
   *   ready for being executed.
   */
  public function getMigrationRequirementsMessages(MigrationInterface $migration): ?string {
    $message = NULL;
    $migration_clone = clone $migration;
    // Check whether the current migration source and destination plugin
    // requirements are met or not.
    try {
      if ($migration_clone->getSourcePlugin() instanceof RequirementsInterface) {
        $migration_clone->getSourcePlugin()->checkRequirements();
      }
      if ($migration_clone->getDestinationPlugin() instanceof RequirementsInterface) {
        $migration_clone->getDestinationPlugin()->checkRequirements();
      }

      if (!empty($missing_migration_ids = $this->getMissingRequirements($migration_clone))) {
        $message = sprintf("Missing migrations %s.", implode(', ', $missing_migration_ids));
      }
    }
    catch (RequirementsException $e) {
      $message = $e->getMessage();
    }

    unset($migration_clone);
    unset($missing_migration_ids);

    return $message;
  }

}
