<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli\Traits;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\DatabaseExceptionWrapper;
use Drupal\migrate\Exception\RequirementsException;
use Drupal\migrate_drupal\NodeMigrateType;
use Drupal\migrate_drupal\Plugin\migrate\source\Variable;
use Drupal\smart_migrate_cli\Utility\MigrationSourceUtility;

/**
 * Shim trait for determining source Drupal version.
 */
trait SmartMigrationConfigurationTrait {

  /**
   * Filters the migrations for import.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface[] $migrations
   *   The discovered migrations.
   * @param int|string $drupal_version
   *   The version of Drupal we're getting the migrations for.
   *
   * @return \Drupal\migrate\Plugin\MigrationInterface[]
   *   The migrations for import.
   */
  protected function filterDrupalMigrations(array $migrations, $drupal_version): array {
    if ($drupal_version) {
      static::filterDrupalNodeMigrations($migrations, $drupal_version);
    }

    static::dropFollowUpMigrations($migrations);

    return $migrations;
  }

  /**
   * Removes unnecessary Drupal migrations from the given set.
   *
   * If the configured source is Drupal 7, removes all the migrations which
   * have any "Drupal *" tag, but miss "Drupal 7".
   *
   * @param array $migrations
   *   The actual migration definitions or plugin instances, keyed by their
   *   plugin ID.
   * @param string|int $drupal_version
   *   The discovered major version of the source Drupal instance.
   */
  protected static function filterDrupalNodeMigrations(array &$migrations, $drupal_version): void {
    // Unset the node migrations that should not run based on the type of node
    // migration. That is, if this is a complete node migration then unset the
    // classic node migrations and if this is a classic node migration then
    // unset the complete node migrations.
    $type = NodeMigrateType::getNodeMigrateType(\Drupal::database(), $drupal_version);
    switch ($type) {
      case NodeMigrateType::NODE_MIGRATE_TYPE_COMPLETE:
        $patterns = '/(d' . $drupal_version . '_node\b)|(d' . $drupal_version . '_node_translation\b)|(d' . $drupal_version . '_node_revision\b)|(d7_node_entity_translation\b)/';
        break;

      case NodeMigrateType::NODE_MIGRATE_TYPE_CLASSIC:
        $patterns = '/(d' . $drupal_version . '_node_complete\b)/';
        break;
    }
    foreach (array_keys($migrations) as $plugin_id) {
      if (preg_match($patterns, $plugin_id)) {
        unset($migrations[$plugin_id]);
      }
    }
  }

  /**
   * Removes Follow-up migrations.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface[] $migrations
   *   The actual migration definitions or plugin instances, keyed by their
   *   plugin ID.
   */
  protected static function dropFollowUpMigrations(array &$migrations): void {
    foreach ($migrations as $plugin_id => $migration) {
      // Skip migrations tagged with any of the follow-up migration tags. They
      // will be derived and executed after the migrations on which they depend
      // have been successfully executed.
      // @see Drupal\migrate_drupal\Plugin\MigrationWithFollowUpInterface
      if (empty(array_intersect($migration->getMigrationTags(), static::getFollowUpMigrationTags()))) {
        continue;
      }

      unset($migrations[$plugin_id]);
    }
  }

  /**
   * Retrieves the source Drupal's major version.
   *
   * @return int|null
   *   The major version of the source Drupal instance, or NULL if there is no
   *   Drupal source configured.
   *
   * @see \Drupal\migrate_drupal\MigrationConfigurationTrait::getLegacyDrupalVersion()
   */
  public static function getSourceDrupalVersion(): ?int {
    static $sourceDrupalMajorVersion;
    if (!isset($sourceDrupalMajorVersion)) {
      if (!\Drupal::moduleHandler()->moduleExists('migrate_drupal')) {
        $sourceDrupalMajorVersion = '';
        return NULL;
      }

      // Try to get the connection to the source.
      try {
        $variable_source_plugin = MigrationSourceUtility::getSourcePlugin([
          'plugin' => 'variable',
          'variables' => [],
        ]);
        $variable_source_plugin->checkRequirements();
      }
      catch (PluginNotFoundException | RequirementsException $e) {
      }

      if (!$variable_source_plugin || (!$variable_source_plugin instanceof Variable)) {
        // If the 'variable' source plugin is not available or its requirements
        // are not met, then we can assume that this is not a Drupal to Drupal
        // migration.
        $sourceDrupalMajorVersion = '';
      }
      else {
        $source_version = static::getLegacyDrupalVersion($variable_source_plugin->getDatabase());
        $sourceDrupalMajorVersion = $source_version ? (int) $source_version : '';
      }
    }
    return $sourceDrupalMajorVersion ?: NULL;
  }

  /**
   * Returns the follow-up migration tags.
   *
   * @return string[]
   *   The follow-up migration tags.
   */
  protected static function getFollowUpMigrationTags(): array {
    static $followUpMigrationTags;
    if (!isset($followUpMigrationTags)) {
      $followUpMigrationTags = \Drupal::service('config.factory')
        ->get('migrate_drupal.settings')
        ->get('follow_up_migration_tags') ?: [];
    }
    return $followUpMigrationTags;
  }

  /**
   * Determines what version of Drupal the source database contains.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection object.
   *
   * @return string|null
   *   A string representing the major branch of Drupal core (e.g. '6' for
   *   Drupal 6.x), or FALSE if no valid version is matched.
   *
   * @see \Drupal\migrate_drupal\MigrationConfigurationTrait::getLegacyDrupalVersion()
   */
  public static function getLegacyDrupalVersion(Connection $connection): ?string {
    // Don't assume because a table of that name exists, that it has the columns
    // we're querying. Catch exceptions and report that the source database is
    // not Drupal.
    // Drupal 5/6/7 can be detected by the schema_version in the system table.
    if ($connection->schema()->tableExists('system')) {
      try {
        $version_string = $connection
          ->query('SELECT [schema_version] FROM {system} WHERE [name] = :module', [':module' => 'system'])
          ->fetchField();
        if ($version_string && $version_string[0] == '1') {
          if ((int) $version_string >= 1000) {
            $version_string = '5';
          }
          else {
            $version_string = FALSE;
          }
        }
      }
      catch (DatabaseExceptionWrapper $e) {
        $version_string = FALSE;
      }
    }
    // For Drupal 8 (and we're predicting beyond) the schema version is in the
    // key_value store.
    elseif ($connection->schema()->tableExists('key_value')) {
      try {
        $result = $connection
          ->query("SELECT [value] FROM {key_value} WHERE [collection] = :system_schema AND [name] = :module", [
            ':system_schema' => 'system.schema',
            ':module' => 'system',
          ])
          ->fetchField();
        $version_string = unserialize($result);
      }
      catch (DatabaseExceptionWrapper $e) {
        $version_string = FALSE;
      }
    }
    else {
      $version_string = FALSE;
    }

    return $version_string ? substr($version_string, 0, 1) : NULL;
  }

}
