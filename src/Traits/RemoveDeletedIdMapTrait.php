<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli\Traits;

use Drupal\migrate\Plugin\migrate\source\SqlBase;

/**
 * Trait for id map plugins which roll back deleted items.
 */
trait RemoveDeletedIdMapTrait {

  /**
   * Whether the source plugin is SqlBase instance AND its query can be joined.
   *
   * @var bool
   */
  protected bool $sourceIsJoinable;

  /**
   * The SqlBase source plugin instance of the migration, if any.
   *
   * @var \Drupal\migrate\Plugin\migrate\source\SqlBase|null
   */
  protected ?SqlBase $source = NULL;

  /**
   * {@inheritdoc}
   */
  public function rewind() {
    $this->currentRow = NULL;
    $query = $this->getSource()->getDatabase()->select($this->getQualifiedMapTableName(), 'map')
      ->fields('map');

    if (!$this->sourceIsJoinable()) {
      $query->range(0, 0);
    }
    else {
      $count = 1;
      $map_join = '';
      $delimiter = '';
      $source_fields = [];
      foreach ($this->getSource()->getIds() as $field_name => $field_schema) {
        $source_fields[] = $field_name;
        $map_join .= "{$delimiter}source.$field_name = map.sourceid" . $count++;
        $delimiter = ' AND ';
      }
      $query->leftJoin($this->getSource()->query(), 'source', $map_join);
      $query->isNull('source.' . reset($source_fields));
      $query->isNotNull('map.destid1');
    }

    $this->result = $query->execute();
    $this->next();
  }

  /**
   * Returns the SQL source plugin of the current migration.
   *
   * @return \Drupal\migrate\Plugin\migrate\source\SqlBase|null
   *   The SQL based source plugin instance of the current migration, or NULL
   *   if the current migration has a different type of source.
   */
  protected function getSource(): ?SqlBase {
    if ($this->sourceIsJoinable() && !isset($this->source)) {
      $source = (clone $this->migration)->getSourcePlugin();
      assert($source instanceof SqlBase);
      $this->source = $source;
    }
    return $this->source;
  }

  /**
   * Checks if we can join against the source table.
   *
   * @return bool
   *   TRUE if we can join against the source table otherwise FALSE.
   *
   * @see \Drupal\migrate\Plugin\migrate\source\SqlBase::mapJoinable()
   */
  protected function sourceIsJoinable() {
    if (isset($this->sourceIsJoinable)) {
      return $this->sourceIsJoinable;
    }

    $source = (clone $this->migration)->getSourcePlugin();
    if (!$source instanceof SqlBase) {
      $joinable = FALSE;
    }
    else {
      // With batching, we want a later batch to return the same rows that would
      // have been returned at the same point within a monolithic query. If we
      // join to the map table, the first batch is writing to the map table and
      // thus affecting the results of subsequent batches. To be safe, we avoid
      // joining to the map table when batching.
      if (($this->migration->getSourceConfiguration()['batch_size'] ?? 0) > 0) {
        $joinable = $joinable ?? FALSE;
      }

      $id_map_database_options = $this->getDatabase()->getConnectionOptions();
      $source_database_options = $source->getDatabase()->getConnectionOptions();

      // Special handling for sqlite which deals with files.
      if ($id_map_database_options['driver'] === 'sqlite' &&
        $source_database_options['driver'] === 'sqlite' &&
        $id_map_database_options['database'] != $source_database_options['database']
      ) {
        $joinable = $joinable ?? FALSE;
      }

      // FALSE if driver is PostgreSQL and database doesn't match.
      if ($id_map_database_options['driver'] === 'pgsql' &&
        $source_database_options['driver'] === 'pgsql' &&
        $id_map_database_options['database'] != $source_database_options['database']
      ) {
        $joinable = $joinable ?? FALSE;
      }

      foreach (['username', 'password', 'host', 'port', 'namespace', 'driver'] as $key) {
        if (isset($source_database_options[$key]) && isset($id_map_database_options[$key])) {
          if ($id_map_database_options[$key] != $source_database_options[$key]) {
            $joinable = $joinable ?? FALSE;
            break 1;
          }
        }
      }
    }

    unset($source);
    $this->sourceIsJoinable = $joinable ?? TRUE;
    return $this->sourceIsJoinable;
  }

}
