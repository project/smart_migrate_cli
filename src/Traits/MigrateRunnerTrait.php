<?php

namespace Drupal\smart_migrate_cli\Traits;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Site\Settings;
use Drupal\migrate\Plugin\migrate\destination\EntityContentBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drush\Utils\StringUtils;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Trait for Drush migrate import commands.
 */
trait MigrateRunnerTrait {

  /**
   * Retrieves a list of active migrations, grouped by tags requested.
   *
   * @param string|null $migration_ids
   *   A comma-separated list of migration IDs. If omitted, will return all
   *   migrations.
   * @param string|null $tags
   *   A comma separated list of tags.
   *
   * @return \Drupal\migrate\Plugin\MigrationInterface[][]
   *   An array keyed by migration tag, each value containing an array of
   *   migrations or an empty array if no migrations match the input criteria.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   If one of the migration IDs is missing.
   */
  protected function doGetGroupedMigrationList(?string $migration_ids, ?string $tags): array {
    $migration_ids = StringUtils::csvToArray((string) $migration_ids);
    $tags = array_filter(array_map('trim', StringUtils::csvToArray((string) $tags)));
    $migrations = count($tags) === 1 && empty($migration_ids)
      ? $this->migrationPluginManager->createInstancesByTag(current($tags))
      : $this->migrationPluginManager->createInstances($migration_ids);

    // Missing IDs might be base plugin IDs.
    $existing_ids_including_base_ids = array_reduce(
      array_keys($migrations),
      function (array $carry, string $full_id) {
        $full_id_parts = explode(PluginBase::DERIVATIVE_SEPARATOR, $full_id);
        for ($i = 1; $i <= count($full_id_parts); $i++) {
          $current = implode(PluginBase::DERIVATIVE_SEPARATOR, array_slice($full_id_parts, 0, $i));
          $carry[$current] = $current;
        }
        return $carry;
      },
      []
    );

    // Check for invalid migration IDs.
    if ($missing_ids = array_diff($migration_ids, $existing_ids_including_base_ids)) {
      throw new \InvalidArgumentException('Invalid migration IDs: ' . implode(', ', $missing_ids));
    }

    // If --tag was not passed, don't group on tags, use a single empty tag.
    if (count($tags) < 2) {
      $key = current($tags) ?: NULL;
      return [$key => $migrations];
    }

    $list = [];
    foreach ($migrations as $plugin_id => $migration) {
      $migration_tags = $migration->getMigrationTags();
      $common_tags = array_intersect($tags, $migration_tags);
      if (!$common_tags) {
        // Skip if migration is not tagged with any of the passed tags.
        continue;
      }
      foreach ($common_tags as $tag) {
        $list[$tag][$plugin_id] = $migration;
      }
    }

    return $list;
  }

  /**
   * Drops migrations whose source or destination requirements aren't met.
   */
  protected function filterMigrationList(array $migrations_grouped) {
    foreach ($migrations_grouped as $group => $migrations) {
      foreach ($migrations as $migration_id => $migration) {
        if (!$this->migrationPluginManager->migrationRequirementsMet($migration, FALSE)) {
          $this->logger()->debug("Migration '$migration_id' is skipped as its source or destination plugin has missed requirements.");
          unset($migrations_grouped[$group][$migration_id]);
        }
      }
    }
    return $migrations_grouped;
  }

  /**
   * {@inheritdoc}
   */
  protected function orderMigrationList(array $list, bool $optimal_order = TRUE): array {
    // We want to build the optimal migration order - so we merge the subsets
    // into one single list.
    if ($optimal_order && count($list) > 1) {
      foreach ($list as $items) {
        $list_grouped[NULL] = array_merge(
          $list_grouped[NULL] ?? [],
          $items
        );
      }
    }
    $list_grouped = $list_grouped ?? $list;

    // If we have a Drupal 6 or Drupal 7 source instance DB configured, drop all
    // the migrations which have at least one "Drupal *" tag, but the
    // appropriate 'Drupal <source_version>' tag.
    // Also drop migrations which source or destination requirements aren't met.
    $source_drupal_version = static::getSourceDrupalVersion();
    $drupal_tag = $source_drupal_version ? "Drupal $source_drupal_version" : NULL;
    if ($drupal_tag) {
      foreach ($list_grouped as $group => $list) {
        foreach ($list as $id => $migration) {
          $tags = $migration->getMigrationTags();
          $any_drupal_tag_found = array_reduce(
            $tags,
            function (bool $carry, string $tag) {
              if (!$carry && strpos($tag, 'Drupal ') === 0) {
                $carry = TRUE;
              }
              return $carry;
            },
            FALSE
          );
          if ($any_drupal_tag_found && !in_array($drupal_tag, $tags, TRUE)) {
            unset($list_grouped[$group][$id]);
          }
        }

        $list_grouped[$group] = static::filterDrupalMigrations($list_grouped[$group], $source_drupal_version);
      }
    }

    // Build the optimal migration order based on required and optional
    // migration dependencies.
    if ($optimal_order) {
      foreach ($list_grouped as $group => $list) {
        $list_grouped[$group] = $this->migrationPluginManager->buildDependencyMigration($list, []);
      }
    }
    return $list_grouped;
  }

  /**
   * Prepares a migration plugin instance before being executed.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration to prepare for execution.
   */
  protected function prepareMigration(MigrationInterface $migration): void {
    $this->ensureUptodateCaches($migration);
    $this->prepareFileMigrations($migration);
  }

  /**
   * Adds the relevant source path to file migration source plugins.
   *
   * This method updates file migration's base path the same way how Migrate
   * Drupal UI's MigrateUpgradeImportBatch does it.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   A migration plugin instance.
   *
   * @see \Drupal\migrate_drupal_ui\Batch\MigrateUpgradeImportBatch::run()
   */
  protected function prepareFileMigrations(MigrationInterface $migration): void {
    if ($migration->getDestinationConfiguration()['plugin'] !== 'entity:file') {
      return;
    }
    // Use the private file path if the scheme property is set in the source
    // plugin definition and is 'private' otherwise use the public file path.
    $source_config = $migration->getSourceConfiguration();
    $scheme = $source_config['scheme'] ?? NULL;
    $base_path = ($scheme === 'private') && Settings::get('source_private_file_path')
      ? Settings::get('source_private_file_path', '')
      : Settings::get('source_base_path', '');
    $source_config['constants']['source_base_path'] = rtrim($base_path, '/');
    $migration->set('source', $source_config);
  }

  /**
   * Resets relevant content entity storage cache.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration plugin instance.
   */
  protected function ensureUptodateCaches(MigrationInterface $migration): void {
    if (!$migration->getDestinationPlugin() instanceof EntityContentBase) {
      return;
    }

    // We have to drop stale entity caches - some base fields (e.g. provided by
    // Content Translation) might be installed after the default translations
    // are imported. If we don't drop cached entities and try to import
    // entity translations, the entity storage will return a stale entity
    // without the values of the base fields which were installed later.
    $entity_type_id = explode(':', $migration->getDestinationConfiguration()['plugin'])[1] ?? NULL;
    // Book destination plugin does not correspond to an entity type ID.
    if ($entity_type_id) {
      $this->entityTypeManager->getStorage($entity_type_id)->resetCache();
    }
  }

  /**
   * Returns a freshly instantiated migration plugin instance.
   *
   * @param string $plugin_id
   *   Plugin ID of the migration.
   *
   * @return \Drupal\migrate\Plugin\MigrationInterface
   *   The migration plugin instance.
   */
  protected function getFreshMigrationInstance(string $plugin_id): MigrationInterface {
    // Follow up migrations are generated based on the migrated entity
    // bundles. Unfortunately, neither our injected manager, nor the other
    // instance available through \Drupal::service() is up to date at this
    // point. So: if a migration is not yet instantiated (the value we want
    // to process is a string - the ID of the migration), first we try to
    // instantiate it. If this does not return with a migration instance, we
    // ask Drupal container for a rebuild, then replace our injected
    // migration manager with a fresh instance pulled directly from Drupal's
    // freshly built, up-to-date service container.
    // If the migration still will be missing, we just leave this command
    // throw a very ugly exception - hoping that it will help our users
    // debug what and why happened.
    if (!($first_try = $this->migrationPluginManager->createInstance($plugin_id)) instanceof MigrationInterface) {
      $this->resetEnvironment();
    }
    $instance = $first_try ?: $this->migrationPluginManager->createInstance($plugin_id);
    // We also need up-to-date source counts.
    //
    // There is a bug in SourcePluginBase::count() which disarms cache
    // invalidation unfortunately, because SourcePluginBase is calling
    // CacheBackendInterface::get() with the second argument being 'cache' -
    // meaning that it allows returning invalid cache entries. (This second
    // argument should be a boolean...)
    // @code
    // $this->getCache()->get($this->cacheKey, 'cache');
    // @endcode
    //
    // Anyway, calling count() with forced refresh still works :).
    //
    // @see \Drupal\migrate\Plugin\migrate\source\SourcePluginBase::count
    assert($instance instanceof MigrationInterface);
    $instance->getSourcePlugin()->count(TRUE);
    return $instance;
  }

  /**
   * Resets the Drupal environment.
   *
   * Follow up migrations are generated based on the migrated entity bundles.
   * Unfortunately, neither our injected manager, nor the other instance
   * available through \Drupal::service() are up to date because the injected
   * managers contain outdated static discovery cache. The solution is ask
   * Drupal container for a rebuild, then replace our injected migration manager
   * with a fresh instance pulled directly from Drupal's freshly built,
   * up-to-date service container.
   */
  protected function resetEnvironment(): void {
    \Drupal::service('kernel')->rebuildContainer();
    $this->migrationPluginManager = \Drupal::service('smart_migrate_cli.plugin.manager.migration.simple');
    $this->migrationPluginManager->clearCachedDefinitions();
  }

  /**
   * Write the message to output if not logging.
   *
   * @param iterable|string $message
   *   The message as an iterable of strings or a single string.
   * @param \Symfony\Component\Console\Output\OutputInterface|null $output
   *   The output to use. Message(s) are written to the default output if this
   *   parameter is omitted.
   * @param bool $newline
   *   Whether a newline should be added at the end or not. Defaults to TRUE.
   */
  protected function logIfNormal($message, ?OutputInterface $output = NULL, bool $newline = TRUE): void {
    if (!empty($this->logging)) {
      return;
    }
    $method = $newline ? 'writeln' : 'write';
    $output = $output ?? $this->output();
    $output->$method($message);
  }

  /**
   * Write the message to output if logging.
   *
   * @param iterable|string $message
   *   The message as an iterable of strings or a single string.
   * @param \Symfony\Component\Console\Output\OutputInterface|null $output
   *   The output to use. Message(s) are written to the default output if this
   *   parameter is omitted.
   */
  protected function logIfLogging($message, ?OutputInterface $output = NULL): void {
    if (empty($this->logging)) {
      return;
    }
    $output = $output ?? $this->output();
    $output->writeln($this->getLogPrefix() . $message);
  }

  /**
   * Write the message to output, regardless of being in logging mode or not.
   *
   * @param iterable|string $message
   *   The message as an iterable of strings or a single string.
   * @param \Symfony\Component\Console\Output\OutputInterface|null $output
   *   The output to use. Message(s) are written to the default output if this
   *   parameter is omitted.
   */
  protected function logAnyway($message, ?OutputInterface $output = NULL): void {
    $output = $output ?? $this->output();
    $output->writeln($this->getLogPrefix() . $message);
  }

  /**
   * Prefix to add to output lines.
   *
   * @return string
   *   The current datetime plus a vertical bar if logging, an empty string
   *   otherwise.
   */
  protected function getLogPrefix(): string {
    return empty($this->logging)
      ? ''
      : (new DrupalDateTime('now'))->format('d-m-Y H:i:s') . ' | ';
  }

}
