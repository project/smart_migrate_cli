<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli\Traits;

use Drupal\Component\Utility\Crypt;
use Drupal\migrate\MigrateMessageInterface;
use Drupal\migrate\Plugin\migrate\id_map\Sql;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_drupal\Plugin\MigrationWithFollowUpInterface;
use Drupal\smart_migrate_cli\SmartMigrateCli;
use Drupal\smart_migrate_cli\Thread\MigrationImportThread;
use Drupal\smart_migrate_cli\Thread\MigrationRollbackRemovedThread;
use Drupal\smart_migrate_cli\Thread\MigrationThreadInterface;
use Drupal\smart_migrate_cli\SmartMigrateExecutable;
use Drupal\smart_migrate_cli\TotalProgress;
use Drupal\smart_migrate_cli\Utility\MigrationSourceUtility;
use Drupal\smart_migrate_cli\Utility\ResettableTimer;
use Drush\Drupal\Migrate\MigrateMessage;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Terminal;

/**
 * Basic and complex routines for smart migrate Drush commands.
 */
trait SmartMigrateHelperTrait {

  use MigrateRunnerTrait;

  /**
   * The last "wait" message logged to output.
   *
   * @var string
   */
  private $lastWaitMessage;

  /**
   * Stores the IDs of migrations which are blocking scheduler.
   *
   * @var string[]
   */
  private $schedulerBlockedBy;

  /**
   * IDs of migrations which should be executed at the end of the process.
   *
   * @var string[]
   */
  private $migrationsToRunAtTheEnd;

  /**
   * Cache of unique requirements.
   *
   * @var array[]
   */
  private $requirementsCache;

  /**
   * Determines whether the migration is runnable.
   *
   * @param string $plugin_id
   *   The ID of the migration.
   * @param bool $include_optionals
   *   Whether optional dependencies also should be evaluated.
   *
   * @return bool
   *   Whether the migration is runnable or not.
   */
  protected function migrationIsRunnable(string $plugin_id, bool $include_optionals): bool {
    $requirements_key = $include_optionals
      ? static::REQUIREMENTS_KEY_WITH_OPTIONALS
      : static::REQUIREMENTS_KEY_REQUIRED_ONLY;

    $temp = clone $this->list[$plugin_id];
    $deps = $temp->getMigrationDependencies();
    $cache_key = Crypt::hashBase64(json_encode($deps));
    $requirements_cached = $this->requirementsCache[$cache_key][$requirements_key] ?? NULL;
    if (is_array($requirements_cached)) {
      $requirements_without_self = array_diff($requirements_cached, [$plugin_id]);
      unset($temp);
      unset($requirements_cached);
      return empty(array_diff($requirements_without_self, $this->migrationsFullyProcessed));
    }

    $required = array_diff($deps['required'], [$plugin_id]);
    $optionals_present = array_intersect($deps['optional'], array_keys($this->list));
    $optionals = array_diff($optionals_present, [$plugin_id]);

    // If a required migration isn't present in the current set, we use the API
    // to se whether all of its rows are processed.
    foreach ($required as $required_id) {
      if (array_key_exists($required_id, $this->list)) {
        continue;
      }

      if ($this->migrationIsFullyProcessed($this->list[$plugin_id])) {
        $this->migrationsFullyProcessed[$plugin_id] = $plugin_id;
      }
    }

    if ($include_optionals) {
      $required = array_unique(
        array_merge(
          $required,
          $optionals
        ),
      );
    }

    $required_with_optionals = array_unique(array_merge(array_values($deps['required']), array_values($optionals_present)));
    $this->requirementsCache[$cache_key][$requirements_key] = $include_optionals
      ? $required_with_optionals
      : array_unique(array_values($deps['required']));
    if (!$include_optionals && empty($optionals)) {
      $this->requirementsCache[$cache_key][static::REQUIREMENTS_KEY_WITH_OPTIONALS] = array_unique(array_values($deps['required']));
    }

    $unmet = array_diff($required, $this->migrationsFullyProcessed);

    return empty($unmet);
  }

  /**
   * Determines whether the migration is rollbackable.
   *
   * @param string $plugin_id
   *   The ID of the migration.
   * @param bool $include_optionals
   *   Whether optional dependencies also should be evaluated.
   *
   * @return bool
   *   Whether the migration is runnable or not.
   */
  protected function migrationIsRollbackable(string $plugin_id, bool $include_optionals): bool {
    $unprocessed_migrations_require_this_plugin = array_reduce(
      $this->migrationsToProcess,
      function (array $carry, string $migration_id) use ($plugin_id) {
        if (in_array($plugin_id, $this->list[$migration_id]->getRequirements())) {
          $carry[$migration_id] = $migration_id;
        }
        return $carry;
      },
      []
    );
    $dependees_unprocessed = array_intersect_key($unprocessed_migrations_require_this_plugin, $this->list);
    return empty($dependees_unprocessed);
  }

  /**
   * Returns the blocked method name.
   *
   * @return callable
   *   The blocked method name.
   */
  protected function blockedCallback(string $operation): callable {
    switch ($operation) {
      case 'import':
        return [$this, 'migrationIsRunnable'];

      case 'rollback':
      case 'rollback-removed':
        return [$this, 'migrationIsRollbackable'];
    }

    throw new \UnexpectedValueException('Uknown operation');
  }

  /**
   * Returns the ID of the next unblocked migration, or NULL.
   *
   * @return string|null
   *   The ID of the next unblocked migration (which all requirements are met),
   *   or NULL if there are no migrations scheduled for executing, or none of
   *   the remaining migration's requirements are met.
   */
  protected function getNextUnblockedMigration(string $operation): ?string {
    $blocked_callback = $this->blockedCallback($operation);
    // Early return for blocking processes.
    static $last_next;
    $active_threads = $this->activeThreads();
    $ids_processing = array_reduce(
      $active_threads,
      function (array $carry, MigrationThreadInterface $item) {
        $carry[] = $item->getMigrationId();
        return $carry;
      },
      []
    );
    natsort($ids_processing);

    if (
      !isset($last_next) &&
      !empty($this->schedulerBlockedBy) &&
      $this->schedulerBlockedBy === $ids_processing
    ) {
      // No change.
      return NULL;
    }
    $next_unblocked_plugin_id = NULL;
    $logger_message = NULL;

    foreach ($this->migrationsToProcess as $plugin_id) {
      if (!$this->list[$plugin_id] instanceof MigrationInterface) {
        $this->list[$plugin_id] = $this->getFreshMigrationInstance($plugin_id);
        $this->initializeMapTables($this->list[$plugin_id]);
      }

      // Some migrations should only be returned if nothing else left.
      if (
        !in_array($plugin_id, $this->migrationsToRunAtTheEnd, TRUE) &&
        call_user_func($blocked_callback, $plugin_id, TRUE)
      ) {
        $next_unblocked_plugin_id = $plugin_id;
        break 1;
      }
    }

    // Maybe every other migration depends on the run-as-ast ones?
    if (!$next_unblocked_plugin_id && !$active_threads) {
      foreach (array_intersect_key($this->migrationsToProcess, $this->migrationsToRunAtTheEnd) as $plugin_id) {
        if (call_user_func($blocked_callback, $plugin_id, TRUE)) {
          // Yes, it seems.
          $this->migrationsToRunAtTheEnd = [];
          $next_unblocked_plugin_id = $plugin_id;
          break 1;
        }
      }
    }

    // No migration left which required and optional dependencies are met.
    // Let's check only required dependencies, but only if active processes is
    // empty.
    if (!$next_unblocked_plugin_id) {
      if (!$this->activeThreads()) {
        foreach ($this->migrationsToProcess as $plugin_id) {
          if (call_user_func($blocked_callback, $plugin_id, FALSE)) {
            $next_unblocked_plugin_id = $plugin_id;
            break 1;
          }
        }
      }
      else {
        $logger_message = count($active_threads) > 1
          ? 'Waiting for one of processes be finished...'
          : sprintf("Waiting for %s be finished...", reset($ids_processing));
        if ($this->lastWaitMessage !== $logger_message) {
          $this->schedulerBlockedBy = $ids_processing;
        }
      }
    }

    if ($next_unblocked_plugin_id) {
      $this->clearLoggerSection();
    }

    if ($logger_message && $this->lastWaitMessage !== $logger_message) {
      $this->lastWaitMessage = $logger_message;
      $this->clearLoggerSection();
      $this->logger()->notice($logger_message);
    }

    $last_next = $next_unblocked_plugin_id;
    return $next_unblocked_plugin_id;
  }

  /**
   * Clears logger section if it is a "thing".
   */
  protected function clearLoggerSection(): void {
    if (
      isset($this->loggerSection) &&
      $this->loggerSection instanceof OutputInterface &&
      empty($this->logging)
    ) {
      $this->loggerSection->clear();
    }
  }

  /**
   * Initializes the required number of threads.
   *
   * Also adds empty lines above and below the bars to increase UX.
   *
   * @param int $threads
   *   Number of the threads. Each thread will have a progress bar.
   * @param string $operation
   *   The operation to perform. This might be "import" or "rollback-removed".
   */
  private function initializeThreads(int $threads, string $operation): void {
    switch ($operation) {
      case 'import':
        $class = MigrationImportThread::class;
        break;

      case 'rollback-removed':
        $class = MigrationRollbackRemovedThread::class;
        break;
    }
    $this->totalProgressSection->writeln('');
    $this->totalProgress->start();
    $this->barPlaceholderSections['before'] = $this->output()->section();
    $this->logIfNormal('', $this->barPlaceholderSections['before']);
    $site_alias = $this->siteAliasManager()->getSelf();
    for ($i = 1; $i <= $threads; $i++) {
      $section = $this->output()->section();
      if (!empty($this->logging)) {
        $section->setVerbosity(OutputInterface::VERBOSITY_QUIET);
      }
      $this->threads[] = new $class($site_alias, $section);
    }
    $this->barPlaceholderSections['after'] = $this->output()->section();
    $this->logIfNormal('', $this->barPlaceholderSections['after']);
  }

  /**
   * Returns a free (idle) thread.
   *
   * @return \Drupal\smart_migrate_cli\Thread\MigrationThreadInterface|null
   *   A free "migration thread", or NULL if there are no free slots.
   */
  private function getFreeThread(): ?MigrationThreadInterface {
    $idle_threads = array_filter(
      $this->threads,
      function (MigrationThreadInterface $thread) {
        return $thread->isIdle();
      }
    );
    return $idle_threads ? reset($idle_threads) : NULL;
  }

  /**
   * Returns busy threads.
   *
   * @return \Drupal\smart_migrate_cli\Thread\MigrationThreadInterface[]
   *   All threads which have unfinished (or not yet reported) processes.
   */
  private function getBusyThreads(): array {
    return array_filter(
      $this->threads,
      function (MigrationThreadInterface $thread) {
        return !$thread->isIdle();
      }
    );
  }

  /**
   * Schedules a subprocess migrating the plugin with the specified ID.
   *
   * @param string $migration_id
   *   The plugin ID of the migration to execute.
   */
  private function scheduleMigrationProcess(string $migration_id): void {
    unset($this->migrationsToProcess[$migration_id]);
    $thread = $this->getFreeThread();
    $thread->addProcess($this->list[$migration_id]);
    $thread->start();
  }

  /**
   * Updates threads and progress bars.
   *
   * This method also resets all finished threads, processes and saves migration
   * messages logged during the subprocess and saves them to the main process'
   * error output storage.
   */
  private function handleThreadProgress(): void {
    $total_progress = 0;
    $active_ids = [];
    foreach ($this->activeThreads() as $thread) {
      $total_progress += ($report = $thread->getReport())->getProgress();
      $migration_id = $thread->getMigrationId();
      if (!$report->processIsFinished()) {
        $active_ids[] = $migration_id;
        continue;
      }

      $this->migrationsProcessed[$migration_id] = $migration_id;
      if ($report->taskIsFullyProcessed()) {
        $this->migrationsFullyProcessed[$migration_id] = $migration_id;
      }
      if (empty($errors = $report->getProcessErrors())) {
        unset($this->errorOutputs[$migration_id]);
      }
      else {
        $this->errorOutputs[$migration_id] = $errors;
      }

      $follow_ups = $report->getFollowUpTasks();
      if (!empty($follow_ups) && $diff = array_diff_key($follow_ups, $this->list)) {
        // New follow up migrations have been discovered. It's time to
        // forcefully drop cached migrations, entity type and entity field
        // caches.
        $this->resetEnvironment();
        $this->list = array_merge(
          $this->list,
          $diff
        );
        $this->migrationsToProcess += array_combine(array_keys($diff), array_keys($diff));
      }

      switch ($report->getProcessExistCode()) {
        // We already handle these exit codes.
        case self::EXIT_SUCCESS:
          break;

        case self::EXIT_PROCESS_CRITICAL:
          $this->clearLoggerSection();
          $this->logger()->critical(sprintf(
            "Migration process of '%s' migration threw an exception.",
            $migration_id
          ));
          call_user_func(static::stopSignalClosure());
          break;

        default:
          $this->clearLoggerSection();
          $this->logger()->critical(sprintf(
            "An unexpected error happened during executing '%s' migration",
            $migration_id
          ));
          call_user_func(static::stopSignalClosure());
      }

      $thread->reset();
    }

    $this->totalProgress->advance($total_progress);
    if (!$this->logging) {
      return;
    }

    // Report every n minute.
    if (ResettableTimer::read(SmartMigrateCli::MULTI_THREAD_ID) >= $this->logging * 60 * 1000) {
      ResettableTimer::reset(SmartMigrateCli::MULTI_THREAD_ID);
      ResettableTimer::start(SmartMigrateCli::MULTI_THREAD_ID);

      $ids_str = $active_ids ? '| ' . implode(', ', $active_ids) : '';

      $this->logIfLogging(sprintf(
        '%s %s',
        $this->totalProgress->getProgressPercentage(),
        $ids_str
      ));
    }
  }

  /**
   * Renders a final report about the errors recorded during the migration.
   */
  private function finalReport(): void {
    if (empty($errors_per_plugin = array_filter($this->errorOutputs))) {
      return;
    }

    // Always render report.
    if (($verbosity_original = $this->output()->getVerbosity()) < OutputInterface::VERBOSITY_NORMAL) {
      $this->output()->setVerbosity(OutputInterface::VERBOSITY_NORMAL);
    }

    // Get terminal width. This ensures we always use the entire width.
    $terminal = new Terminal();
    $width = $terminal->getWidth();
    $headers = [dt('Error'), dt('Count')];
    $available_width = $width - ((count($headers) * 3) + 1);

    $table = new Table($this->output());
    $table->setHeaders($headers)
      // Error column always use the rest of the space.
      ->setColumnMaxWidth(0, $available_width - 6)
      // Width of the "Count" column is fixed.
      ->setColumnMaxWidth(1, 6)
      ->setColumnWidths([0 => $available_width - 6, 1 => 6]);
    ksort($errors_per_plugin);
    $last = array_key_last($errors_per_plugin);
    foreach ($errors_per_plugin as $migration_id => $errors) {
      $table->addRow(
        [new TableCell("<comment>$migration_id</comment>", ['colspan' => 2])]
      );
      $table->addRow(new TableSeparator());
      foreach ($errors as $error) {
        $table->addRow([
          str_replace(DRUPAL_ROOT, '<DRUPAL_ROOT>', $error['message']),
          $error['count'] ?? 1,
        ]);
      }
      if ($last !== $migration_id) {
        $table->addRow(new TableSeparator());
      }
    }

    $this->logger()->warning('Some migrations triggered errors during processing. Please check them!');
    $table->render();
    $this->output()->writeln('');
    $this->output()->setVerbosity($verbosity_original);
  }

  /**
   * Executes the given migration plugin instance.
   *
   * Simplified version of Drush's version in MigrateRunnerCommands.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration plugin instance to execute.
   * @param string|null $operation
   *   The migration operation. This can be 'import', 'rollback' or
   *   'rollback-removed'. Defaults to 'import'.
   *
   * @return string[]
   *   IDs of follow up migration, if any.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *
   * @see \Drush\Drupal\Commands\core\MigrateRunnerCommands::executeMigration()
   */
  protected function executeMigration(MigrationInterface $migration, string $operation = 'import'): array {
    $this->prepareMigration($migration);
    if ($operation === 'rollback-removed') {
      $id_map = $migration->getIdMap()->getPluginId();
      $migration->set('idMapPlugin', NULL);
      $migration->set('idMap', ['plugin' => $id_map . '_rollback_removed']);
      $operation = 'rollback';
    }

    $executable = new SmartMigrateExecutable($migration, $this->getMigrateMessage(), $this->output());
    drush_op([$executable, $operation]);
    if ($failure_count = $migration->getIdMap()->errorCount()) {
      $this->logger()->warning(dt(
        '!name migration: ID map plugin reports !count failed rows.',
        ['!name' => $migration->id(), '!count' => $failure_count]
      ));
    }

    // After the migration on which they depend has been successfully executed,
    // the follow-up migrations should be added to the list of the migrations
    // to be executed.
    $follow_ups = $operation === 'import' && $migration instanceof MigrationWithFollowUpInterface
      ? $migration->generateFollowUpMigrations()
      : [];

    return array_keys($follow_ups);
  }

  /**
   * Checks whether the given migration plugin's requirements are met.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration to check.
   *
   * @return bool
   *   Whether the given migration plugin's requirements are met.
   */
  protected function migrationRequirementsAreMet(MigrationInterface $migration): bool {
    return $this->migrationPluginManager->migrationRequirementsMet($migration, TRUE);
  }

  /**
   * Gets the unmet requirements problem message of the given migration, if any.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration to check.
   *
   * @return string|null
   *   The requirements issues of the plugin as human readable string, or NULL
   *   if the plugin's requirements are met.
   */
  protected function getMigrationRequirementsMessages(MigrationInterface $migration): ?string {
    return $this->migrationPluginManager->getMigrationRequirementsMessages($migration);
  }

  /**
   * Initializes the multi thread migration process.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface[] $list
   *   List of the migrations to execute.
   * @param string[] $run_at_the_end
   *   IDs of migrations to execute at the end of the process.
   * @param string|null $format
   *   Progress bar format for the total progress feedback.
   * @param string $operation
   *   The operation to prepare. Optional, defaults to 'import'.
   */
  protected function initializeMultiThreadProcessing(array $list, array $run_at_the_end = [], string $format = NULL, string $operation = 'import'): void {
    $this->logAnyway('Initializing multi-thread processing...', $this->loggerSection);
    $this->totalProgressSection = $this->output()->section();
    $this->totalProgress = new TotalProgress($this->totalProgressSection, $format);
    if ($this->logging) {
      $this->totalProgressSection->setVerbosity(OutputInterface::VERBOSITY_QUIET);
    }
    $this->list = $list;

    // Force-create migrate map tables.
    array_walk($this->list, [$this, 'initializeMapTables']);
    $this->logIfLogging('Finished initializing migrate map tables', $this->loggerSection);

    // Unstuck migrations.
    array_walk($this->list, [$this, 'unstuckMigration']);
    $this->logIfLogging('Finished unstucking migrations', $this->loggerSection);

    $already_processed_callback = [
      'import' => [$this, 'migrationIsFullyProcessed'],
      'rollback-removed' => [$this, 'migrationHasNoDeletedItems'],
    ][$operation];

    $already_processed = $already_processed_callback
      ? array_filter($this->list, $already_processed_callback)
      : [];

    $this->migrationsFullyProcessed = array_combine(
      array_keys($already_processed),
      array_keys($already_processed)
    );
    $this->migrationsNotNeedingProcess = $this->migrationsProcessed = $this->migrationsFullyProcessed;

    $needs_process = array_diff(array_keys($this->list), $this->migrationsProcessed);

    // Populate our local storage which stores the list of the migrations which
    // should be executed.
    $this->migrationsToProcess = array_combine($needs_process, $needs_process);

    // Some core migrations have the best results if they are executed at the
    // end of the process.
    $this->migrationsToRunAtTheEnd = array_combine($run_at_the_end, $run_at_the_end);
    $this->logIfLogging('Finished checking items to process', $this->loggerSection);
  }

  /**
   * Initialized the migration map tables of the given migration.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration plugin instance.
   */
  protected function initializeMapTables(MigrationInterface $migration): void {
    $this->logIfNormal('Init map of ' . $migration->id(), $this->loggerSection, FALSE);
    $clone = clone $migration;
    if (($id_map = $clone->getIdMap()) instanceof Sql) {
      $id_map->getDatabase();
    }
    unset($clone);
    $this->clearLoggerSection();
  }

  /**
   * Unstucks the given migration if it is stuck.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration plugin instance.
   */
  protected function unstuckMigration(MigrationInterface $migration): void {
    $this->logIfNormal('Unstucking ' . $migration->id(), $this->loggerSection, FALSE);
    if ($migration->getStatus() !== MigrationInterface::STATUS_IDLE) {
      $migration->setStatus(MigrationInterface::STATUS_IDLE);
    }
    $this->clearLoggerSection();
  }

  /**
   * Determines whether all rows of a migration are processed.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface|string $migration
   *   The migration plugin instance, or the ID of the migration.
   *
   * @return bool
   *   Whether the rows are processed or not.
   */
  protected function allMigrationRowsProcessed($migration): bool {
    $this->logIfNormal('Checking rows of ' . $migration->id(), $this->loggerSection, FALSE);
    $clone = $migration instanceof MigrationInterface
      ? clone $migration
      : $this->migrationPluginManager->createInstance($migration);
    try {
      $source_count = $clone->getSourcePlugin()->count();
      // If the source is uncountable, we have no way of knowing if it's
      // complete, so stipulate that it is.
      $processed_count = 0;
      if ($source_count > 0) {
        $processed_count = $clone->getIdMap()->processedCount();
      }

      $return = $source_count <= $processed_count;
    }
    catch (\Exception $e) {
    }
    unset($clone);
    $this->clearLoggerSection();
    return $return ?? FALSE;
  }

  /**
   * Determines whether the rows of the migration are fully processed or not.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration plugin instance.
   *
   * @return bool
   *   Whether the rows of the migration are fully processed or not. Fully
   *   processed row means that it was saved and does not need update, or it
   *   was skipped on purpose.
   */
  protected function migrationIsFullyProcessed(MigrationInterface $migration): bool {
    $this->logIfNormal('Checking status of ' . $migration->id(), $this->loggerSection, FALSE);
    $items_to_process = MigrationSourceUtility::getNumberOfItemsToImportProcess($migration);
    if ($items_to_process > 0) {
      $this->totalProgress->incrementMax($items_to_process);
    }

    $this->clearLoggerSection();
    return $items_to_process === 0;
  }

  /**
   * Determines whether none of the migrated source items were deleted.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration plugin instance.
   *
   * @return bool
   *   Whether none of the migrated source items were deleted.
   */
  protected function migrationHasNoDeletedItems(MigrationInterface $migration): bool {
    $this->logIfNormal('Checking deleted items in ' . $migration->id(), $this->loggerSection, FALSE);
    $clone = clone $migration;
    $id_map = $clone->getIdMap()->getPluginId();
    $clone->set('idMapPlugin', NULL);
    $clone->set('idMap', ['plugin' => $id_map . '_rollback_removed']);
    $id_map_removed = $clone->getIdMap();
    $id_map_removed->rewind();
    $removed_count = iterator_count($id_map_removed);
    unset($clone);
    unset($id_map_removed);

    if ($removed_count) {
      $this->totalProgress->incrementMax($removed_count);
    }
    $this->clearLoggerSection();
    return $removed_count < 1;
  }

  /**
   * Returns the migrate message logger.
   *
   * @return \Drupal\migrate\MigrateMessageInterface
   *   The migrate message logger.
   */
  protected function getMigrateMessage(): MigrateMessageInterface {
    if (!isset($this->migrateMessage)) {
      $this->migrateMessage = new MigrateMessage($this->logger());
    }
    return $this->migrateMessage;
  }

}
