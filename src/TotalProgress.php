<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli;

use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Tracks and displays total progress.
 */
class TotalProgress {

  /**
   * The progress bar.
   *
   * @var \Symfony\Component\Console\Helper\ProgressBar
   */
  protected $bar;

  /**
   * The output.
   *
   * @var \Symfony\Component\Console\Output\OutputInterface
   */
  protected $output;

  /**
   * The format of the progress bar.
   *
   * @var string|null
   */
  protected $format;

  /**
   * The "storable" maximum of the progress bar.
   *
   * @var int
   */
  protected $max = 0;

  /**
   * The "storable" progress.
   *
   * @var int
   */
  protected $progress = 0;

  /**
   * Number of ignored digits in progress and max.
   *
   * @var int
   */
  protected $ignoredDigitNum = 0;

  /**
   * The remainder progress that cannot be added because of the ignored digits.
   *
   * @var int
   */
  protected $notAddedProgress = 0;

  /**
   * Constructs a new TotalProgress instance.
   *
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *   The output to use.
   */
  public function __construct(OutputInterface $output) {
    $this->output = $output;
  }

  /**
   * Starts TotalProgress.
   */
  public function start(): void {
    $this->bar = new ProgressBar($this->output, $this->getCurrentMax());
    $this->bar->setFormat(
      $this->getCurrentMax() === 0
        ? SmartMigrateCli::PROGRESS_BAR_FORMAT_ID_TOTAL_NOMAX
        : SmartMigrateCli::PROGRESS_BAR_FORMAT_ID_TOTAL
    );
    $this->bar->start();
    $this->bar->setProgress($this->progress);
  }

  /**
   * Advances the displayed total progress.
   *
   * @param int $processed
   *   Progress since the last update.
   */
  public function advance(int $processed): void {
    if ($this->ignoredDigitNum) {
      $processed = $this->notAddedProgress + $processed;
      $this->notAddedProgress = $processed % (int) str_pad('1', $this->ignoredDigitNum, '0', STR_PAD_RIGHT);
    }
    $internal_processed = (int) $this->splitOffDigits($processed);
    $this->bar->advance($internal_processed);
    $this->progress += $internal_processed;
  }

  /**
   * Finishes displaying total progress.
   */
  public function finish(): void {
    $this->bar->finish();
    $this->bar->clear();
    $this->output->clear();
  }

  /**
   * Returns the currently set maximum.
   */
  public function getCurrentMax(): int {
    return $this->max;
  }

  /**
   * Returns the percentage value of the progress of the actual process.
   *
   * @return string
   *   The percentage value as string, e.g."00.09%" or "67.21%".
   */
  public function getProgressPercentage(): string {
    $number_formatted = $this->max
      ? number_format($this->progress / ($this->max / 100), 2, '.')
      : '0.0';
    $parts = explode('.', $number_formatted);
    $formatted = str_pad($parts[0], 2, '0', STR_PAD_LEFT) .
      '.' .
      str_pad($parts[1], 2, '0', STR_PAD_RIGHT) .
      '%';
    return $this->max ? $formatted : 'NaN';
  }

  /**
   * Increments the maximum.
   *
   * @param string|int $incremented
   *   Integer to increment the maximum with.
   */
  public function incrementMax($incremented): void {
    $extra_digit_splitoff = 0;
    $number_to_add = $this->splitOffDigits($incremented);
    $currentmax = $this->getCurrentMax();

    while (!static::canAdd($currentmax, $number_to_add)) {
      $number_to_add = $this->splitOffDigits($number_to_add, 1);
      $currentmax = $this->splitOffDigits($currentmax, 1);
      $extra_digit_splitoff++;
    }

    if ($extra_digit_splitoff) {
      $this->incrementIgnoredDigitNumber($extra_digit_splitoff);
    }

    $this->max += (int) $incremented;
  }

  /**
   * Splits off the given number of digits from the given number.
   *
   * @param string|int $numeric
   *   The numeric to split the given number of digits from.
   * @param int|null $digits_to_split_off
   *   Number of digits to split off. Optional, defaults to the current number
   *   of the ignored digits.
   *
   * @return string
   *   The new numeric value with the given digit offset.
   */
  protected function splitOffDigits($numeric, int $digits_to_split_off = NULL): string {
    $digits_to_split_off = $digits_to_split_off ?? $this->ignoredDigitNum;
    if ($digits_to_split_off) {
      $numeric_string = (string) $numeric;
      $numeric_string = substr($numeric_string, 0, strlen($numeric_string) - $digits_to_split_off);
      return $numeric_string ? $numeric_string : '0';
    }

    return (string) $numeric;
  }

  /**
   * Increments the current ignored digit number.
   *
   * @param int $ignored_digit_num
   *   The integer to increase the number of ignored digits.
   */
  protected function incrementIgnoredDigitNumber(int $ignored_digit_num): void {
    $this->max = (int) $this->splitOffDigits($this->max, $ignored_digit_num - $this->ignoredDigitNum);
    $this->progress = (int) $this->splitOffDigits($this->progress, $ignored_digit_num - $this->ignoredDigitNum);
    $this->ignoredDigitNum += $ignored_digit_num;
  }

  /**
   * Determines whether to numbers can be added and their sum is an integer.
   *
   * @param int|string $num_1
   *   Numeric value.
   * @param int|string $num_2
   *   Numeric value.
   *
   * @return bool
   *   Whether to numbers can be added and their sum is an integer.
   */
  protected static function canAdd($num_1, $num_2): bool {
    return !static::biggerThanIntMax($num_1) && !static::biggerThanIntMax($num_2) && gettype((int) $num_1 + (int) $num_2) === 'integer';
  }

  /**
   * Determines whether the given numeric value is bigger than PHP_INT_MAX.
   *
   * @param int|string $string
   *   Numeric value to check.
   *
   * @return bool
   *   Whether the given numeric value is bigger than PHP_INT_MAX.
   */
  protected static function biggerThanIntMax($string): bool {
    return (string) ((int) $string) !== (string) $string;
  }

}
