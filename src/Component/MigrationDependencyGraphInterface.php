<?php

namespace Drupal\smart_migrate_cli\Component;

/**
 * Interface for migration dependency graph.
 */
interface MigrationDependencyGraphInterface {

  /**
   * Adds a single item to the graph.
   *
   * @param array $item
   *   The item to add.
   * @param string|int $key
   *   The key (identifier) id the item.
   */
  public function addItem(array $item, $key): void;

  /**
   * Performs a depth-first search and sort on the directed acyclic graph.
   *
   * @return array[]
   *   The given $graph with more secondary keys filled in:
   *   - 'paths': Contains a list of vertices than can be reached on a path from
   *     this vertex.
   *   - 'weight': If there is a path from a vertex to another then the weight
   *     of the latter is higher.
   *   - 'component': Vertices in the same component have the same component
   *     identifier.
   */
  public function searchAndSort();

}
