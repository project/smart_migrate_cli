<?php

namespace Drupal\smart_migrate_cli\Component;

use Drupal\Component\Graph\Graph;

/**
 * Directed acyclic graph manipulation.
 *
 * Same as in core, but optimized for building migration dependency graph. For
 * example reverse paths are excluded.
 */
class MigrationDependencyGraph extends Graph implements MigrationDependencyGraphInterface {

  /**
   * {@inheritdoc}
   */
  public function addItem(array $item, $key): void {
    $this->graph[$key] = $item;
  }

  /**
   * {@inheritdoc}
   */
  protected function depthFirstSearch(&$state, $start, &$component = NULL) {
    parent::depthFirstSearch($state, $start, $component);
    foreach (array_keys($this->graph[$start]['paths']) as $key) {
      unset($this->graph[$key]['reverse_paths']);
    }
  }

}
