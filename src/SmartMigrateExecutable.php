<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli;

use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateMessageInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drush\Drupal\Migrate\MigrateExecutable as DrushMigrateExecutable;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * MigrateExecutable replacement for Drush's implementation.
 *
 * This is a drastically simplified version of Drush core's own version, without
 * event listeners, progress bar, memory limit etc.
 */
class SmartMigrateExecutable extends DrushMigrateExecutable {

  /**
   * {@inheritdoc}
   */
  public function __construct(MigrationInterface $migration, MigrateMessageInterface $message, OutputInterface $output, array $options = []) {
    $options = [
      'idlist' => NULL,
      'limit' => NULL,
      'feedback' => NULL,
      'timestamp' => FALSE,
      'total' => FALSE,
      'delete' => FALSE,
      'progress' => FALSE,
    ];
    parent::__construct($migration, $message, $output, $options);

    $this->unregisterListeners();
    $this->memoryLimit = PHP_INT_MAX;
  }

  /**
   * {@inheritdoc}
   */
  protected function attemptMemoryReclaim() {
    return memory_get_usage();
  }

  /**
   * {@inheritdoc}
   */
  protected function memoryExceeded() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function processPipeline(Row $row, string $destination, array $plugins, $value) {
    try {
      parent::processPipeline($row, $destination, $plugins, $value);
    }
    catch (MigrateException | MigrateSkipRowException $handled_exception) {
      throw $handled_exception;
    }
    catch (\Exception $unhandled_exception) {
      throw new MigrateException(sprintf(
        "Processing destination property threw the following exception: %s",
        $unhandled_exception->getMessage()
      ));
    }
  }

}
