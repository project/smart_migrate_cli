<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli\Thread;

/**
 * Simple report object about a thread's task and subprocess..
 *
 * @internal
 */
class ThreadReport implements ThreadReportInterface {

  /**
   * The progress of the actual task since the last report.
   *
   * @var int
   */
  protected int $progress;

  /**
   * Whether the task is finished or not.
   *
   * @var bool
   */
  protected bool $processFinished;

  /**
   * Whether the task is fully done or not.
   *
   * @var bool
   */
  protected bool $taskIsFullyProcessed;

  /**
   * Errors captured during the execution of the task.
   *
   * @var array|null
   */
  protected ?array $errors;

  /**
   * IDs of the follow-up tasks, if any.
   *
   * @var array|null
   */
  protected ?array $followUps;

  /**
   * Exit code of the task's subprocess.
   *
   * @var int|null
   */
  protected ?int $exitCode;

  /**
   * Constructs a ThreadReport instance.
   *
   * @param int $progress
   *   The progress of the actual task since the last report.
   * @param bool $process_finished
   *   Whether the task is finished or not.
   * @param bool $fully_processed
   *   Whether the task is fully done or not.
   * @param array|null $errors
   *   Errors captured during the execution of the task.
   * @param array|null $follow_ups
   *   IDs of the follow-up tasks, if any.
   * @param int|null $exit_code
   *   Exit code of the task's subprocess.
   */
  public function __construct(int $progress, bool $process_finished = FALSE, bool $fully_processed = FALSE, ?array $errors = NULL, ?array $follow_ups = NULL, ?int $exit_code = NULL) {
    $this->progress = $progress;
    $this->processFinished = $process_finished;
    $this->taskIsFullyProcessed = $fully_processed;
    $this->errors = $errors;
    $this->followUps = $follow_ups;
    $this->exitCode = $exit_code;
  }

  /**
   * {@inheritdoc}
   */
  public function getProgress(): int {
    return $this->progress;
  }

  /**
   * {@inheritdoc}
   */
  public function processIsFinished(): bool {
    return $this->processFinished;
  }

  /**
   * {@inheritdoc}
   */
  public function taskIsFullyProcessed(): bool {
    return $this->taskIsFullyProcessed;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessErrors(): ?array {
    return $this->errors;
  }

  /**
   * {@inheritdoc}
   */
  public function getFollowUpTasks(): ?array {
    return $this->followUps;
  }

  /**
   * {@inheritdoc}
   */
  public function getProcessExistCode(): ?int {
    return $this->exitCode;
  }

}
