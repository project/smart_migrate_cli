<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli\Thread;

/**
 * Interface of thread reports..
 *
 * @internal
 */
interface ThreadReportInterface {

  /**
   * Updates progress and returns the number of the processed items.
   *
   * @return int
   *   Processed items since the last report.
   */
  public function getProgress(): int;

  /**
   * Whether the underlying process is finished.
   *
   * @return bool
   *   Whether the subprocess is finished (terminated).
   */
  public function processIsFinished(): bool;

  /**
   * Whether the underlying process is fully processed.
   *
   * @return bool
   *   Whether the task of the subprocess is fully finished.
   */
  public function taskIsFullyProcessed(): bool;

  /**
   * Returns the errors of the subprocess.
   *
   * @return array|null
   *   Errors happened during the subprocess as array.
   */
  public function getProcessErrors(): ?array;

  /**
   * Returns the follow-up tasks.
   *
   * @return array|null
   *   IDs of the follow up tasks which should be executed after
   *   the successfully finished subprocess.
   */
  public function getFollowUpTasks(): ?array;

  /**
   * Returns the exit code of the subprocess if it is finished.
   *
   * @return int|null
   *   Exit code of the subprocess if it is terminated.
   */
  public function getProcessExistCode(): ?int;

}
