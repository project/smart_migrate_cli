<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli\Thread;

use Consolidation\SiteAlias\SiteAliasInterface;
use Consolidation\SiteProcess\SiteProcess;
use Drupal\migrate\Plugin\migrate\id_map\Sql;
use Drupal\migrate\Plugin\MigrateIdMapInterface;
use Drupal\migrate\Plugin\MigrateSourceInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\smart_migrate_cli\SmartMigrateCli;
use Drupal\smart_migrate_cli\Utility\MigrationSourceUtility;
use Drush\Drush;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleSectionOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Migration thread.
 *
 * Represents a process and handles its progress bar.
 *
 * @internal
 */
class MigrationImportThread implements MigrationThreadInterface {

  const PROGRESS_TRACKER_STATUS_ADDITION = 10;

  const SQL_MAP_STATUS_FIELD = 'source_row_status';

  /**
   * The current site alias.
   *
   * @var \Consolidation\SiteAlias\SiteAliasInterface
   */
  protected SiteAliasInterface $siteAlias;

  /**
   * The output.
   *
   * @var \Symfony\Component\Console\Output\OutputInterface
   */
  protected OutputInterface $output;

  /**
   * Progress bar of the process.
   *
   * @var \Symfony\Component\Console\Helper\ProgressBar
   */
  protected ProgressBar $bar;

  /**
   * Migration ID of the currently processed migration.
   *
   * @var string|null
   */
  protected ?string $migrationId = NULL;

  /**
   * ID map plugin instance of the currently processed migration.
   *
   * @var \Drupal\migrate\Plugin\MigrateIdMapInterface|null
   */
  protected ?MigrateIdMapInterface $migrationIdMap = NULL;

  /**
   * The subprocess of the currently processed task, if any.
   *
   * @var \Consolidation\SiteProcess\SiteProcess|null
   */
  protected ?SiteProcess $process = NULL;

  /**
   * Source plugin instance.
   *
   * @var \Drupal\migrate\Plugin\MigrateSourceInterface|null
   */
  protected ?MigrateSourceInterface $source = NULL;

  /**
   * Number of the items in the source.
   *
   * @var int
   */
  protected int $sourceCount = 0;

  /**
   * Number of items to process.
   *
   * @var int
   */
  protected int $itemsToProcessCount = 0;

  /**
   * Changes are tracked.
   *
   * @var bool
   */
  protected bool $trackChanges = FALSE;

  /**
   * High water is tracked.
   *
   * @var bool
   */
  protected bool $highwater = FALSE;

  /**
   * Number of the processed items since the previous update.
   *
   * @var int
   */
  protected int $previousProgress = 0;

  /**
   * Number of the processed items.
   *
   * @var int
   */
  protected int $currentProgress = 0;

  /**
   * Number of the previously processed items.
   *
   * @var int
   */
  protected int $initialIdMapCount = 0;

  /**
   * Constructs a MigrationThread instance.
   *
   * @param \Consolidation\SiteAlias\SiteAliasInterface $site_alias
   *   The current site alias.
   * @param \Symfony\Component\Console\Output\OutputInterface $output
   *   Output to use.
   */
  public function __construct(SiteAliasInterface $site_alias, OutputInterface $output) {
    $this->siteAlias = $site_alias;
    $this->output = $output;

    $this->initialize();
  }

  /**
   * Initializes a new MigrationThread.
   */
  protected function initialize(): void {
    $this->output->write(SmartMigrateCli::PROGRESS_BAR_EMPTY_TEXT);
    $this->bar = new ProgressBar($this->output, 0, 0.5);
    $this->bar->setFormat(SmartMigrateCli::PROGRESS_BAR_FORMAT_ID_NOMAX);
    $this->bar->setRedrawFrequency(1);
    $this->bar->maxSecondsBetweenRedraws(0.8);
    $this->bar->setBarWidth(SmartMigrateCli::PROGRESS_BAR_WIDTH);
  }

  /**
   * {@inheritdoc}
   */
  public function isIdle(): bool {
    return empty($this->process);
  }

  /**
   * {@inheritdoc}
   */
  public function isActive(): bool {
    return !empty($this->process) && $this->process->isStarted();
  }

  /**
   * {@inheritdoc}
   */
  public function isFinished(): bool {
    return $this->isActive() && $this->process->isTerminated();
  }

  /**
   * {@inheritdoc}
   */
  public function getMigrationId(): ?string {
    return $this->migrationId ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function addProcess(MigrationInterface $migration): void {
    if ($this->process) {
      throw new \LogicException('Cannot add new process.');
    }
    $this->process = Drush::drush($this->siteAlias, 'smart-migrate:single-import')
      ->setEnv(getenv() + [
        SmartMigrateCli::MIGRATION_PLUGIN_ID_ENV => $migration->id(),
      ])
      ->enableOutput();
    $this->migrationId = $migration->id();
    $this->migrationIdMap = (clone $migration)->getIdMap();
    $this->source = (clone $migration)->getSourcePlugin();
    $this->sourceCount = $this->source->count();
    $this->trackChanges = (bool) ($migration->getSourceConfiguration()['track_changes'] ?? FALSE);
    $this->highwater = array_key_exists('high_water_property', $migration->getSourceConfiguration());
    $this->itemsToProcessCount = MigrationSourceUtility::getNumberOfItemsToImportProcessFromPieces(
      $this->source,
      $this->migrationIdMap,
      $this->getMigrationId(),
      $this->trackChanges,
      $this->highwater
    );
    if ($this->trackChanges) {
      $this->initialIdMapCount = $this->migrationIdMap->processedCount() - $this->migrationIdMap->updateCount();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function start(): void {
    if ($this->process->isStarted()) {
      throw new \LogicException('Cannot start an already active migration thread.');
    }

    if ($this->migrationIdMap instanceof Sql) {
      $this->migrationIdMap->getDatabase()
        ->update($this->migrationIdMap->mapTableName())
        ->expression(
          static::SQL_MAP_STATUS_FIELD,
          static::SQL_MAP_STATUS_FIELD . ' + ' . static::PROGRESS_TRACKER_STATUS_ADDITION
        )
        ->condition(
          static::SQL_MAP_STATUS_FIELD,
          static::PROGRESS_TRACKER_STATUS_ADDITION,
          '<'
        )
        ->execute();
    }

    $format = $this->itemsToProcessCount >= 0 ? SmartMigrateCli::PROGRESS_BAR_FORMAT_ID : SmartMigrateCli::PROGRESS_BAR_FORMAT_ID_NOMAX;

    $this->resetBar();
    $this->bar->setMessage("<info>$this->migrationId</info>");
    $this->bar->setFormat($format);
    $this->bar->start(max(0, $this->itemsToProcessCount));
    $this->bar->setProgress($this->currentProgress);

    $this->process->start();
  }

  /**
   * {@inheritdoc}
   */
  public function stop(): void {
    if (isset($this->process) && $this->process->isRunning()) {
      $this->process->signal(SIGKILL);
      // Wait for subprocess being terminated.
      while (!$this->process->isTerminated()) {
        usleep((int) SmartMigrateCli::POLL_WAIT_SECONDS * 1000 * 1000);
      }
    }
    $this->reset();
    if ($this->output instanceof ConsoleSectionOutput) {
      $this->output->clear();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function reset(): void {
    if ($this->process && $this->process->isRunning()) {
      throw new \LogicException('Cannot finish/reset a running thread. Thread must be forcefully stopped or terminated.');
    }
    $this->process = NULL;
    $this->migrationId = NULL;
    $this->sourceCount = 0;
    $this->source = NULL;
    $this->previousProgress = 0;
    $this->currentProgress = 0;
    $this->migrationIdMap = NULL;
    $this->itemsToProcessCount = 0;
    $this->initialIdMapCount = 0;
    $this->resetBar();
  }

  /**
   * {@inheritdoc}
   */
  public function getReport(): ThreadReportInterface {
    // We cannot pause a subprocess. Because of this, we always risk race
    // conditions, because we cannot determine the various states of a process
    // in the same moment. It might happen that the import process still works
    // on the last item when we ask for a progress update (and get know how many
    // items are processed so far), and after that, when we check whether the
    // subprocess is terminated, we get a TRUE result.
    // So to prevent any race condition, we check "isFinished" first and store
    // its return value in a local variable. Of course, it still can happen that
    // the process is finished after we checked its termination, but in this
    // case, we will be able to properly finish the job when the scheduler
    // asks for a new report next time (then we will report "finished" status
    // with 0 items processed since the last report).
    $is_finished = $this->isFinished() && !($is_idle = $this->isIdle());
    $progress = empty($is_idle) ? $this->updateProgress() : 0;
    $fully_processed = empty($is_idle) && $this->isFullyProcessed();

    if ($is_finished) {
      $process_output = array_filter(explode("\n", $this->process->getOutput()));
      $last = array_pop($process_output);
      $message = $last ? json_decode($last, TRUE) : [];
      $follow_ups = array_combine(
        $message['follow_ups'] ?? [],
        $message['follow_ups'] ?? [],
      );
      $errors = array_reduce(
        array_filter(explode("\n", $this->process->getErrorOutput())),
        function (array $carry, string $error) {
          $key = md5($error);
          $carry[$key] = $carry[$key] ?? [
            'count' => 0,
            'message' => $error,
          ];
          $carry[$key]['count'] += 1;
          return $carry;
        },
        []
      );
      // Reset the status field.
      if ($this->migrationIdMap instanceof Sql) {
        $this->migrationIdMap->getDatabase()
          ->update($this->migrationIdMap->mapTableName())
          ->expression(
            static::SQL_MAP_STATUS_FIELD,
            static::SQL_MAP_STATUS_FIELD . ' - ' . static::PROGRESS_TRACKER_STATUS_ADDITION
          )
          ->condition(
            static::SQL_MAP_STATUS_FIELD,
            static::PROGRESS_TRACKER_STATUS_ADDITION,
            '>='
          )
          ->execute();
      }
      else {
        // If map is not SQL, and there are no errors, assume fully finished
        // status.
        $fully_processed = $fully_processed ?: empty($errors);
      }

      // If there are no errors, and the current migration was configured to
      // track changes, and the current progress does not match the initially
      // calculated number of the items to process, we assume that the source
      // plugin just skipped some previously imported unchanged rows.
      if (
        !$errors &&
        $this->trackChanges &&
        $this->currentProgress !== $this->itemsToProcessCount &&
        $this->sourceCount <= $this->migrationIdMap->processedCount()
      ) {
        $progress += $this->initialIdMapCount;
      }
    }

    return new ThreadReport(
      $progress ?? 0,
      $is_finished ?? FALSE,
      $fully_processed ?? FALSE,
      $errors ?? NULL,
      $follow_ups ?? NULL,
      $is_finished ? $this->process->getExitCode() : NULL
    );
  }

  /**
   * Updates the progress bar of the thread and returns the progress.
   *
   * @return int
   *   The number of the processed items since the last update.
   */
  protected function updateProgress(): int {
    $this->previousProgress = $this->currentProgress;
    $this->currentProgress = $this->getCurrentProgress();
    $this->bar->setProgress($this->currentProgress);
    return $this->currentProgress - $this->previousProgress;
  }

  /**
   * Returns the actual number of the processed items.
   *
   * @return int
   *   The actual number of the processed items.
   */
  protected function getCurrentProgress(): int {
    if (!$this->migrationIdMap instanceof Sql) {
      return 0;
    }
    $current_progress_q = $this->migrationIdMap->getDatabase()
      ->select($this->migrationIdMap->mapTableName(), 'm')
      ->fields('m', [static::SQL_MAP_STATUS_FIELD])
      ->condition('m.' . static::SQL_MAP_STATUS_FIELD, static::PROGRESS_TRACKER_STATUS_ADDITION, '<');
    return (int) $current_progress_q->countQuery()->execute()->fetchField();
  }

  /**
   * Determines whether the current task is fully processed.
   *
   * @return bool
   *   Whether the current task is fully processed.
   */
  protected function isFullyProcessed(): bool {
    if (!$this->process->isTerminated()) {
      return FALSE;
    }
    // If the source is uncountable, we have no way of knowing if the migration
    // is complete, so assume that it is.
    if ($this->sourceCount < 0) {
      return TRUE;
    }
    if ($this->itemsToProcessCount <= 0) {
      return TRUE;
    }
    // Task is fully process if the number of items to process matches current
    // progress...
    return $this->itemsToProcessCount <= $this->currentProgress ||
      // ...or if we are tracking changes, the source count matches the process
      // count (this slightly simplifies the situation).
      $this->trackChanges && $this->sourceCount <= $this->migrationIdMap->processedCount();
  }

  /**
   * Resets the progress bar.
   */
  protected function resetBar(): void {
    $this->bar->finish();
    $this->bar->clear();
    if ($this->output instanceof ConsoleSectionOutput) {
      $this->output->overwrite(SmartMigrateCli::PROGRESS_BAR_EMPTY_TEXT);
    }
  }

}
