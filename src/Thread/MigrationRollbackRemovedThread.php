<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli\Thread;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\smart_migrate_cli\SmartMigrateCli;
use Drush\Drush;

/**
 * Thread for rolling back migrated items deleted on the source.
 *
 * @internal
 */
class MigrationRollbackRemovedThread extends MigrationImportThread {

  /**
   * Number of the processed items before deleted items are rolled back.
   *
   * @var int
   */
  protected int $originalItemCount;

  /**
   * {@inheritdoc}
   */
  public function addProcess(MigrationInterface $migration): void {
    if ($this->process) {
      throw new \LogicException('Cannot add new process.');
    }
    $this->process = Drush::drush($this->siteAlias, 'smart-migrate:single-rollback-removed')
      ->setEnv(getenv() + [
        SmartMigrateCli::MIGRATION_PLUGIN_ID_ENV => $migration->id(),
      ])
      ->enableOutput();
    $migration->set('idMapPlugin', NULL);
    $this->migrationId = $migration->id();
    $this->migrationIdMap = (clone $migration)->getIdMap();
    $this->originalItemCount = $this->migrationIdMap->processedCount();
    $removed_items_id_map = (clone $migration)->set('idMap', ['plugin' => $this->migrationIdMap->getPluginId() . '_rollback_removed'])->getIdMap();
    $removed_items_id_map->rewind();
    $this->sourceCount = iterator_count($removed_items_id_map);
    unset($removed_items_id_map);
  }

  /**
   * {@inheritdoc}
   */
  public function start(): void {
    if ($this->process->isStarted()) {
      throw new \LogicException('Cannot start an already active migration thread.');
    }

    $this->previousProgress = 0;
    $this->currentProgress = 0;

    $this->resetBar();
    $this->bar->setMessage("<info>{$this->migrationId}</info>");
    $this->bar->setFormat(SmartMigrateCli::PROGRESS_BAR_FORMAT_ID);
    $this->bar->start($this->sourceCount);
    $this->bar->setProgress(0);

    $this->process->start();
  }

  /**
   * {@inheritdoc}
   */
  protected function updateProgress(): int {
    $this->previousProgress = $this->currentProgress;
    $this->currentProgress = $this->originalItemCount - $this->migrationIdMap->processedCount();
    $this->bar->setProgress($this->currentProgress);
    return $this->currentProgress - $this->previousProgress;
  }

}
