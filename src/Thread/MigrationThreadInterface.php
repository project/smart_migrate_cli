<?php

declare(strict_types = 1);

namespace Drupal\smart_migrate_cli\Thread;

use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Interface of migration threads..
 *
 * @internal
 */
interface MigrationThreadInterface {

  /**
   * Whether the current thread is free to manage another migration import task.
   *
   * @return bool
   *   The current thread is free to manage another migration import task.
   */
  public function isIdle(): bool;

  /**
   * The migration thread has an active subprocess.
   *
   * @return bool
   *   Whether the migration thread has an active subprocess.
   */
  public function isActive(): bool;

  /**
   * The migration thread's subprocess is terminated.
   *
   * @return bool
   *   Whether the migration thread's subprocess is terminated.
   */
  public function isFinished(): bool;

  /**
   * Returns the migration plugin ID of the thread, if any.
   *
   * @return string|null
   *   The migration plugin ID of the thread if any, or NULL if the thread is
   *   idle.
   */
  public function getMigrationId(): ?string;

  /**
   * Adds a new subprocess to the migration thread.
   *
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration plugin instance to import.
   */
  public function addProcess(MigrationInterface $migration): void;

  /**
   * Starts the current subprocess of the thread.
   */
  public function start(): void;

  /**
   * Stops the subprocess of the thread by sending SIGKILL signal.
   */
  public function stop(): void;

  /**
   * Resets the thread.
   */
  public function reset(): void;

  /**
   * Returns report about the thread and its subprocess.
   */
  public function getReport(): ThreadReportInterface;

}
